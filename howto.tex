To access the {\sc Tcl Placet} interface type on your terminal:

  \begin{verbatim}
    $.> placet
  \end{verbatim}

The {\tt Help} command will list all the available commands of your \placet{} version.
While {\tt Help command-name} shows information about the usage of the
command, e.g.:

\begin{verbatim}
  $.> placet
  **********************************************************************
  **                                                                  **
  **                PLACET Version No 0.99.04beta (SWIG)              **
  **                      written by D. Schulte                       **
  **                        contributions from                        **
  **                       A. Latina, N. Leros,                       **
  **                 P. Eliasson, E. D'Amico, E. Adli                 **
  **               B. Dalena, J. Snuverink, Y. Levinsen               **
  **                                                                  **
  **                        THIS VERSION INFO:                        **
  **                      Octave module enabled                       **
  **                      Python module enabled                       **
  **                         MPI module enabled                       **
  **                                                                  **
  **            Submit bugs reports / feature requests to             **
  **             https://savannah.cern.ch/projects/placet             **
  **                                                                  **
  **********************************************************************
  $\%$ Help Quadrupole
  This command places a quadrupole in the current girder.
  
  Quadrupole:
 -name                 Element name [STRING]
 -s                    Longitudinal position [m] [READ-ONLY]
 -x                    Horizontal offset [um]
 -y                    Vertical offset [um]
 -xp                   Horizontal offset in angle [urad]
 -yp                   Vertical offset in angle [urad]
 -roll                 Roll angle [urad]
 -length               Element length [m]
 -synrad               Synchrotron Radiation emission [BOOL]
 -six_dim              Longitudinal motion (6d) [BOOL]
 -thin_lens            Number of steps for Thin Lens approximation [INT]
 -e0                   Reference energy [GeV]
 -aperture_x           Horizontal aperture [m]
 -aperture_y           Vertical aperture [m]
 -aperture_losses      Fraction of lost particles [#] [READ-ONLY]
 -aperture_shape       Aperture shape [STRING]
 -tclcall_entrance     Tcl/Tk callback at element entrance [STRING]
 -tclcall_exit         Tcl/Tk callback at element exit [STRING]
 -strength             Integrated quadrupole strength [GeV/m]
 -tilt                 Tilt angle [rad]
 -tilt_deg             Tilt angle [deg]
 -Kn                   Multipolar error strength [GeV/m^(type-1)]
 -type                 Multipolar error order (3: sextupole, 4: octupole, ...) [INT]
 -hcorrector           Horizontal correction leverage [STRING] [READ-ONLY]
 -hcorrector_step_size Horizontal corrector step size [DOUBLE]
 -vcorrector           Vertical correction leverage [STRING] [READ-ONLY]
 -vcorrector_step_size Vertical corrector step size [DOUBLE]
\end{verbatim}

For each \placet{} element or command the Help lists all the possible input
parameter the user can specify, when he is build-up and/or modifying his lattice.   

\subsection{Program Structure}
All simulations consist usually of several steps. First, the beamline has to
be defined, then the beams to be used for beam-based alignment and tracking.
Then the prealignment and beam based correction method have to be specified.

The first step consists of defining and placing elements by using commands as

  \begin{verbatim}
    Quadrupole -length 0.1 -strength 2.0
    Drift -length 1.0
  \end{verbatim}

In the second step the beams are defined using commands like

  \begin{verbatim}
    DriveBeam beam0 -bunches 1 \
    ...
  \end{verbatim}

In the following the twiss parameters of the lattice can for example be plotted
using

  \begin{verbatim}
    TwissPlot beam0 -file twiss.dat
  \end{verbatim}

Or, the effect of the beam-based correction scheme can be investigated for a
hundred machines using

  \begin{verbatim}
 TestSimple Correction -beam beam0 -machines 100 -emitt_file emitt.dat
  \end{verbatim}

\subsection{Simple Example}

\begin{figure}
  \epsfbox{twiss}
  \caption{The horizontal beta-function for the simple example.}
  \label{f:ex-twiss}
\end{figure}

Here, a simple example command file for the program is shown. It creates a
lattice of 10 FODO-cells with a quadrupole spacing of $3\u{m}$ and a
quadrupole length of $10\u{cm}$. The integrated focal strength of the
quadrupoles is $0.5\u{m^{-1}}$ for a $1\u{GeV}$ particle. A beam is
created with an energy of $1\u{GeV}$ and used to write the beta-functions
along the beam line into the file {\tt twiss.dat}. The initial
twiss parameters are calculated using the command {\tt MatchFodo}.
The only input needed is the file {\tt beam.dat} which contains the wakefields
information for the beam. For the present purpose the file can be
produced using the {\tt TwissMain} command. It creates the file {\tt beam.dat}
with a bunch containing only one slice, one electron and no wakefields effects.

  \begin{verbatim}
    Girder
    Quadrupole -length 0.05 -strength 0.25
    for {set i 0} {$i<9} {incr i} {
      Girder
      Drift -length 2.9
      Quadrupole -length 0.1 -strength -0.5
      Drift -length 2.9
      Quadrupole -length 0.1 -strength 0.5
    }
    Drift -length 2.9
    Quadrupole -length 0.1 -strength -0.5
    Drift -length 2.9
    Quadrupole -length 0.05 -strength 0.25
    
    BeamlineSet
    
    array set match [MatchFodo -K1 0.5 -K2 -0.5 -l1 0.1 -l2 0.1 -L 3.0]
    
    puts [array get match]
    
    TwissMain -file beam.dat
    
    MainBeam beam0 \
    -file beam.dat \
    -bunches 1 \
    -e0 1.0 \
    -macroparticles 1 \
    -ecut 3.0 \
    -energyspread 0.1 \
    -alpha_x $match(alpha_x) \
    -beta_x $match(beta_x) \
    -emitt_x 1.0 \
    -alpha_y $match(alpha_y) \
    -beta_y $match(beta_y) \
    -emitt_y 1.0
  
    TwissPlotStep -beam beam0 -file twiss.dat
  \end{verbatim}


It can be run by typing:


  \begin{verbatim}
    $.> placet SimpleExample.tcl
  \end{verbatim}

{\tt BeamLineSet} set the lattice that has been created before and display 
the kind and the number of elements of the lattice, while {puts [array get match]}
displays the resulting twiss parameters evaluated by the {\tt MatchFodo} command
(see \S~\ref{match}).\\
The result of this command file is stored in the file {\tt twiss.dat}
(see \S~\ref{twiss} for twiss plot functions). 
The horizontal beta-function can be plotted with gnuplot:

\begin{verbatim}
    $.> gnuplot
    G N U P L O T
    Version 4.0 patchlevel 0
    last modified Thu Apr 15 14:44:22 CEST 2004
    System: Linux 2.6.9-78.0.1.EL.cernsmp
    
    Copyright (C) 1986 - 1993, 1998, 2004
    Thomas Williams, Colin Kelley and many others
    
    This is gnuplot version 4.0.  Please refer to the documentation
    for command syntax changes.  The old syntax will be accepted
    throughout the 4.0 series, but all save files use the new syntax.
    
    Type `help` to access the on-line reference manual.
    The gnuplot FAQ is available from
    http://www.gnuplot.info/faq/
    
    Send comments and requests for help to
    <gnuplot-info@lists.sourceforge.net>
    Send bugs, suggestions and mods to
    <gnuplot-bugs@lists.sourceforge.net>
    
    
    Terminal type set to 'x11'
    gnuplot>  plot 'twiss.dat' u 2:6  ti "beta_x" w l
  \end{verbatim}


This will produce a plot as shown in Fig.~\ref{f:ex-twiss}
(without labels on the axis).

\subsection{Output Files}

The {\tt emitt.dat} file is produced by {\tt TestNoCorrection} command.
It contains 10 columns with the following informations:
\begin{itemize}
\item[1.] index of the quadrupoles
\item[2.] average horizontal emittance [10$^{-7}$ m]
\item[3.] $\sigma_{\epsilon_x}$ of the average horizontal emittance (always zero)
\item[4.] average horizontal trajectory
\item[5.] always 0
\item[6.] average vertical emittance [10$^{-7}$ m] 
\item[7.] $\sigma_{\epsilon_y}$ of the average vertical emittance (always zero)
\item[8.] beam envelop defined as $\sqrt{max[x^2_i+ y^2_i]}$
\item[9.] always 0
\item[10.] number of machines     
\end{itemize} 
Starting from version 0.99.05, the possibility to customise the emittance file
has been added. All commands that can generate such a file, accept now
the option '-format' that allows to specify which columns should be
put in the file. The possible 'tokens' are:
\begin{description}
\item[\%ex]   emittance x [10$^{-7}$ m]
\item[\%ey]   emittance y [10$^{-7}$ m]
\item[\%sex]  sigma emittance x [10$^{-7}$ m]
\item[\%sey]  sigma emittance y [10$^{-7}$ m]
\item[\%x]    average x [um]
\item[\%y]    average y [um]
\item[\%xp]   average xp [urad]
\item[\%yp]   average yp [urad]
\item[\%Env]    envelope [um]
\item[\%sx]   sigma x [um]
\item[\%sy]   sigma y [um]
\item[\%sxp]  sigma xp [urad]
\item[\%syp]  sigma yp [urad]
\item[\%E]    average energy [GeV]
\item[\%dE]    energy spread [GeV]
\item[\%s]    element s position [m]
\item[\%n]    number of machines [\#]
\item[\%name] element name
\end{description}
For instance, the previously described emittance file is obtained
using the format string: 
\begin{verbatim}
TestNoCorrection -beam beam0 -emitt emitt.dat\
    -format "%s %ex %sex %x 0 %ey %sey %Env 0 %n"
\end{verbatim}
which in fact is the default. A description of each column is added at the beginning of the file.

The {\tt BeamSaveAll} command produces a file with the parameters
of a sliced beam, one row for each bunch, slice and macroparticles
(line separation between each bunch).
\begin{itemize}
\item[1.]  s  long position along [um]
\item[2.]  weight [-]
\item[3.]  energy [GeV]
\item[4.]  x   [um]       
\item[5.]  x'  [um/m]
\item[6.]  y   [um]   
\item[7.]  y'  [um/m]
\item[8.]  $\sigma_x * \sigma_x$
\item[9.]  $\sigma_x * \sigma_x'$
\item[10.] $\sigma_x'* \sigma_x'$
\item[11.] $\sigma_y * \sigma_y$
\item[12.] $\sigma_y * \sigma_y'$
\item[13.] $\sigma_y'* \sigma_y'$
\item[14.] $\sigma_x * \sigma_y $ (always 0) 
\item[15.] ... (always 0)
\item[16.] ... (always 0)
\item[17.] ... (always 0)
\end{itemize}
Columns 8-17 are the sigmas, sigma-matrix with the transverse distribution for each macroparticle.\\


The {\tt BeamDump} command produces a file with the parameters
of a particles beam with the following informations:
\begin{itemize}
\item[1.] energy [GeV] 
\item[2.] x   [um]
\item[3.] y   [um]
\item[4.] z   [um]
\item[5.] x'  [urad]
\item[6.] y'  [urad]
\end{itemize}
If the {\tt -axisx} and the {\tt -axisy} option are different from zero
the mean x,y offsets and angles are removed.

\subsection{Tips}

Although \placet{} does not currently have a command-line history or tab completion, the simple tool rlwrap can provide a similar behaviour. Just start \placet{} with:

\begin{verbatim}
rlwrap -c placet
\end{verbatim}