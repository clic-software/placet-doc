Defining a lattice is usually the first step of any simulation.
With \placet{} three different types of beamlines can be simulated. Firstly,
a normal main linac where the structure length is very small compared to the
beta-function (the main linac of CLIC). Secondly, an accelerator where the 
structure length is comparable to the beta-function (the drive beam accelerator
of CLIC). And at last a decelerator with a high group velocity inside the
structures (the drive beam decelerator of CLIC). In the first
case one can assume that the beam position does not change significantly
inside the structure while in the second case each structure consists of cells
with a number of transverse dipole and quadrupole modes. It is assumed that
these modes do not couple to one another and that the wakefield kick is located
in the cell. In the last case a single longitudinal and a single transverse
mode are assumed but the group velocity is taken into account for both.
Currently the three models cannot be mixed but this is planned as a future
extension.

\input{dec}

\subsection{Setting up the Lattice}
Two possibilities exist to create the lattice, one can either read a complete
description file for the main beamline or create and place the elements
individually. The first is done using the command {\tt SetupMainBeamline}. In
the second case one creates the elements one after another starting from the
injection point. For each module the girder has to be created using the command
{\tt Girder}, then all the elements are placed using {\tt Bpm},
{\tt Quadrupole}, {\tt Drift} and {\tt Cavity} commands. The length of the
girder is automatically adjusted to match the total length of all elements it
supports. Other supported elements can be found in (\S~\ref{first-chapter}).

\input{girder}

\input{quadrupole}

\input{multipole}

\input{sbend}

\input{drift}

\input{bpm}

\input{cavity}

\input{dipole}

\input{quadbpm}

\input{beamlineset}

\subsection{Halo and tails studies}

The following option are common to all elements:

\begin{itemize}
  \item[-] gas\_A                Atomic number [amu]
  \item[-] gas\_Z                Atomic charge [e]
  \item[-] gas\_pressure         Gas pressure [torr]
  \item[-] gas\_temperature      Gas temperature [K]
  \item[-] gas\_minumum\_theta    Minimum scattering angle [rad]
  \item[-] radiation\_length     Radiation length [m]
\end{itemize}

Halo particles in linear colliders can result in significant losses and serious back-ground which 
may reduce the overall performances. Basically HTGEN, is a Monte carlo halo and tail generator, 
which used the random number particle generator, together with the residual gas parameters and the 
scattering processes. To track the halo through beamline, we usually need to define residual gas 
(eg., Nitrogen, CO) in the beam pipe with some specific parameters. Atomic number and atomic weight 
of the gas. An ultra small gas pressure(10 ntorr in BDS) is used at normal temperature 300K. While 
the radiation length of the gas is important for bremsstrahlung (inelastic beam-gas scattering), 
which causes the loss of the beam energy in terms of radiated photons. Angular divergence is the 
minimum scattering angle which strongly depends on the beam-gas elastic scattering cross-section.

Following process are included up to now in HTGEN.

\begin{itemize}
\item[1.] Beam-gas elastic scattering (Mott)
\item[2.] beam-gas inelastic scattering (bremsstrahlung)
\item[3.] Synchrotron Radiation 
\end{itemize}

Futher documentation can be find at the following URL:
http://hbu.home.cern.ch/hbu/HTGEN.html.

\subsection{Coherent Synchrotron Radiation studies}

A module for the simulation of Coherent Synchrotron
Radiation emission (CSR) has been implemented. The
methodology follows closely the implementation of Elegant
[7], which is based on the the work of [8]. This
methodology uses a 1D CSR model and does not take into
account shielding, but it has the advantage that it correctly
calculates the transient build-up of the CSR in a bend. The
\placet{} model has currently implemented CSR in the bending
magnets, plus a simple field exponential decay model
for the drift spaces(update to better models is planned in
the near future). The module is very easy to use, as CSR
is enabled simply by setting a parameters for the SBend
element benchmarking show excellent agreement between \placet{} and Elegant
for CSR in bends and at high energies (for energies below
$\sim$ 100 MeV, the results differs substantially due to the v $=$ c 
assumption of \placet{}).

The option available for the Sbend element are:

\begin{itemize}
  \item[-] csr                      Coherent Synchrotron Radiation (CSR) [BOOL]
  \item[-] csr\_charge               CSR: [REQUIRED] total charge of input distribution [C]
  \item[-] csr\_enable\_driftwake     Enable csr wake propagation into trailing drift [BOOL]
     drift wake will be applied until 1 \% remains or until next SBend
  \item[-] csr\_nbins                CSR: \# of bins ( $>=$ 10 ) [\#]
  \item[-] csr\_nsectors             CSR: \# of dipole sectors ( $>=$ 1 )  [\#]
  \item[-] csr\_nhalffilter          CSR: Savitzky$-$Golay filter half$-$width ( $>=$ 1 )  [\# of bins]
  \item[-] csr\_filterorder          CSR: Savitzky$-$Golay filter order ( $>=$ 1 ), 1 implies MA  [-]
  \item[-] csr\_savesectors          CSR: save data for each sector  [nbin  s   lambda   dlambda   dE\_ds [GeV/m] ]   [BOOL]
  \item[-] csr\_attenuation\_length   CSR: attenuation length for csr drift (default value is 1.5*overtaking length) [m]
  \item[-] csr\_enforce\_steady\_state CSR: enforce steady state mode (infinite slippage length)  [BOOL]
\end{itemize}


[7] M. Borland; Argonne National Laboratory Advanced; Photon
Source Report No. LS-287, 2000.

[8] E. L. Saldin et al., ``On the coherent radiation of an electron
bunch moving in an arc of a circle''; NIM A 398 (1997) 392.

%\begin{clist}
%\command Girder \\
%Creates a new girder to place the elements created in the following.

%\command Quadrupole \\
%Creates a quadrupole and places it on the current girder.
%\opf synrad If not zero, the particles emit synchrotron radiation in the
%quadrupole.
%\opf length Length of the quadrupole in $\u{m}$.
%\opf strength Integrated strength of the quadrupole in $\u{m^{-1}}$ for a
%1\u{GeV} particle.

%\command Multipole \\
%Creates a multipole and places it on the current girder.
%\opi type Order of the multipole; 2=quadrupole, 3=sextupole etc..
%\opf tilt Rotation of the multipole.
%\opf synrad If not zero, the particles emit synchrotron radiation in the
%multipole.
%\opf length Length of the multipole in $\u{m}$.
%\opf strength Integrated strength of the multipole in $\u{m^{1-{\tt type}}}$
%for a 1\u{GeV} particle.

%\command Sbend \\
%Creates a bend and places it on the current girder.
%\opf angle Bending angle in radian.
%\opf length Length of the bend in $\u{m}$.
%\opf e0 Reference energy in \u{GeV} at which the bending angle is achieved. 
%\opi synrad If not zero, the particles emit synchrotron radiation in the
%bend.

%\command Drift \\
%Creates a drift and places it on the current girder.
%\opf Length length of the drift in $\u{m}$.

%\command Bpm \\
%Creates a BPM and places it on the current girder.
%\opf Length length of the BPM in $\u{m}$.

%\nix{
%\command Cavity \\
%Creates an accelerating  cavity and places it on the current girder.
%\opfr Length length of the cavity in $\u{m}$.

%\command Solcav \\
%Creates a solenoid with acceleration and places it one the current girder.
%\opfr length The length of the solenoid in $\u{m}$.
%\opfr bz The strength of the solenoid in $\u{T}$.
%\opfr v1 The strength of the solenoid in $\u{T}$.
%}

%\command DecCavity \\
%Creates a decelerating cavity and places it on the current girder.
%\opfr Length length of the cavity in $\u{m}$.
%\opf v1 Determines the azimuthal rotation angle of the structure in radian.
%Is only needed if the field non-uniformity is simulated. {\it the name will
%change to phi}

%\command Dipole \\
%Creates a correction dipole and places it one the current girder.
%\opfr length The length of the dipole in $\u{m}$.

%\command Solenoid \\
%Creates a solenoid and places it one the current girder.
%\opfr length The length of the solenoid in $\u{m}$.
%\opfr bz The strength of the solenoid in $\u{T}$.

%\command QuadBpm \\
%Creates a quadrupole with a BPM in its centre and places it one the current
%girder. \opfr length The length of the solenoid in $\u{m}$.
%\opfr strength The strength of the solenoid in $\u{T/m}$.

%\command BeamlineSet \\
%Fixes the beamline. This command is used to do some initial calculations, it
%must be called once but only once in run.

%\end{clist}

\subsection{Matching}
In order to create beams the initial twiss parameter of a lattice have to be
known. Two commands exist to match to a periodic lattice, {\tt MatchFodo} and
{\tt MatchTriplet}. With two other commands ({\tt TwissPlot} and
{\tt TwissPlotStep}) the twiss parameters can be plotted along the beamline.

\subsubsection{Matching Commands}\label{match}
\begin{clist}
\command MatchFodo \\
This command calculates the matched twiss parameters in the centre of a
quadrupole of a FODO cells. It returns a list in the form
{ \{alpha\_x {\it value}
beta\_x {\it value}
mu\_x {\it value}
alpha\_y {\it value}
beta\_y {\it value}
mu\_y {\it value}\}}. Here, {\tt beta\_x} and {\tt beta\_y} are the
horizontal and vertical beta-functions in meter, {\tt alpha\_x} and
{\tt alpha\_y} are the $\alpha$-parameters and {\tt mu\_x} and {\tt mu\_y}
are the phase advances in radian.
\opfr K1 Integrated normalized strength of the first quadrupole in
\u{m^{-1}}.
\opfr K2 Integrated normalized strength of the second quadrupole in
\u{m^{-1}}.
\opfr l1 Length of the first quadrupole in $\u{[m]}$.
\opfr l2 Length of the second quadrupole in $\u{[m]}$.
\opfr L Distance between quadrupole centres in $\u{[m]}$.

\command MatchTriplet \\
This command calculates the matched twiss parameters in the centre of a
triplet in a periodic triplet lattice. It returns data in the same way as
{\tt MatchFodo}.
\opfr K1 Integrated normalized strength of the inner quadrupole in
\u{m^{-1}}.
\opfr K2 Integrated normalized strength of the outer quadrupoles in
\u{m^{-1}}.
\opfr l1 Length of the inner quadrupole in $\u{[m]}$.
\opfr l2 Length of the outer quadrupoles in $\u{[m]}$.
\opfr L Distance between triplet centres in $\u{[m]}$.
\opfr l\_sep Distance between the inner and the outer quadrupole in \u{m}.
This value is the drift between the two not the distance between the centres.
\end{clist}

\subsection{Plotting of Twiss Parameters}\label{twiss}
The twiss parameters can be plotted with two different functions,
{\tt TwissPlot} and {\tt TwissPlotStep}. Both write a similar file, but the
first calculates the parameters only in the quadrupole centres, while the
second one plots them in each element in small steps.
The output file contains in each line the following data:
\begin{displaymath}
j(s), s, E(s), \beta_{x_m}(s), \alpha_{x_m}(s),  \beta_{x_i}(s), \alpha_{x_i}(s),\beta_{y_m}(s), \alpha_{y_m}(s),\beta_{y_i}(s), \alpha_{y_i}(s), ks
\end{displaymath}
Here, $s$ is the longitudinal position at which all the other values are
determined. $j(s)$ is the element number and $E(s)$,
$\beta_{x_m}(s)$, $\alpha_{x_m}(s)$, $\beta_{y_m}(s)$ and $\alpha_{y_m}(s)$ are the energy and
the twiss parameters of the central slice of the beam, and $\beta_{x_i}(s)$, $\alpha_{x_i}(s)$, $\beta_{y_i}(s)$ and $\alpha_{y_i}(s)$
are the twiss parameters integrated on all the slices of the beam. $E$ is given in [GeV]
and $\beta$ in [m]. $ks$ is the quadrupole strength.
\begin{clist}
\command TwissPlot \\
Plots the Twiss parameters along the beamline and stores the results in a
file or returns them as a string. The parameters are evaluated in the centre
of each quadrupole.
\ops beam
Name of the beam to be used for the calculation.
\ops file
Name of the file where the results should be stored. If no file is specified
the result is returned as a list and can be processed in the program.
Examples
\begin{verbatim}
TwissPlot -beam beam0 -file twiss.dat
set twiss [TwissPlot -beam beam0]
\end{verbatim}

\command TwissPlotStep \\
This commands corresponds to {\tt TwissPlot} except that the Twiss parameters
are plotted in each element at the entry and exit. In between they are plotted
at intervals not larger than specified by {\tt -step}.
\ops beam
Name of the beam to be used for the calculation.
\ops file
Name of the file where the results should be stored. If no file is specified
the result is returned as a list and can be processed in the program.
\opf step
The maximum step in [m] before a new value of the Twiss parameters is plotted.
In each element the value immediately after the entry and at the exit are
plotted. The positions in between are will be equidistant. The default is
$0.02\u{m}$.
\end{clist}

\subsubsection{Example}
The following example matches to a FODO-lattice with a quadrupole spacing of
2\u{m}, quadrupole lengths of $30\u{mm}$ and integrated quadrupole strengths
of $K=0.6\u{m^{-1}}$, the result is stored in the array {\tt match}.
\begin{verbatim}
array set match [MatchFodo -l1 0.3 -l2 0.3 -K1 0.6 -K2 -0.6 -L 2.0]
\end{verbatim}
The values can now be accessed via {\tt \$match(beta\_x)} as for example
\begin{verbatim}
puts "beta_x = $match(beta_x) m"
\end{verbatim}

\subsection{Example of a Lattice}
Here the decelerator lattice of CLIC should serve as an example. It consists
of simple FODO-cells with a length of $2.23\u{m}$ each. Before this part of the
program is executed, {\tt s} has been set to the normalized quadrupole strength
and {\tt e} to the initial beam energy. {\tt de} is the maximum deceleration per
FODO-cell.  In addition a DriveBeam and a DecCavity has to be defined.  
Please refer to your PREFIX/examples/decelerator.tcl for a self-consistent DriveBeam example.
\begin{verbatim}
#
# Definition of the geometry
#
set girderlength 2.23
set cavitylength 0.8
set quadrupolelength 0.14
set bpmlength 0.03
set driftlength [expr 0.25*($girderlength-2.0*$cavitylength \
        -2.0*$quadrupolelength-2.0*$bpmlength)]
#
# Implementation of the actual lattice
# starts and ends in the centre of a quadrupole
#
set first 1

for {set i 0} { $i <250} {incr i } {

    Girder
    if {$first == 0} {
        Drift -length $driftlength
        Bpm -length $bpmlength
        Quadrupole -length $quadrupolelength \
                   -strength [expr $s * $e]
    } {
        Quadrupole -length [expr 0.5*$quadrupolelength] \
                   -strength [expr 0.5 * $s * $e]
        set first 0
    }
    Drift -length $driftlength
    DecCavity -length $cavitylength -v1 $v1
    Drift -length $driftlength
    Bpm -length $bpmlength
    Quadrupole -length $quadrupolelength \
               -strength [expr -$s * ($e-0.5*$de)]
    Drift -length $driftlength
    DecCavity -length $cavitylength -v1 $v2
    set e [expr $e - $de]
}

Drift -length $driftlength
Bpm -length $bpmlength
Quadrupole -length [expr 0.5*$quadrupolelength] \
        -strength [expr 0.5 * $s * $e]

#
# fix the lattice
#

BeamlineSet
\end{verbatim}

\subsection{Extracting Information from the Lattice}

The following commands can be used to extract informations from
the lattice. See {\S~\ref{first-chapter}} for valid options for these
commands.  
\begin{figure}
\epsfxsize=15cm
%\begin{rotate}{90}{\hspace*{-10cm}\epsfbox{beamline.eps}}\end{rotate}
\epsfbox{beamline}
\caption{The window created by the command {\tt BeamlineView}.}
\label{f:beamline}
\end{figure}

\begin{clist}
\command PlotFillFactor \\
Plots the variation of the integrated fill factor over the linac length.
This is useful to estimate the highest integrated fill factor for a two beam
accelerator.
\ops file name of the file where to store the result, if not set the results
will be printed to stdout.

\command BeamlineInfo \\
Returns a list containing information about the beamline. This can be stored
in a Tcl array using {\tt array set name [BeamlineInfo]}. The following
indexes are available:
\begin{itemize}
\setlength{\labelsep}{2mm}
\item {\tt length:} the total length of the beamline in $\u{[m]}$
\item {\tt n\_quadrupole:} the number of quadrupoles in the beamline
\item {\tt n\_bpm:} the number of BPMs in the beamline
\item {\tt n\_cavity:} the number of cavities in the beamline
\item {\tt n\_drift:} the number of drifts in the beamline
\item {\tt n\_element:} the total number of elements in the beamline
\end{itemize}

\command BeamlineView \\
This command allows to investigate the current status of the beamline
It opens a window in which the beamline elements are plotted, see
Fig.~\ref{f:beamline}. Each type can
individually be switched on and off and the transverse resolutions can
be adjusted to the requirements. This command can be used as a callback
function in any element along the line. If one clicks on an element the
vertical position is displayed. The resolution for this depends on the
resolution of the window.

The scaling of the horizontal and vertical
axis can be changed by clicking into the fields corresponding to
{\tt sscale} and {\tt yscale}, respectively. After changing the values one has
to type return for the change to become active. Also the centre of the
vertical can be varied by adjusting {\tt y0}. The slider can be used to
change the region of the beam line viewed.
With the checkboxes the different element types can be switched on or off.

\command BeamShow \\
This command shows the beam in a window. It is used by {\tt BeamViewFilm}
and {\tt BeamView} but can also be incorporated into new graphical
routines.

\command BeamViewFilm \\
This command shows the current status of a bunch train. It can be used as a
callback function in any element and displays the positions of all bunches
as they pass this element. Normally this function is attached to a number of
elements (e.g. all quadrupoles) thus showing the time evolution of the
train.

\command WakeSet %\\
This command produces a wakefield set consisting of the wavelengths, the
loss factors and the damping of each mode. It creates a new command with the
name of the set to evaluate the wakefields at any given point.
Example:
\begin{quote}
WakeSet wake {1.0 0.5 100 1.1 0.5 100}
\end{quote}
creates a wakefield with two modes having wavelengths of $1.0$ and $1.1\u{m}$
a loss factor of $0.5\u{V/(pCm^2)}$ and $Q=100$ each. With
\begin{quote}
puts [wake 0.7]
\end{quote}
prints the wakefield $0.7\u{m}$ behind the driving particle.

\command TestSpectrum %\\
calculates the dependence of the sensitivity of the emittance growth onto the
wavelength of the prealignment error. The result is the amplitude the
error may have to create only $6\u{\%}$ emittance growth.
%\ops file
%name of the file where to store the results.
%\opf lambda0
%minimal wavelength to be used.
%\opf lambda1
%maximal wavelength to be used.
%\opi steps
%number of steps to be used.
% The interval between the two wavelength is divided logarithmically.

\command TestSpectrumSimpleCorrection %\\
calculates the dependence of the sensitivity of the emittance growth onto the
wavelength of the prealignment error and applies one-to-one correction. The result 
is the amplitude the error may have to create only $6\u{\%}$ emittance growth.
%\ops file
%name of the file where to store the results.
%\opf lambda0
%minimal wavelength to be used.
%\opf lambda1
%maximal wavelength to be used.
%\opi steps
%number of steps to be used. The interval between the two wavelength is divided
%logarithmically.

\end{clist}

\subsection{Energy Related Commands}
In order to minimize the effect of transverse wakefields the so-called
BNS-damping can be used. This can be done by introducing and energy spread in
the bunches. The command {\tt BnsPlot} allows to plot the spread required to
fulfill the damping condition. The command {\tt EnergySpreadPlot} allows to
plot the actual energy spread of the beam along the beamline.
The other three commends are usually used for investigating the drive beam.\\
%\begin{clist}
%\command BnsPlot %\\

\input{bnsplot}
%Plots the energy spread required for BNS-damping along the beamline.\\
%\ops beam
%name of the beam to be used for the calculation
%\ops file
%name of the file where to store the results.

\input{energyspreadplot}
%\command EnergySpreadPlot %\\
%Plots the energy spread along the beamline.\\
%\ops beam
%name of the beam to be used for the calculation
%\ops file
%name of the file where to store the results.

\input{beamenergyplot}
%\command BeamEnergyPlot %\\
%Prints the total beam energy along the beamline into a file. \\
%\ops beam
%name of the beam to be used for the calculation
%\ops file
%file where to store the results

\input{beamenergyprint}
%\command BeamEnergyPrint %\\
%Prints the total beam energy of the current beam to stdout. \\

\input{beamenergyprofileplot}
%\command BeamEnergyProfilePlot %\\
%Prints the maximum, minimum and mean energy of each bunch in the current beam.\\
%\ops file
%name of the file where the values should be stored
%\end{clist}
