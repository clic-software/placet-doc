<?xml version="1.0"?>
<!--
    images-floats.mod.xsl - images inclusion and floats handling.
	$Id: images-floats.mod.xsl,v 1.1 2008/04/07 09:10:35 alatina Exp $	

    Copyright © 2004, 2005 Torsten Bronger <bronger@physik.rwth-aachen.de>.

    This file is part of texi2latex.

    texi2latex is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License as published by the Free
    Software Foundation; either version 2 of the License, or (at your option)
    any later version.

    texi2latex is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with texi2latex; if not, write to the Free Software Foundation, Inc., 59
    Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- First (and much smaller) part: Images.  If they are alone (i.e., not in a
     float), they are embedded into a {center} environment.  As far as I can
     see, there are no inline images in Texinfo. -->

<xsl:template match="image">
  <xsl:if test="not(parent::float)">
    <xsl:text>&#10;\begin{center}</xsl:text>
  </xsl:if>
  <xsl:text>&#10;\includegraphics</xsl:text>
  <!-- Handle optional \includegraphics arguments -->
  <xsl:for-each select="@width[normalize-space(.) != ''] | @height[normalize-space(.) != '']">
    <xsl:if test="position() = 1">
      <xsl:text>[</xsl:text>
    </xsl:if>
    <xsl:apply-templates select="."/>
    <xsl:choose>
      <xsl:when test="position() = last()">
        <xsl:text>]</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>,</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:for-each>
  <xsl:text>{</xsl:text>
  <!-- Extension is *always* included.  This may be a bad idea.  But since the
       extension is mostly ommitted anyway, I don't care really. -->
  <xsl:value-of select="concat(@name,@extension)"/>
  <xsl:text>}&#10;</xsl:text>
  <!-- No general apply-templates ==> alttext is gobbled -->
  <xsl:if test="not(parent::float)">
    <xsl:text>\end{center}&#10;</xsl:text>
  </xsl:if>
</xsl:template>

<xsl:template match="image/@height | image/@width">
  <xsl:value-of select="concat(local-name(.),'=',string(.))"/>
</xsl:template>

<!-- Now for the second part: Floats.  Their representation in Texinfo is
     rather similar to LaTeX, which makes it a little bit more simple.
     However, they are not the same.  For example, there can be an arbitrary
     number of float type, not only figure and table.  This must be done by
     the float package.  Moreover, we have the situation of captions without
     numbers (because no label was given), and numbers without caption (which
     can be done easily with caption v3; the problem is the colon after the
     number that I have to suppress).

     Last but not least there are "type-less" floats.  I call them in
     "miscellaneous" here.  (Name clashs are impossible because all new float
     types get a "float" prefix.)  For "miscellaneous", the float type must be
     suppressed; so only number and caption, if any. -->

<xsl:template match="float">
  <xsl:variable name="float-type">
    <xsl:call-template name="create-normalized-float-type">
      <xsl:with-param name="float-type" select="floattype"/>
    </xsl:call-template>
  </xsl:variable>
  <xsl:value-of select="concat('&#10;\begin{',$float-type,'}')"/>
  <xsl:apply-templates select="floatpos"/>
  <xsl:text>&#10;\centering&#10;</xsl:text>
  <xsl:apply-templates select="*[not(self::floatpos or self::floattype or 
                                     self::caption or self::shortcaption)]"/>
  <xsl:call-template name="insert-caption"/>
  <xsl:apply-templates select="@name"/>     <!-- label -->
  <xsl:value-of select="concat('&#10;\end{',$float-type,'}&#10;')"/>
</xsl:template>

<!-- This is not yet implemented in Texinfo, but the XML output contains it
     already and Karl Berry told me that its format will be the same as in
     LaTeX.  So I can just pass it here. -->

<!-- By the way, you can see a general problem of Texinfo's XML format here:
     Often attributes and even elements are generated but empty, and therefore
     I have to ignore them.  But in order to ignore them, I must test for
     them, with normalize-space() = '' and thing like this.  You will see this
     in various places in texi2latex's code. -->

<xsl:template match="floatpos">
  <xsl:if test="normalize-space(.) != ''">
    <xsl:text>[</xsl:text>
    <xsl:value-of select="."/>
    <xsl:text>]</xsl:text>
  </xsl:if>
</xsl:template>

<!-- Generally, there are two cases for caption (realised in the xsl:choose):
     Either is has a label and thus a number, or it is just a text under the
     float.  But I only add a caption here, not the label.  Please note that
     this is not a template but a routine, because I need to add a caption
     even if no <caption> element is in the XML (when you need a superfluous
     element in the XML, it isn't there ;). -->

<xsl:template name="insert-caption">
  <xsl:variable name="caption">
    <xsl:apply-templates select="caption"/>
  </xsl:variable>
  <xsl:variable name="short-caption">
    <xsl:apply-templates select="shortcaption"/>
  </xsl:variable>
  <xsl:variable name="with-label" select="normalize-space(@name) != ''"/>
  <xsl:choose>
    <xsl:when test="$with-label">
      <xsl:if test="normalize-space($caption) = ''">
        <xsl:text>\par</xsl:text>
        <xsl:text>\renewcommand{\captionlabeldelim}{}</xsl:text>
        <xsl:text>\renewcommand{\captionlabelsep}{}</xsl:text>
      </xsl:if>
      <xsl:value-of select="concat('\caption',$short-caption,'{',$caption,'}&#10;')"/>
    </xsl:when>
    <xsl:when test="$caption != ''">
      <xsl:value-of select="concat('\captionlabelfalse&#10;\makeatletter&#10;',
                                   '\@makecaption{}{',$caption,'}%&#10;',
                                   '\makeatother&#10;')"/>
    </xsl:when>
  </xsl:choose>
</xsl:template>

<!-- The result of the shortcaption template already contains the brackets
     that makes it the optional argument of \caption. -->

<xsl:template match="shortcaption">
  <xsl:text>[</xsl:text>
  <xsl:apply-templates/>
  <xsl:text>]</xsl:text>
</xsl:template>

<!-- This adds the \label command after the caption. -->

<xsl:template match="float/@name[normalize-space(.) != '']">
  <xsl:text>\label{</xsl:text>
  <xsl:call-template name="clean-up-identifier">
    <xsl:with-param name="identifier" select="."/>
  </xsl:call-template>
  <xsl:text>}&#10;</xsl:text>
</xsl:template>

<!-- Note that this \nakedlistof command doesn't generate any heading but
     *only* the list.  This fits into Texinfo's structure, because there the
     headings is explicit in contrast to LaTeX.  (By the way, Texinfo does it
     better in my opinion.) -->

<xsl:template match="listoffloats">
  <xsl:variable name="float-type-normalized">
    <xsl:call-template name="create-normalized-float-type">
      <xsl:with-param name="float-type" select="@type"/>
    </xsl:call-template>
  </xsl:variable>
  <xsl:value-of select="concat('\nakedlistof{',$float-type-normalized,'}{',
                               'List of ',@type,'s}&#10;')"/>
</xsl:template>

<!-- This is an important routine used in the others here: I take the original
     float type specified in the Texinfo XML file and convert it to a proper
     LaTeX float environment name.  For this, the following rules apply:

     1.  The name is converted to lowercase, all spaces become "-", and other
         means of normalization.

     2.  If the name is "figure" or "table", it is left like this, because
         these two environments exist obviously, and I assume that using them
         minimises possible trouble.  (Mostly floats will actually be figures
         or tables after all.)

     3.  If the name is empty, it becomes the internal "miscellaneous",
         otherwise just a "float" is prepended. -->

<xsl:template name="create-normalized-float-type">
  <xsl:param name="float-type"/>
  <xsl:variable name="float-type-raw" select="normalize-space($float-type)"/>
  <xsl:choose>
    <xsl:when test="$float-type-raw = ''">
      <xsl:text>miscellaneous</xsl:text>
    </xsl:when>
    <xsl:when test="$float-type-raw = 'Figure'">
      <xsl:text>figure</xsl:text>
    </xsl:when>
    <xsl:when test="$float-type-raw = 'Table'">
      <xsl:text>table</xsl:text>
    </xsl:when>
    <xsl:otherwise>
      <xsl:text>float</xsl:text>  <!-- prepend float -->
      <xsl:call-template name="clean-up-identifier">
        <xsl:with-param name="identifier" select="$float-type-raw"/>
      </xsl:call-template>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!-- Routines for creating the initialisation of non-standard float types in
     the preamble, using comminds of the float package. -->

<!-- First collect all occuring types (including duplicates) in one big list
     each delimited by '|'. -->

<xsl:template name="insert-float-styles">
  <xsl:variable name="complete-type-list">
    <xsl:for-each select="//floattype">
      <xsl:value-of select="concat('|',normalize-space(.),'|')"/>
    </xsl:for-each>
  </xsl:variable>
  <xsl:call-template name="interpret-float-style-list">
    <xsl:with-param name="list" select="$complete-type-list"/>
  </xsl:call-template>
</xsl:template>

<!-- This is the recursive routine of the procedure: Create a \newfloat for
     each new float, but now ignore duplicates and "figure" and "table". -->

<!-- FixMe: Are re-definitions of "miscellaneous" really avoided?  Isn't it
     necessary to add a test of empty loat type to the above routine?  And
     isn't it wise to use create-normalized-float-type instead of an ordinary
     clean-up-identifier and simplify the xsl:if below greatly? -->

<xsl:template name="interpret-float-style-list">
  <xsl:param name="list"/>
  <xsl:param name="already-done"/>
  <xsl:if test="$list != ''">
    <xsl:variable name="first-item"
      select="substring-before(substring($list,2),'|')"/>
    <xsl:variable name="first-item-normalized">
      <xsl:call-template name="clean-up-identifier">
        <xsl:with-param name="identifier" select="$first-item"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="rest"
      select="substring-after(substring($list,2),'|')"/>
    <xsl:if test="$first-item != 'Figure' and $first-item != 'Table' and
                  $first-item != 'figure' and $first-item != 'table' and
                  not(contains($already-done,concat('|',$first-item,'|')))">
      <xsl:value-of select="concat('\newfloat{float',$first-item-normalized,'}{tb}{',
                            $first-item-normalized,'}[chapter]&#10;')"/>
      <xsl:value-of select="concat('\floatname{float',$first-item-normalized,'}{',
                            $first-item,'}&#10;')"/>
    </xsl:if>
    <xsl:call-template name="interpret-float-style-list">
      <xsl:with-param name="list" select="$rest"/>
      <xsl:with-param name="already-done" select="concat($already-done,'|',$first-item,'|')"/>
    </xsl:call-template>
  </xsl:if>
</xsl:template>

</xsl:stylesheet>
