<?xml version="1.0"?>
<!--
    index.mod.xsl - concept index and other indices.
	$Id: index.mod.xsl,v 1.1 2008/04/07 09:10:35 alatina Exp $	

    Copyright © 2004, 2005 Torsten Bronger <bronger@physik.rwth-aachen.de>.

    This file is part of texi2latex.

    texi2latex is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License as published by the Free
    Software Foundation; either version 2 of the License, or (at your option)
    any later version.

    texi2latex is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with texi2latex; if not, write to the Free Software Foundation, Inc., 59
    Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- It's harmful to insert \index commands if not index is actually printed.
     The reason is that if there is no index, no index package is included by
     preamble.mod.xsl.  However, in this case all "_" within \index commands
     generate an error message.  I don't know why, but I don't object to
     avoiding \index without <printindex> anyway. -->

<xsl:variable name="index-available" select="//printindex"/>

<!-- A straightforward template: Map the indexterm element to an \index macro.
     However, have in mind that the index package is loaded.  So \index has an
     optional command for the index type, and this argument is always
     generated.

     I generate two variales, $sort-key and $contents, with the only
     difference than contents contains markup.  If both are the same, one
     incarnation becomes the argument of the \index macro, otherwise both are
     used, with an '@' inbetween.  This is the usual makeindex @-notation.

     Last but not least, it's important to see that one one of the pre-defined
     index types, namely the concept index "cp", has a markup in roman
     typeface.  All others are printes in a typewriter font.  This is also
     true if there is a synindex mapping pointing to a typewriter-class
     index. -->

<!-- FixMe: Totally new, user-defined indices that are not mapped to
     pre-defined ones seem to be ignored at the moment. -->

<xsl:template match="indexterm">
  <xsl:if test="$index-available">
    <xsl:variable name="sort-key" select="concat($start-delimiter,.,$end-delimiter)"/>
    <xsl:variable name="contents">
      <xsl:choose>
        <xsl:when test="contains('|fn|vr|tp|ky|pg|',concat('|',@index,'|')) or 
                        /texinfo/synindex[@from = current()/@index]/@code = 'yes'">
          <xsl:text>\texttt{</xsl:text>
          <xsl:apply-templates/>
          <xsl:text>}</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:apply-templates/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:text>\index</xsl:text>
    <xsl:apply-templates select="@index"/>
    <xsl:text>{</xsl:text>
    <xsl:if test="$contents != $sort-key">
      <xsl:value-of select="concat($sort-key,'_')"/>
    </xsl:if>
    <xsl:value-of select="$contents"/>
    <xsl:text>}</xsl:text>
  </xsl:if>
</xsl:template>

<!-- Here I compute the optional argument for \index, which usually is a
     two-letter index code.  Here, too, I oney synindex mappings. -->

<xsl:template match="indexterm/@index">
  <xsl:variable name="index" select="normalize-space(.)"/>
  <xsl:if test="$index != ''">
    <xsl:text>[</xsl:text>
    <xsl:choose>
      <xsl:when test="/texinfo/synindex[@from = $index]">
        <xsl:value-of select="/texinfo/synindex[@from = $index]/@to"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$index"/>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text>]</xsl:text>
  </xsl:if>
</xsl:template>

<!-- The following is similar to floats: For the LaTeX preamble, I have to
     insert initialisation commands for all the new indices using commands of
     the index package.

     First, I collect all of them, with duplicates. -->

<xsl:template name="insert-index-types">
  <xsl:variable name="complete-type-list">
    <xsl:for-each select="//indexterm">
      <xsl:value-of select="concat('|',normalize-space(@index),'|')"/>
    </xsl:for-each>
  </xsl:variable>
  <xsl:call-template name="interpret-index-style-list">
    <xsl:with-param name="list" select="$complete-type-list"/>
  </xsl:call-template>
</xsl:template>

<!-- Then, in this recursive routine, I create the initialisation commands.
     However, I drop duplicates here and insert only indices that are not
     mapped to something else.  You see, it is *very* similar to floats.  One
     important difference is that I include all indices, because LaTeX has no
     predefined ones.

     Additionally, I write a makeindex call to a file called makeinfo.bat that
     can later be used e.g. as a bash script to process all indices
     easily. -->

<xsl:template name="interpret-index-style-list">
  <xsl:param name="list"/>
  <xsl:param name="already-done"/>
  <xsl:if test="$list != ''">
    <xsl:variable name="first-item"
      select="substring-before(substring($list,2),'|')"/>
    <xsl:variable name="rest"
      select="substring-after(substring($list,2),'|')"/>
    <!-- The following two nested if-tests could be done in one, but the first
         will be so oten 'false' that I want to be *sure* that the second
         isn't tested any more in this case. -->
    <xsl:if test="not(contains($already-done,concat('|',$first-item,'|')))">
      <xsl:if test="not(/texinfo/synindex[@from = $first-item])">
        <xsl:value-of select="concat('\newindex{',$first-item,'}{',$first-item,'x}{',
                                     $first-item,'d}{',$first-item,' Index}')"/>
        <xsl:value-of select="concat('    \immediate\write15{makeindex -s texi2latex.ist ',
                                     '-o \jobname.',$first-item,'d \jobname.',$first-item,
                                     'x}&#10;')"/>
      </xsl:if>
    </xsl:if>
    <xsl:call-template name="interpret-index-style-list">
      <xsl:with-param name="list" select="$rest"/>
      <xsl:with-param name="already-done" select="concat($already-done,'|',$first-item,'|')"/>
    </xsl:call-template>
  </xsl:if>
</xsl:template>

<!-- This one is obviously simple, but even here we have the new optional
     index type argument introduced by the index package. -->

<!-- FixMe: Is it possible to have indices here that were mapped to something
     else? -->

<xsl:template match="printindex">
  <xsl:text>\printindex[</xsl:text>
  <xsl:value-of select="."/>
  <xsl:text>]&#10;</xsl:text>
</xsl:template>

</xsl:stylesheet>
