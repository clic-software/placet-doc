<?xml version="1.0"?>
<!--
    xrefs.mod.xsl - templates for cross references.
	$Id: xrefs.mod.xsl,v 1.1 2008/04/07 09:10:36 alatina Exp $	

    Copyright © 2004, 2005 Torsten Bronger <bronger@physik.rwth-aachen.de>.

    This file is part of texi2latex.

    texi2latex is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License as published by the Free
    Software Foundation; either version 2 of the License, or (at your option)
    any later version.

    texi2latex is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with texi2latex; if not, write to the Free Software Foundation, Inc., 59
    Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<!DOCTYPE xsl:stylesheet [
<!ENTITY section.all "self::top or self::chapter or self::unnumbered or self::appendix or
                      self::section or self::unnumberedsec or self::appendixsec or
                      self::subsection or self::unnumberedsubsec or
                      self::appendixsubsec or self::subsubsection or
                      self::unnumberedsubsubsec or self::appendixsubsubsec or
                      majorheading or self::chapheading or self::heading or
                      self::subheading or self::subsubheading">
]>

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- I use \autoref for all cross-links, although it still doesn't support all
     human languages (e.g. French is missing).  It is usable for dvi output,
     too.  By and large, I try to mimic the original Texinfo behaviour here
     (although I don't like it very much). -->

<!-- This is also a point where the Texinfo XML format is flawed.  The "See"
     for introducing a cross-reference is generated automatically so I cannot
     change it, but the "section" or "chapter" is not.  I could take this from
     the strings (see translation-strings.xml), but \autoref is good enough so
     far. -->

<xsl:template match="xref">
  <xsl:variable name="node-name" select="string(xrefnodename)"/>
  <xsl:variable name="label">
    <xsl:call-template name="clean-up-identifier">
      <xsl:with-param name="identifier" select="$node-name"/>
    </xsl:call-template>
  </xsl:variable>
  <xsl:variable name="target-name">
    <xsl:variable name="target-node-name"
      select="local-name(//nodename[string(.) = $node-name]/following-sibling::*[&section.all;])"/>
    <xsl:value-of select="$target-node-name"/>
    <xsl:if test="$target-node-name = ''">
      <xsl:text>float</xsl:text>
    </xsl:if>
  </xsl:variable>
  <xsl:call-template name="create-autoref-name">
    <xsl:with-param name="target-name" select="$target-name"/>
    <xsl:with-param name="node-name" select="$node-name"/>
  </xsl:call-template>
  <xsl:value-of select="concat('\ref{',$label,'}')"/>
  <xsl:if test="$target-name != 'float'">
    <xsl:text> [</xsl:text>
    <xsl:choose>
      <xsl:when test="xrefprinteddesc">
        <xsl:apply-templates select="xrefprinteddesc"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates select="xrefnodename"/>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text>]</xsl:text>
  </xsl:if>
  <xsl:value-of select="concat(' \vpageref{',$label,'}')"/>
</xsl:template>

<xsl:template name="create-autoref-name">
  <xsl:param name="target-name"/>
  <xsl:param name="node-name"/>
  <xsl:variable name="name-label">
    <xsl:choose>
      <xsl:when test="$target-name = 'float'">
        <xsl:variable name="float-type"
          select="normalize-space(//*[normalize-space(@name) = 
                                      $node-name]/floattype)"/>
        <xsl:choose>
          <xsl:when test="$float-type = 'Figure' or $float-type = 'Table'">
            <xsl:call-template name="insert-word">
              <xsl:with-param name="english-word" select="$float-type"/>
              <xsl:with-param name="convert-to-lowercase" select="true()"/>
            </xsl:call-template>
          </xsl:when>
          <xsl:otherwise>
            <xsl:call-template name="convert-to-lowercase-possibly">
              <xsl:with-param name="word" select="$float-type"/>
            </xsl:call-template>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:when test="$target-name = 'chapter' or $target-name = 'unnumbered' or 
                      $target-name = 'majorheading' or 
                      $target-name = 'chapheading'">
        <xsl:call-template name="insert-word">
          <xsl:with-param name="english-word" select="'Chapter'"/>
          <xsl:with-param name="convert-to-lowercase" select="true()"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:when test="$target-name = 'appendix'">
        <xsl:call-template name="insert-word">
          <xsl:with-param name="english-word" select="'Appendix'"/>
          <xsl:with-param name="convert-to-lowercase" select="true()"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="insert-word">
          <xsl:with-param name="english-word" select="'section'"/>
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <xsl:value-of select="$name-label"/>
  <xsl:if test="$name-label != ''">
    <xsl:value-of select="$start-delimiter"/>
    <xsl:text>&#xa0;</xsl:text>
    <xsl:value-of select="$end-delimiter"/>
  </xsl:if>
</xsl:template>

<!-- The special case of external Info links -->

<xsl:template match="xref[xrefinfofile]">
  <xsl:call-template name="insert-word">
    <xsl:with-param name="english-word" select="'section'"/>
  </xsl:call-template>
  <xsl:text> \lqHook </xsl:text>
  <xsl:choose>
    <xsl:when test="xrefprinteddesc">
      <xsl:apply-templates select="xrefprinteddesc"/>
    </xsl:when>
    <xsl:otherwise>
      <xsl:apply-templates select="xrefnodename"/>
    </xsl:otherwise>
  </xsl:choose>
  <xsl:text>\rqHook{} </xsl:text>
  <xsl:call-template name="insert-word">
    <xsl:with-param name="english-word" select="'in'"/>
  </xsl:call-template>
  <xsl:text> </xsl:text>
  <xsl:choose>
    <xsl:when test="xrefprintedname">
      <xsl:text>\textit{</xsl:text>
      <xsl:apply-templates select="xrefprintedname"/>
      <xsl:text>}</xsl:text>
    </xsl:when>
    <xsl:otherwise>
      <xsl:text>\texttt{</xsl:text>
      <xsl:apply-templates select="xrefinfofile"/>
      <xsl:text>}</xsl:text>
      <xsl:message>
        <xsl:text>texi2latex: no real title given for info file '</xsl:text>
        <xsl:value-of select="xrefinfofile"/>
        <xsl:text>' in cross-reference</xsl:text>
      </xsl:message>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!-- The <inforef> element is for refering to Info files where *only* the Info
     file is available or appropriate, rather than the printed version.  This
     may be because no Texinfo source is available, or because you want to
     refer to a thing that only exists in the Info online version (e.g. via
     @ifinfo).  

     Please note that in printed output, the child <inforefrefname> is always
     omitted.  -->

<xsl:template match="inforef">
  <xsl:text>\MakeUppercase </xsl:text>
  <xsl:call-template name="insert-word">
    <xsl:with-param name="english-word" select="'see'"/>
  </xsl:call-template>
  <xsl:text> </xsl:text>
  <xsl:call-template name="insert-word">
    <xsl:with-param name="english-word" select="'Info file'"/>
  </xsl:call-template>
  <xsl:text> </xsl:text>
  <xsl:apply-templates select="inforefinfoname"/>
  <xsl:text>, </xsl:text>
  <xsl:call-template name="insert-word">
    <xsl:with-param name="english-word" select="'node'"/>
  </xsl:call-template>
  <xsl:text> </xsl:text>
  <xsl:apply-templates select="inforefnodename"/>
</xsl:template>

<!-- <inforefinfoname> and <inforefnodename> are printed as "@file" and
     "@samp", respectively.  This fits nicely with the layout in the original
     Texinfo.  -->

<xsl:template match="inforefinfoname">
  <xsl:text>{\fileHook{</xsl:text>
  <xsl:apply-templates/>
  <xsl:text>}}</xsl:text>
</xsl:template>

<xsl:template match="inforefnodename">
  <xsl:text>{\sampHook{</xsl:text>
  <xsl:apply-templates/>
  <xsl:text>}}</xsl:text>
</xsl:template>

</xsl:stylesheet>
