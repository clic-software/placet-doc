<?xml version="1.0"?>
<!--
    urls.mod.xsl - routines for handling URLs, email addresses and hyperlinks.
	$Id: urls.mod.xsl,v 1.1 2008/04/07 09:10:36 alatina Exp $	

    Copyright © 2004, 2005 Torsten Bronger <bronger@physik.rwth-aachen.de>.

    This file is part of texi2latex.

    texi2latex is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License as published by the Free
    Software Foundation; either version 2 of the License, or (at your option)
    any later version.

    texi2latex is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with texi2latex; if not, write to the Free Software Foundation, Inc., 59
    Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<!DOCTYPE xsl:stylesheet [
<!ENTITY bph           "&#x0082;">  <!-- Break permitted here -->
]>

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- The <url> tag is not the result of @url but of @indicateurl.  Therefore it
     is not linked with its target.  (@url has been abandoned, but if it's
     still used, it is an @uref with only the first parameter.) -->

<xsl:template match="url">
  <xsl:text>{\urlHook{</xsl:text>
  <xsl:call-template name="insert-url-breakpoints">  <!-- see below -->
    <xsl:with-param name="url">
      <xsl:apply-templates/>
    </xsl:with-param>
  </xsl:call-template>
  <xsl:text>}}</xsl:text>
</xsl:template>

<!-- For emails, additionally to URLs I have to generate the associated name.
     The rest is the same as above. -->

<xsl:template match="email">
  <xsl:apply-templates select="emailname"/>
  <xsl:if test="emailname">
    <xsl:text> (</xsl:text>
  </xsl:if>
  <xsl:text>\href{</xsl:text>
  <xsl:value-of select="translate(concat('mailto:',emailaddress),'&bph;','')"/>
  <xsl:text>}{\emailHook{</xsl:text>
  <xsl:call-template name="insert-url-breakpoints">
    <xsl:with-param name="url">
      <xsl:apply-templates select="emailaddress"/>
    </xsl:with-param>
  </xsl:call-template>
  <xsl:text>}}</xsl:text>
  <xsl:if test="emailname">
    <xsl:text>)</xsl:text>
  </xsl:if>
</xsl:template>

<!-- Rather straightforward: The \href is correctly interpreted by both
     pdflatex and latex.  The first argument is the link target, the second is
     what's printed on paper and made clickable.

     I have to distinguish between three possible cases (as far as optional
     parameters are concerned). -->

<xsl:template match="uref">
  <!-- Maybe there are &bph;'s from the Texinfo output.  Actually impossible,
       but who knows. -->
  <xsl:variable name="href-call"
    select="concat('\href{',translate(urefurl,'&bph;',''), '}{\urlHook{')"/>
  <xsl:choose>
    <xsl:when test="urefreplacement">
      <xsl:value-of select="$href-call"/>
      <xsl:apply-templates select="urefreplacement"/>
      <xsl:text>}}</xsl:text>
    </xsl:when>
    <xsl:when test="urefdesc">
      <xsl:apply-templates select="urefdesc"/>
      <xsl:text> (</xsl:text>
      <xsl:value-of select="$href-call"/>
      <xsl:call-template name="insert-url-breakpoints">
        <xsl:with-param name="url">
          <xsl:apply-templates select="urefurl"/>
        </xsl:with-param>
      </xsl:call-template>
      <xsl:text>}})</xsl:text>
    </xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="$href-call"/>
      <xsl:call-template name="insert-url-breakpoints">
        <xsl:with-param name="url">
          <xsl:apply-templates select="urefurl"/>
        </xsl:with-param>
      </xsl:call-template>
      <xsl:text>}}</xsl:text>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!-- Here I insert allowed breakpoints in URLs manually.  Actually this should
     be done by hyperref, but hyperref does it improperly so that character
     protruding doesn't work anymore.

     Attention: This routine assumes that $url is delimited with string
     delimiters, e.g. the result of apply-templates on a text() node! -->

<!-- FixMe: Actually this routine becomes a bad hack if there is inline markup
     inside the URL.  the problem is that then the substitution is done even
     oder the inserted LaTeX commands.  Since "/:.@=_ " shouldn't occur inside
     LaTeX commands, this *shouldn't* matter, but it's dirty anyway.

     Remedy: Apply this only for text() childs (not descendants!) of URL
     elements.  But then it must be done for the very last character, too,
     which is not the case for the current substitute-in-string in
     common.mod.xsl. -->

<xsl:template name="insert-url-breakpoints">
  <xsl:param name="url"/>
  <!-- I switch off uncontrolled (automatic) hyphenation in URLs. -->
  <xsl:text>{</xsl:text>
  <xsl:if test="ancestor-or-self::title">
    <!-- In headings that may wander in the TOC -->
    <xsl:text>\protect</xsl:text>
  </xsl:if>
  <xsl:text>\hyphenrules{nohyphenation}</xsl:text>
  <xsl:call-template name="substitute-in-string">
    <xsl:with-param name="str">
      <xsl:call-template name="insert-breakpoints">
        <!-- The "select" attribute contains possible break points in URLs and
             email addresses -->
        <xsl:with-param name="break-after" select="'/:.@=_'"/>
        <xsl:with-param name="string">
          <xsl:call-template name="substitute-in-string">
            <!-- Line breaks at spaces within URLs (bad, but allowed) would be
                 very confusing.  -->
            <xsl:with-param name="str"
              select="translate(normalize-space($url),' ','&#xa0;')"/>
            <xsl:with-param name="look-for" select="'-'"/>
            <!-- I have to replace dashes with \xhyphen in order to have
                 correct character protruding. -->
            <xsl:with-param name="replace-with"
              select="concat($end-delimiter,'\xhyphen ',$start-delimiter)"/>
          </xsl:call-template>
        </xsl:with-param>
      </xsl:call-template>
    </xsl:with-param>
    <!-- the "http://" mustn't be broken between the slashs. -->
    <xsl:with-param name="look-for" select="'/&bph;/'"/>
    <xsl:with-param name="replace-with" select="'//'"/>
  </xsl:call-template>
  <xsl:text>}</xsl:text>
</xsl:template>


</xsl:stylesheet>
