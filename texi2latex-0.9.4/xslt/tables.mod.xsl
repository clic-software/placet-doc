<?xml version="1.0"?>
<!--
    tables.mod.xsl - multicolumn tables.
	$Id: tables.mod.xsl,v 1.1 2008/04/07 09:10:36 alatina Exp $	

    Copyright © 2004, 2005 Torsten Bronger <bronger@physik.rwth-aachen.de>.

    This file is part of texi2latex.

    texi2latex is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License as published by the Free
    Software Foundation; either version 2 of the License, or (at your option)
    any later version.

    texi2latex is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with texi2latex; if not, write to the Free Software Foundation, Inc., 59
    Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- Multi-column (i.e. real) tables.  For two-column tables plese look at
     lists.mod.xsl. -->

<!-- If the table stands alone, I use {longtable} to cover the case when the
     table is too long for one page.  If it is part of a float, I use
     {tabular}, because I assume that the author is clever enough to know that
     floating material should be shorter than a page.

     There is an alternative possibly: The {longtable} envirenment allows for
     a \caption.  So one could process the <float> with a table as a special
     case and construct a solution that uses {longtable} or every table.  But
     I think they do not float, even if they could.  So this could be a bad
     idea. -->

<!-- All this code here relies on the great booktabs package. -->

<xsl:template match="multitable">
  <xsl:variable name="table-environment">
    <xsl:choose>
      <xsl:when test="parent::float">
        <xsl:text>tabular</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>longtable</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <xsl:value-of select="concat('&#10;\begin{',$table-environment,'}{@{}')"/>
  <xsl:apply-templates select="columnfraction"/>
  <xsl:text>@{}}&#10;\toprule&#10;</xsl:text>
  <xsl:apply-templates select="thead | tbody"/>
  <xsl:text>\bottomrule&#10;</xsl:text>
  <xsl:value-of select="concat('\end{',$table-environment,'}&#10;&#10;')"/>
</xsl:template>

<!-- All columns are aligned to the left. -->

<xsl:template match="columnfraction">
  <xsl:text>l</xsl:text>
</xsl:template>

<xsl:template match="thead">
  <xsl:apply-templates/>
  <xsl:text>\midrule&#10;\endhead&#10;</xsl:text>
</xsl:template>

<xsl:template match="row">
  <!-- The \relax prevents first-column entries that start wit "[" to be
       interpreted as a vertical skip dimension. -->
  <xsl:text>\relax </xsl:text>
  <xsl:apply-templates/>
  <xsl:text>\\&#10;</xsl:text>
</xsl:template>

<xsl:template match="entry">
  <xsl:apply-templates/>
  <xsl:if test="following-sibling::entry">
    <xsl:text>  &amp;  </xsl:text>
  </xsl:if>
</xsl:template>

</xsl:stylesheet>
