<?xml version="1.0"?>
<!--
    titlepage.xsl - generating the title page and the second page.
	$Id: titlepage.mod.xsl,v 1.1 2008/04/07 09:10:36 alatina Exp $	

    Copyright © 2004, 2005 Torsten Bronger <bronger@physik.rwth-aachen.de>.

    This file is part of texi2latex.

    texi2latex is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License as published by the Free
    Software Foundation; either version 2 of the License, or (at your option)
    any later version.

    texi2latex is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with texi2latex; if not, write to the Free Software Foundation, Inc., 59
    Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- The title page -->

<xsl:template match="titlepage">
  <xsl:variable name="second-page">
    <xsl:apply-templates select="*[not(self::author) and preceding-sibling::author]"/>
  </xsl:variable>
  <xsl:text>\begingroup\parindent0pt&#10;\begin{titlepage}\vbox to \vsize{\vspace*{3.8cm}</xsl:text>
  <xsl:apply-templates select="*[self::author or not(preceding-sibling::author)]"/>
  <xsl:text>\vspace*{0.4cm}}\end{titlepage}</xsl:text>
  <xsl:if test="normalize-space($second-page) != ''">
    <xsl:text>\vbox to \vsize{\vfill&#10;</xsl:text>
    <xsl:value-of select="$second-page"/>
    <xsl:text>}</xsl:text>
  </xsl:if>
  <xsl:text>\endgroup&#10;</xsl:text>
</xsl:template>


<xsl:template match="settitle">
  <xsl:text>\title{</xsl:text>
  <xsl:apply-templates/>
  <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="booktitle">
  <xsl:text>{\bfseries\huge </xsl:text>
  <xsl:apply-templates/>
  <xsl:text>}\vskip4pt \hrule height 4pt width \hsize \vskip4pt</xsl:text>
</xsl:template>

<xsl:template match="booksubtitle">
  <xsl:text>\hbox to \hsize{\hfill </xsl:text>
  <xsl:apply-templates/>
  <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="author">
  <xsl:if test="not(preceding-sibling::author)">
    <xsl:text>\vfill{\bfseries\Large&#10;</xsl:text>
  </xsl:if>
  <xsl:apply-templates/>
  <xsl:choose>
    <xsl:when test="following-sibling::author">
      <xsl:text>\\\relax </xsl:text>
    </xsl:when>
    <xsl:otherwise>
      <xsl:text>\par}\vskip4pt \hrule height 2pt width \hsize</xsl:text>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

</xsl:stylesheet>
