<?xml version="1.0"?>
<!--
    definitions.xsl - markup of definitions of functions and such.
	$Id: definitions.mod.xsl,v 1.1 2008/04/07 09:10:35 alatina Exp $	

    Copyright © 2004, 2005 Torsten Bronger <bronger@physik.rwth-aachen.de>.

    This file is part of texi2latex.

    texi2latex is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License as published by the Free
    Software Foundation; either version 2 of the License, or (at your option)
    any later version.

    texi2latex is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with texi2latex; if not, write to the Free Software Foundation, Inc., 59
    Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="definitionterm">
  <xsl:text>\par\noindent\begingroup\hyphenrules{nohyphenation}</xsl:text>
  <xsl:for-each select="*[not(self::defcategory)]">
    <xsl:apply-templates select="."/>
    <xsl:if test="position() != last() and not(self::indexterm)">
      <xsl:text>\hskip0.5em</xsl:text>
    </xsl:if>
  </xsl:for-each>
  <xsl:text>\hfill </xsl:text>
  <xsl:apply-templates select="defcategory"/>
  <xsl:text>\endgroup</xsl:text>
  <xsl:if test="following-sibling::definitionterm or following-sibling::definitionitem">
    <xsl:text>\Nopagebreak</xsl:text>
  </xsl:if>
</xsl:template>

<xsl:template match="definitionitem">
  <xsl:text>\begin{definitionitem}</xsl:text>
  <xsl:apply-templates/>
  <xsl:text>\end{definitionitem}&#10;</xsl:text>
</xsl:template>

<xsl:template match="defcategory">
  <xsl:text>[</xsl:text>
  <xsl:apply-templates/>
  <xsl:text>]</xsl:text>
</xsl:template>

<xsl:template match="deffunction">
  <xsl:text>\texttt{</xsl:text>
  <xsl:apply-templates/>
  <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="defvariable">
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="defparam">
  <xsl:text>\textsl{</xsl:text>
  <xsl:apply-templates/>
  <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="defdelimiter">
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="deftype">
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="defparamtype">
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="defdatatype">
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="defclass">
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="defclassvar">
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="defoperation">
  <xsl:apply-templates/>
</xsl:template>


</xsl:stylesheet>
