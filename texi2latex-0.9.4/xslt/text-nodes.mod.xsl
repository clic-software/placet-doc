<?xml version="1.0"?>
<!--
    text-nodes.mod.xsl - XSLT routines for processing text nodes.
	$Id: text-nodes.mod.xsl,v 1.1 2008/04/07 09:10:36 alatina Exp $	

    Copyright © 2004, 2005 Torsten Bronger <bronger@physik.rwth-aachen.de>.

    This file is part of texi2latex.

    texi2latex is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License as published by the Free
    Software Foundation; either version 2 of the License, or (at your option)
    any later version.

    texi2latex is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with texi2latex; if not, write to the Free Software Foundation, Inc., 59
    Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- These delimiters come from the tbook system.  (tbook has even more, but up
     to now they wouldn't make sense in texi2latex.)  They are inserted into
     the output and used (and removed) by tbrplent.  tbrplent can identify text
     nodes and distiguish them from LaTeX code.  Otherwise a word that you want
     to have in bold face ends up as "\textbf{foo}" (sic!) on the paper. -->

<xsl:variable name="start-delimiter" select="'&#x98;'"/>
<xsl:variable name="end-delimiter" select="'&#x9c;'"/>

<xsl:template match="text()" priority="0">
  <xsl:value-of select="$start-delimiter"/>
  <xsl:value-of select="."/>
  <xsl:value-of select="$end-delimiter"/>
</xsl:template>

<!-- In verbatim environments we do the same as above, *plus*, for simpulating
     \obeyspaces, every line break becomes an explicit line break &#xc2028;,
     which is later replaced with "\\".  In order to simulate \obeyspaces, I
     create no break spaces for all ordinary spaces.  Doing that on the LaTeX
     level is a nightmare.  -->

<xsl:template match="text()[ancestor::example or ancestor::smallexample or
                     ancestor::display or ancestor::smalldisplay or
                     ancestor::format or ancestor::smallformat]"
                     name="process-display-text" priority="0.1">
  <xsl:variable name="contents">
    <xsl:value-of select="$start-delimiter"/>
    <xsl:value-of select="."/>
    <xsl:value-of select="$end-delimiter"/>
  </xsl:variable>
  <xsl:value-of select="translate($contents,'&#10; ','&#x2028;&#xa0;')"/>
</xsl:template>

<!-- @verbatim is a special case, because I have to handle TABs here.  Believe
     me, doing that on the LaTeX level is a nightmare, too.  -->

<xsl:template match="text()[ancestor::verbatim]">
  <xsl:choose>
    <xsl:when test="$ignore-tabs != 'no'">
      <xsl:call-template name="process-display-text"/>
    </xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="$start-delimiter"/>
      <xsl:call-template name="process-verbatim-text">
        <xsl:with-param name="rest" select="translate(string(.),' ','&#xa0;')"/>
      </xsl:call-template>
      <xsl:value-of select="$end-delimiter"/>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!-- If you set this XSLT parameter to 'yes' on the command line, all tabs are
     ignored, and the processing of @verbatim is greatly accelerated.  -->

<xsl:param name="ignore-tabs" select="'no'"/>

<!-- Here I skim through the contents of an @verbatim character by character
     and look for line breaks (reset column to "0" and repace them with
     explicit line breaks), tabs (insert spaces to next tabulator), and
     ordinary characters (put them to output).  I (try to) keep track of the
     current column number in the current line.  -->

<xsl:template name="process-verbatim-text">
  <xsl:param name="rest"/>
  <xsl:param name="column" select="0"/>
  <xsl:if test="$rest != ''">
    <xsl:variable name="next-char" select="substring($rest,1,1)"/>
    <xsl:choose>
      <xsl:when test="$next-char = '&#9;'">
        <xsl:value-of select="substring(
                              '&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;',
                              1,8 - ($column mod 8))"/>
        <xsl:call-template name="process-verbatim-text">
          <xsl:with-param name="rest" select="substring($rest,2)"/>
          <xsl:with-param name="column" select="8 * (floor($column div 8) + 1)"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:when test="$next-char = '&#10;'">
        <xsl:text>&#x2028;</xsl:text>
        <xsl:call-template name="process-verbatim-text">
          <xsl:with-param name="rest" select="substring($rest,2)"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$next-char"/>
        <xsl:call-template name="process-verbatim-text">
          <xsl:with-param name="rest" select="substring($rest,2)"/>
          <xsl:with-param name="column" select="$column + 1"/>
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:if>
</xsl:template>

<xsl:template match="linebreak">
  <xsl:text>\lb{}</xsl:text>
</xsl:template>

<!-- This is a weak point of Texinfo's XML format: Punctuation with explicit
     information about whether it is the end of a sentence or not, is only
     inserted where TeX's heuristic would fail.  Since I convert to LaTeX, it
     doesn't matter to me, on the contrary, it makes it very simple for me.
     However, for the *general* XML format this is unfortunate (although not
     unacceptable). -->

<xsl:template match="punct">
  <xsl:if test="@end-of-sentence = 'yes'">
    <xsl:text>\@</xsl:text>
  </xsl:if>
  <xsl:apply-templates/>
  <xsl:if test="@end-of-sentence = 'no'">
    <xsl:text>\@{}</xsl:text>
  </xsl:if>
</xsl:template>

</xsl:stylesheet>
