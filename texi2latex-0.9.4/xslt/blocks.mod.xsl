<?xml version="1.0"?>
<!--
    blocks.mod.xsl - displays, examples. quotations, and such.
	$Id: blocks.mod.xsl,v 1.1 2008/04/07 09:10:35 alatina Exp $	

    Copyright © 2004, 2005 Torsten Bronger <bronger@physik.rwth-aachen.de>.

    This file is part of texi2latex.

    texi2latex is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License as published by the Free
    Software Foundation; either version 2 of the License, or (at your option)
    any later version.

    texi2latex is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with texi2latex; if not, write to the Free Software Foundation, Inc., 59
    Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- This XSLT parameter switches syntax highlighting on or off.  At the
     moment, it only affects Lisp snippets, and only if they doesn't contain
     any further formatting commands.  -->

<xsl:param name="syntax-highlighting" select="'no'"/>

<!-- FixMe: The definition of all the "block" (or "verbatim") environments may
     be not robust enough.  It is not wise to nest them.  This is not
     explicitly allowed in the Texinfo specification, however, I think it
     should be made possible (and it is possible to do so). -->

<!-- FixMe: Maybe it's bad to treat <copying> the same as <quotation>.  At
     least, it must be tested. -->

<!-- If a @list environment contains markup, it is treated like @example.
     Thus, there won't be any syntax highlighting.  This is not very nice, but
     the alternative is to switch off syntax highlighting always. -->

<!-- Flushleft and flushright are not yet supported by makeinfo.  Whyever. -->

<xsl:template match="quotation | copying | verbatim | example | lisp | display
                     | format | smallexample | smalllisp | smalldisplay |
                     smallformat | flushleft | flushright | group | cartouche">
  <xsl:value-of select="concat('\begin{',local-name(),'Hook}')"/>
  <xsl:apply-templates/>
  <xsl:value-of select="concat('\end{',local-name(),'Hook}')"/>
</xsl:template>

<!-- At the moment any explicit formatting in @lisp doesn't work, see above.
     But at least this could be fixed manually for any occuring formatting
     command.  Not elegant, but sensible I think.

     Alternatively, it may be possible to tweak the listings package that it
     interprets all LaTeX commands as is.  -->

<xsl:template match="lisp[not(*)]" name="process-pure-lisp">
  <xsl:text>\begin{lispHook}</xsl:text>
  <xsl:choose>
    <xsl:when test="$syntax-highlighting != 'no'">
      <xsl:text>\begin{lstlisting}[language=lisp]&#10;</xsl:text>
      <!-- Avoid text node delimiters. -->
      <xsl:value-of select="."/>
      <xsl:text>\end{lstlisting}</xsl:text>
    </xsl:when>
    <xsl:otherwise>
      <xsl:apply-templates/>
    </xsl:otherwise>
  </xsl:choose>
  <xsl:text>\end{lispHook}</xsl:text>
</xsl:template>

<xsl:template match="smalllisp[not(*)]">
  <xsl:text>\begin{smalllispHook}</xsl:text>
  <xsl:choose>
    <xsl:when test="$syntax-highlighting != 'no'">
      <!-- FixMe: Set smaller typeface using listings options.  -->
      <xsl:text>\begin{lstlisting}[language=lisp]&#10;</xsl:text>
      <!-- Avoid text node delimiters. -->
      <xsl:value-of select="."/>
      <xsl:text>\end{lstlisting}</xsl:text>
    </xsl:when>
    <xsl:otherwise>
      <xsl:apply-templates/>
    </xsl:otherwise>
  </xsl:choose>
  <xsl:text>\end{smalllispHook}</xsl:text>
</xsl:template>

<xsl:template match="exdent">
  <xsl:text>\hspace*{-\standardmargin}</xsl:text>
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="center">
  <xsl:text>\par\noindent{\centerHook{</xsl:text>
  <xsl:apply-templates/>
  <xsl:text>}}\par</xsl:text>
</xsl:template>

<!-- Since only one line per @center is allowed, I can ignore the para's.  By
     the way, <center> has a very stange context model in Texinfo.  -->

<xsl:template match="center/para">
  <xsl:apply-templates/>
</xsl:template>

</xsl:stylesheet>
