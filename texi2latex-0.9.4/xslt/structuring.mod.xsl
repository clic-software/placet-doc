<?xml version="1.0"?>
<!--
    structuring.mod.xsl - templates for headings and such.
	$Id: structuring.mod.xsl,v 1.1 2008/04/07 09:10:36 alatina Exp $	

    Copyright © 2004, 2005 Torsten Bronger <bronger@physik.rwth-aachen.de>.

    This file is part of texi2latex.

    texi2latex is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License as published by the Free
    Software Foundation; either version 2 of the License, or (at your option)
    any later version.

    texi2latex is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with texi2latex; if not, write to the Free Software Foundation, Inc., 59
    Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<!-- In the entity &is-appendix; I list all elements that are supposed to be
     part of the index, in a way that it can be used in an XPath
     predicate. -->

<!DOCTYPE xsl:stylesheet [
<!ENTITY is-appendix "self::appendix or self::appendixsec or
                      self::appendixsubsec or self::appendixsubsubsec">
<!ENTITY section.all "self::top or self::chapter or self::unnumbered or self::appendix or
                      self::section or self::unnumberedsec or self::appendixsec or
                      self::subsection or self::unnumberedsubsec or
                      self::appendixsubsec or self::subsubsection or
                      self::unnumberedsubsubsec or self::appendixsubsubsec or
                      majorheading or self::chapheading or self::heading or
                      self::subheading or self::subsubheading">
]>

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- The node infrastructure is irrelevant in the printed output. -->

<xsl:template match="menu | nodename | nodeprev | nodenext | nodeup">
  <!-- gobble -->
</xsl:template>

<!-- Every node must create a \label.  Ideally it should be immediately after
     an unstared sectioning command.  But due to the looseness of the Texinfo
     file format, we must take what we can get.

     Be that as it may, here I only save the label in a provisional command
     that is called later (hopefully). -->

<xsl:template match="node">
  <xsl:text>&#10;&#10;\gdef\InsertLabelMaybe{\label{</xsl:text>
  <xsl:call-template name="clean-up-identifier">
    <xsl:with-param name="identifier" select="nodename"/>
  </xsl:call-template>
  <xsl:text>}\gdef\InsertLabelMaybe{}}&#10;</xsl:text>

  <!-- If there is no sectioning command, I insert the label where the node
       starts.  Alternatively, one could insert it after the first
       content-producing child element. -->

  <xsl:if test="not(descendant::*[&section.all;])">
    <xsl:text>\InsertLabelMaybe&#10;</xsl:text>
  </xsl:if>
  <xsl:apply-templates/>
</xsl:template>

<!-- This is a very important template.  When we stumble over a <nodename>,
     this is an anchor, i.e. a \label in LaTeX.  Therefore I must generate a
     \label here, so that the chapter/appendix/section becomes referable.
     Note that every non-stared section has a number due to a secnumdepth of
     "5".  Also note that you can't include a prefix here like "sec:" because
     when I refer to it, I can't find out easily wheter the target is a figure
     or a section. -->

<xsl:template match="nodename" mode="label">
  <xsl:text>\label{</xsl:text>
  <xsl:call-template name="clean-up-identifier">
    <xsl:with-param name="identifier" select="."/>
  </xsl:call-template>
  <xsl:text>}&#10;</xsl:text>
</xsl:template>

<xsl:template match="node[normalize-space(nodename) = 'Top']">
  <!-- Look for only one in the Top node: the table of contents.  Gobble the
       rest. -->
  <xsl:apply-templates select="descendant::contents"/>
</xsl:template>

<!-- The central structuring template.  Actually it is one big choose that
     generates the corresponding LaTeX structuring command.  I use the <title>
     element as the pivot because then I have direct access to the heading.
     But by and large this is insignificant. -->

<xsl:template match="title">
  <xsl:variable name="is-appendix" select="parent::*[&is-appendix;]"/>
  <!-- Is this the very first appendix element in the document? -->
  <xsl:if test="$is-appendix and not(preceding::*[&is-appendix;]) and
                not(parent::*[ancestor::*[&is-appendix;]])">
    <!-- If yes, then start the appendix. -->
    <xsl:text>\appendix&#10;</xsl:text>
  </xsl:if>
  <xsl:choose>
    <xsl:when test="parent::chapter or parent::appendix">
      <xsl:text>\chapter</xsl:text>
    </xsl:when>
    <xsl:when test="parent::unnumbered">
      <xsl:text>\chapter*</xsl:text>
    </xsl:when>
    <xsl:when test="parent::section or parent::appendixsec">
      <xsl:text>\section</xsl:text>
    </xsl:when>
    <xsl:when test="parent::unnumberedsec">
      <xsl:text>\section*</xsl:text>
    </xsl:when>
    <xsl:when test="parent::subsection or parent::appendixsubsec">
      <xsl:text>\subsection</xsl:text>
    </xsl:when>
    <xsl:when test="parent::unnumberedsubsec">
      <xsl:text>\subsection*</xsl:text>
    </xsl:when>
    <xsl:when test="parent::subsubsection or parent::appendixsubsubsec">
      <xsl:text>\subsubsection</xsl:text>
    </xsl:when>
    <xsl:when test="parent::unnumberedsubsubsec">
      <xsl:text>\subsubsection*</xsl:text>
    </xsl:when>
    <xsl:otherwise>
      <xsl:message>
        <xsl:text disable-output-escaping="yes">&lt;title&gt; element has unrecognised parent</xsl:text>
      </xsl:message>
      <xsl:text>{</xsl:text>
    </xsl:otherwise>
  </xsl:choose>
  <!-- I don't create hypbmsec arguments for starred sectioning commands, because
       hypbmsec doesn't support it (sensibly).  -->
  <xsl:if test="not(starts-with(local-name(parent::*),'unnumbered'))">
    <xsl:call-template name="insert-hypbmsec-argument"/>
  </xsl:if>
  <xsl:call-template name="insert-heading"/>
</xsl:template>

<!-- Texinfo's ...heading commands have a different structure.  There is no
     <title>, so I must treat them differently.  It is more simple
     actually. -->

<!-- FixMe: <majorheading> and <chapheading> start a new page at the moment,
     which is wrong. -->

<xsl:template match="majorheading | chapheading">
  <xsl:text>&#10;&#10;\chapter*</xsl:text>
  <xsl:call-template name="insert-heading"/>
</xsl:template>

<xsl:template match="heading">
  <xsl:text>&#10;&#10;\section*</xsl:text>
  <xsl:call-template name="insert-heading"/>
</xsl:template>

<xsl:template match="subheading">
  <xsl:text>&#10;&#10;\subsection*</xsl:text>
  <xsl:call-template name="insert-heading"/>
</xsl:template>

<xsl:template match="subsubheading">
  <xsl:text>&#10;&#10;\subsubsection*</xsl:text>
  <xsl:call-template name="insert-heading"/>
</xsl:template>

<!-- Here I create the last part of the heading command, i.e. after the
     e.g. "\section".  If the heading contains elements that expanded to
     somthing else than just its contents (very most do so), or dangerous
     characters were used, than a "plain" heading is added in round parentheses
     using the hypbmsec package.

     FixMe: The pathological characters of LaTeX will produce hyperref
     warnings, becuase tbrplent subsitutes rather complex constructs for them
     This could be fixed with a very complex substring subsitution here (and
     omitting of the delimiters for the (...) argument).  But the much better
     way is to make tbrplent better by adding more modi to it (instead of three
     at the moment). -->

<xsl:template name="insert-hypbmsec-argument">
  <xsl:variable name="heading-unformatted" 
    select="concat($start-delimiter,.,$end-delimiter)"/>
  <xsl:variable name="heading-formatted">
    <xsl:apply-templates/>
  </xsl:variable>
  <xsl:if test="$heading-unformatted != string($heading-formatted)">
    <xsl:value-of select="concat('({',$heading-unformatted,'})')"/>
  </xsl:if>
</xsl:template>

<xsl:template name="insert-heading">
  <xsl:text>{</xsl:text>
  <xsl:apply-templates/>
  <xsl:text>}</xsl:text>
  <xsl:text>\InsertLabelMaybe&#10;</xsl:text>
</xsl:template>

<!-- The table of contents.  Very simple. -->

<xsl:template match="contents">
  <xsl:text>\tableofcontents </xsl:text>
</xsl:template>

</xsl:stylesheet>
