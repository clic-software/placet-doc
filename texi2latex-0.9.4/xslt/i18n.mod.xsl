<?xml version="1.0"?>
<!--
    i18n.mod.xsl - internationalisation, i.e. translations and more.
	$Id: i18n.mod.xsl,v 1.1 2008/04/07 09:10:35 alatina Exp $	

    Copyright © 2004, 2005 Torsten Bronger <bronger@physik.rwth-aachen.de>.

    This file is part of texi2latex.

    texi2latex is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License as published by the Free
    Software Foundation; either version 2 of the License, or (at your option)
    any later version.

    texi2latex is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with texi2latex; if not, write to the Free Software Foundation, Inc., 59
    Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<!DOCTYPE xsl:stylesheet [
<!ENTITY thinsp                       "&#x02009;" ><!--=spatium  -->
<!ENTITY translation-strings   SYSTEM "translation-strings.xml">
]>

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:loc="local">

<!-- $document-language-code is the value of the xml:lang attributes in the
     original XML file.  Note that Texinfo documents can have only *one* human
     language which is global for the whole document. -->

<xsl:variable name="document-language-code">
  <xsl:call-template name="detect-document-language-code"/>
</xsl:variable>

<!-- I define three global variables (well, one is commented out so far) that
     are used in the document preamble in texi2latex.xsl: $document-language
     contains the babel code of the document language, as accurate as
     possible.  However, varioref cannot deal with all fine-grained languages.
     So $document-language-roughly contains the same, but in special cases a
     more simple variant (e.g. "english" instead of "USenglish").  Finally,
     $document-language-very-rougly contains only the basic document language.
     Some packages (e.g. minitoc) can only cope with that. -->

<xsl:variable name="document-language">
  <xsl:call-template name="language"/>
</xsl:variable>

<!-- $document-language-roughly is used e.g. for varioref, that doesn't support
     all babel codes. -->

<xsl:variable name="document-language-roughly">
  <xsl:call-template name="language">
    <xsl:with-param name="accuracy-level" select="5"/>
  </xsl:call-template>
</xsl:variable>

<!-- $document-language-very-roughly would only be used by very few packages
     with only few language option, e.g. minitoc -->

<!--xsl:variable name="document-language-very-roughly">
  <xsl:call-template name="language">
    <xsl:with-param name="accuracy-level" select="0"/>
  </xsl:call-template>
</xsl:variable-->

<!-- Here I detect the language code of the source XML document.  Apparently
     xml:lang can occur at two places: The root element and the
     <documentlanguage> element.  The latter has precedence. -->

<xsl:template name="detect-document-language-code">
  <xsl:variable name="explicit-element"
    select="normalize-space(//documentlanguage[1]/@xml:lang)"/>
  <xsl:variable name="root-attribute"
    select="normalize-space(/texinfo/@xml:lang)"/>
  <xsl:variable name="language-code-raw">
    <xsl:choose>
      <xsl:when test="$explicit-element != ''">
        <xsl:value-of select="$explicit-element"/>
      </xsl:when>
      <xsl:when test="$root-attribute != ''">
        <xsl:value-of select="$root-attribute"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>en</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <xsl:value-of select="translate($language-code-raw,$uppercase,$lowercase)"/>
</xsl:template>

<!-- Creation of a LaTeX language name -->

<!-- This routine produces a language name from a language attribute that is
     passed to it.  So the produced string can serve as the argument in e.g. a
     \selectlanguage or babel.  accuracy-level can be 0, 5 or 10 and means
     whether all sub-languages should be procuded, else it produces
     e.g. "german" instead of "austrian".  Reason: Some packages
     (e.g. minitoc) can't cope with all languages.  The big <choose> tries to
     sort the precise cases out first.

     By and large copied from tbook.  I only deleted the heuristic language
     guessing and the default value of $lang. -->

<xsl:template name="language">
  <xsl:param name="lang" select="$document-language-code"/>
  <xsl:param name="accuracy-level" select="10"/>
  <xsl:variable name="lang-lc"
                select="translate($lang,$uppercase,$lowercase)"/>
  <xsl:variable name="possibly-country" select="substring($lang-lc,4,2)"/>
  <xsl:variable name="language-code" select="substring($lang-lc,1,2)"/>
  <xsl:choose>
    <xsl:when test="$lang-lc='en-gb' and $accuracy-level &gt; 5">
      <xsl:text>UKenglish</xsl:text>
    </xsl:when>
    <xsl:when test="$lang-lc='en-us' and $accuracy-level &gt; 5">
      <xsl:text>USenglish</xsl:text>
    </xsl:when>
    <!-- fallback -->
    <xsl:when test="starts-with($lang-lc,'en')">
      <xsl:text>english</xsl:text>
    </xsl:when>
    <!-- Now for German.  *sigh* -->
    <xsl:when test="$lang-lc='de-at-1996' and $accuracy-level &gt; 0">
      <xsl:text>naustrian</xsl:text>
    </xsl:when>
    <xsl:when test="$lang-lc='de-de-1996' and $accuracy-level &gt; 0">
      <xsl:text>ngerman</xsl:text>
    </xsl:when>
    <xsl:when test="$lang-lc='de-at-1901' and $accuracy-level &gt; 0">
      <xsl:text>austrian</xsl:text>
    </xsl:when>
    <xsl:when test="$lang-lc='de-de-1901' and $accuracy-level &gt; 0">
      <xsl:text>german</xsl:text>
    </xsl:when>
    <!-- fallbacks for German -->
    <xsl:when test="$lang-lc='de-1996' and $accuracy-level &gt; 0">
      <xsl:text>ngerman</xsl:text>
    </xsl:when>
    <xsl:when test="$lang-lc='de-1901' and $accuracy-level &gt; 0">
      <xsl:text>german</xsl:text>
    </xsl:when>
    <xsl:when test="$lang-lc='de-at' and $accuracy-level &gt; 0">
      <xsl:text>naustrian</xsl:text>
    </xsl:when>
    <xsl:when test="$lang-lc='de-de' and $accuracy-level &gt; 0">
      <xsl:text>ngerman</xsl:text>
    </xsl:when>
    <xsl:when test="starts-with($lang-lc,'de')">
      <xsl:choose>
        <xsl:when test="$accuracy-level = 0">
          <xsl:text>german</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>ngerman</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:when>
    <xsl:when test="starts-with($lang-lc,'fr')">
      <xsl:text>french</xsl:text>
    </xsl:when>
    <xsl:when test="starts-with($lang-lc,'it')">
      <xsl:text>italian</xsl:text>
    </xsl:when>
    <xsl:when test="starts-with($lang-lc,'es')">
      <xsl:text>spanish</xsl:text>
    </xsl:when>
    <xsl:when test="starts-with($lang-lc,'ca')">
      <xsl:text>catalan</xsl:text>
    </xsl:when>
    <xsl:when test="starts-with($lang-lc,'pl')">
      <xsl:text>polish</xsl:text>
    </xsl:when>
    <xsl:when test="starts-with($lang-lc,'nl')">
      <xsl:text>dutch</xsl:text>
    </xsl:when>
    <xsl:when test="starts-with($lang-lc,'sv')">
      <xsl:text>swedish</xsl:text>
    </xsl:when>
    <xsl:when test="starts-with($lang-lc,'da')">
      <xsl:text>danish</xsl:text>
    </xsl:when>
    <xsl:when test="starts-with($lang-lc,'no-nyn')">
      <xsl:text>nynorsk</xsl:text>
    </xsl:when>
    <xsl:when test="starts-with($lang-lc,'no-bok')">
      <xsl:text>norsk</xsl:text>
    </xsl:when>
    <!-- fallback -->
    <xsl:when test="starts-with($lang-lc,'no')">
      <xsl:text>norsk</xsl:text>
    </xsl:when>
    <xsl:when test="starts-with($lang-lc,'fi')">
      <xsl:text>finnish</xsl:text>
    </xsl:when>
    <xsl:when test="starts-with($lang-lc,'pt-br')">
      <xsl:text>brasilian</xsl:text>
    </xsl:when>
    <!-- fallback -->
    <xsl:when test="starts-with($lang-lc,'pt')">
      <xsl:text>portuguese</xsl:text>
    </xsl:when>
    <xsl:when test="starts-with($lang-lc,'ru')">
      <xsl:text>russian</xsl:text>
    </xsl:when>
    <xsl:otherwise>
      <xsl:text>english</xsl:text>
      <xsl:message>
        <xsl:text>texi2latex: Language not recognized: </xsl:text>
        <xsl:value-of select="$lang"/>
        <xsl:text> (English set)</xsl:text>
      </xsl:message>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!-- Map English words to international ones -->

<!-- At the moment, all this is simply copied from my tbook project and not
     used.  However, it could easily turn out that texi2latex has to create
     snippets in foreign languages.  Moreover, the DTD could change and make
     this necessary. -->

<!-- The following routine insert-word takes $english-word as a handle to
     insert a proper translation according to the current language.

     $kind = 'code' means that it's not really a text patch, but a language
     dependent code piece like a bibliography style. -->

<xsl:template name="insert-word">
  <xsl:param name="english-word"/>
  <xsl:param name="kind" select="''"/>
  <xsl:param name="lang" select="$document-language-code"/>
  <xsl:param name="convert-to-lowercase" select="false()"/>
  <xsl:variable name="n-lang" select="translate(substring($lang,1,2),$uppercase,$lowercase)"/>
  <xsl:variable name="result">
    <xsl:choose>
      <xsl:when test="contains('|en|de|fr|it|es|ca|pl|nl|sv|da|no-nyn|no|fi|pt-br|pt|ru|',
                               concat('|',$n-lang,'|'))">
        <xsl:apply-templates select="document('')/xsl:stylesheet/
                              loc:strings/loc:stringgroup[lang($n-lang)][1]/
                              loc:string[@key=$english-word and string(@kind)=$kind]"/>
      </xsl:when>
      <xsl:otherwise>  <!-- 'unknown' -->
        <xsl:apply-templates select="document('')/xsl:stylesheet/
                              loc:strings/loc:stringgroup[lang('en')][1]/
                              loc:string[@key=$english-word and string(@kind)=$kind]"/>
        <xsl:if test="$kind!='code'">
          <xsl:text> [not translated because language '</xsl:text>
          <xsl:value-of select="ancestor-or-self::*[@xml:lang][1]/@xml:lang"/>
          <xsl:text>' not available]</xsl:text>
        </xsl:if>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <xsl:choose>
    <xsl:when test="$convert-to-lowercase">
      <xsl:call-template name="convert-to-lowercase-possibly">
        <xsl:with-param name="word" select="$result"/>
        <xsl:with-param name="lang" select="$lang"/>
      </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="$result"/>
    </xsl:otherwise>
  </xsl:choose>
  <xsl:if test="string($result)=''">
    <xsl:message>
      <xsl:text>texi2latex: the </xsl:text>
      <xsl:choose>
        <xsl:when test="$kind = 'code'">
          <xsl:text>code</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>text</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:text> '</xsl:text>
      <xsl:value-of select="$english-word"/>
      <xsl:text>' not yet translated into the language '</xsl:text>
      <xsl:value-of select="$n-lang"/>
      <xsl:text>'</xsl:text>
    </xsl:message>
    <xsl:value-of select="$english-word"/> <!-- fallback -->
    <xsl:if test="$kind!='code'">
      <xsl:text> [not yet translated]</xsl:text>
    </xsl:if>
  </xsl:if>
</xsl:template>

<xsl:template name="convert-to-lowercase-possibly">
  <xsl:param name="word"/>
  <xsl:param name="lang" select="$document-language-code"/>
  <xsl:variable name="n-lang" select="translate(substring($lang,1,2),$uppercase,$lowercase)"/>
  <xsl:choose>
    <xsl:when test="document('')/xsl:stylesheet/
                    loc:strings/loc:stringgroup[lang($n-lang)][1]/
                    @nouns-lowercase = 'no'">
      <xsl:value-of select="$word"/>
    </xsl:when>
    <xsl:otherwise>
      <xsl:variable name="actual-word" select="substring-after($word,$start-delimiter)"/>
      <xsl:value-of select="concat(substring-before($word,$start-delimiter),$start-delimiter)"/>
      <xsl:value-of select="concat(translate(substring($actual-word,1,1),$uppercase,
                            $lowercase),substring($actual-word,2))"/>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!-- The following is an inclusion of the file translation-strings.xml -->

&translation-strings;


<!-- Quotation marks are heavily language dependent.  Thus, the following
     routines are big <choose>s.  They create LaTeX commands that hold the
     quotation marks of the Texinfo document language.

     There are four LaTeX commands, two for left (opening) and two for right
     (closing) marks.  \lqHook and \rqHook are the outer (first level) marks,
     and \lnqHook and \rnqHook are the inner, "nested" (second level) marks.
     In American English it means “this is an outer, ‘and this an inner’
     quotation”.  -->

<!-- The following routine inserts the LaTeX commands for the four quotation
     marks in the LaTeX preamble.  -->

<xsl:template name="insert-quotation-marks-commands">
  <xsl:text>\def\lqHook{</xsl:text>
  <xsl:value-of select="$start-delimiter"/>
  <xsl:call-template name="insert-first-level-left-quotes"/>
  <xsl:value-of select="$end-delimiter"/>
  <xsl:text>}</xsl:text>
  <xsl:text>\def\rqHook{</xsl:text>
  <xsl:value-of select="$start-delimiter"/>
  <xsl:call-template name="insert-first-level-right-quotes"/>
  <xsl:value-of select="$end-delimiter"/>
  <xsl:text>}</xsl:text>
  <xsl:text>\def\lnqHook{</xsl:text>
  <xsl:value-of select="$start-delimiter"/>
  <xsl:call-template name="insert-second-level-left-quotes"/>
  <xsl:value-of select="$end-delimiter"/>
  <xsl:text>}</xsl:text>
  <xsl:text>\def\rnqHook{</xsl:text>
  <xsl:value-of select="$start-delimiter"/>
  <xsl:call-template name="insert-second-level-right-quotes"/>
  <xsl:value-of select="$end-delimiter"/>
  <xsl:text>}%&#10;</xsl:text>
</xsl:template>

<!-- The following four routines generate the respective quotation mark. -->

<xsl:template name="insert-first-level-left-quotes">
  <xsl:choose>  <!-- Open quotes -->
    <xsl:when test="lang('en-GB')">
      <xsl:text>‘</xsl:text>
    </xsl:when>
    <xsl:when test="lang('en')">
      <xsl:text>“</xsl:text>
    </xsl:when>
    <xsl:when test="lang('de') or lang('da') or lang('pl')">
      <xsl:text>„</xsl:text>  <!-- » also possible -->
    </xsl:when>
    <xsl:when test="lang('fr')">
      <xsl:choose>
        <xsl:when test="ancestor::title">
          <xsl:text>“</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>«&thinsp;</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:when>
    <xsl:when test="lang('it')">
      <xsl:choose>
        <xsl:when test="ancestor::title">
          <xsl:text>“</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>«</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:when>
    <xsl:when test="lang('es') or lang('ca')">
      <xsl:text>“</xsl:text>   <!-- « also possible -->
    </xsl:when>
    <xsl:when test="lang('nl')">
      <xsl:text>„</xsl:text>
    </xsl:when>
    <xsl:when test="lang('fi') or lang('sv')">
      <xsl:text>”</xsl:text>  <!-- » also possible -->
    </xsl:when>
    <xsl:when test="lang('pt')">
      <xsl:choose>
        <xsl:when test="ancestor::title">
          <xsl:text>“</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>«</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:when>
    <xsl:when test="lang('no')">
      <xsl:choose>
        <xsl:when test="ancestor::title">
          <xsl:text>“</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>«</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:when>
    <xsl:when test="lang('ru')">
      <xsl:text>„</xsl:text>  <!-- « also possible -->
    </xsl:when>
    <xsl:otherwise>  <!-- Fallback to English -->
      <xsl:text>“</xsl:text>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template name="insert-first-level-right-quotes">
  <xsl:choose>  <!-- Closing quotes -->
    <xsl:when test="lang('en-GB')">
      <xsl:text>’</xsl:text>
    </xsl:when>
    <xsl:when test="lang('en')">
      <xsl:text>”</xsl:text>
    </xsl:when>
    <xsl:when test="lang('de') or lang('da') or lang('pl')">
      <xsl:text>“</xsl:text>  <!-- « also possible -->
    </xsl:when>
    <xsl:when test="lang('fr')">
      <xsl:choose>
        <xsl:when test="ancestor::title">
          <xsl:text>”</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>&thinsp;»</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:when>
    <xsl:when test="lang('it')">
      <xsl:choose>
        <xsl:when test="ancestor::title">
          <xsl:text>„</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>»</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:when>
    <xsl:when test="lang('es') or lang('ca')">
      <xsl:text>”</xsl:text>   <!-- » also possible -->
    </xsl:when>
    <xsl:when test="lang('nl')">
      <xsl:text>”</xsl:text>
    </xsl:when>
    <xsl:when test="lang('fi') or lang('sv')">
      <xsl:text>”</xsl:text>  <!-- » also possible -->
    </xsl:when>
    <xsl:when test="lang('pt')">
      <xsl:choose>
        <xsl:when test="ancestor::title">
          <xsl:text>”</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>»</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:when>
    <xsl:when test="lang('no')">
      <xsl:choose>
        <xsl:when test="ancestor::title">
          <xsl:text>”</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>»</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:when>
    <xsl:when test="lang('ru')">
      <xsl:text>“</xsl:text>  <!-- » also possible -->
    </xsl:when>
    <xsl:otherwise>  <!-- Fallback to English -->
      <xsl:text>”</xsl:text>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template name="insert-second-level-left-quotes">
  <xsl:choose>  <!-- Open quotes -->
    <xsl:when test="lang('en-GB')">
      <xsl:text>“</xsl:text>
    </xsl:when>
    <xsl:when test="lang('en')">
      <xsl:text>‘</xsl:text>
    </xsl:when>
    <xsl:when test="lang('de') or lang('da') or lang('pl')">
      <xsl:text>,</xsl:text>  <!-- › also possible -->
    </xsl:when>
    <xsl:when test="lang('fr')">
      <xsl:choose>
        <xsl:when test="ancestor::title">
          <xsl:text>‘</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>‹&thinsp;</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:when>
    <xsl:when test="lang('it')">
      <xsl:choose>
        <xsl:when test="ancestor::title">
          <xsl:text>“</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>‹</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:when>
    <xsl:when test="lang('es') or lang('ca')">
      <xsl:text>‘</xsl:text>   <!-- ‹ also possible -->
    </xsl:when>
    <xsl:when test="lang('nl')">
      <xsl:text>,</xsl:text>
    </xsl:when>
    <xsl:when test="lang('fi') or lang('sv')">
      <xsl:text>’</xsl:text>  <!-- › also possible -->
    </xsl:when>
    <xsl:when test="lang('pt')">
      <xsl:choose>
        <xsl:when test="ancestor::title">
          <xsl:text>‘</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>‹</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:when>
    <xsl:when test="lang('no')">
      <xsl:choose>
        <xsl:when test="ancestor::title">
          <xsl:text>‹</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>‘</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:when>
    <xsl:when test="lang('ru')">
      <xsl:text>,</xsl:text>  <!-- ‹ also possible -->
    </xsl:when>
    <xsl:otherwise>  <!-- Fallback to English -->
      <xsl:text>‘</xsl:text>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template name="insert-second-level-right-quotes">
  <xsl:choose>  <!-- Closing quotes -->
    <xsl:when test="lang('en-GB')">
      <xsl:text>”</xsl:text>
    </xsl:when>
    <xsl:when test="lang('en')">
      <xsl:text>’</xsl:text>
    </xsl:when>
    <xsl:when test="lang('de') or lang('da') or lang('pl')">
      <xsl:text>‘</xsl:text>  <!-- ‹ also possible -->
    </xsl:when>
    <xsl:when test="lang('fr')">
      <xsl:choose>
        <xsl:when test="ancestor::title">
          <xsl:text>’</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>&thinsp;›</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:when>
    <xsl:when test="lang('it')">
      <xsl:choose>
        <xsl:when test="ancestor::title">
          <xsl:text>,</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>›</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:when>
    <xsl:when test="lang('es') or lang('ca')">
      <xsl:text>’</xsl:text>   <!-- › also possible -->
    </xsl:when>
    <xsl:when test="lang('nl')">
      <xsl:text>’</xsl:text>
    </xsl:when>
    <xsl:when test="lang('fi') or lang('sv')">
      <xsl:text>’</xsl:text>  <!-- › also possible -->
    </xsl:when>
    <xsl:when test="lang('pt')">
      <xsl:choose>
        <xsl:when test="ancestor::title">
          <xsl:text>’</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>›</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:when>
    <xsl:when test="lang('no')">
      <xsl:choose>
        <xsl:when test="ancestor::title">
          <xsl:text>›</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>’</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:when>
    <xsl:when test="lang('ru')">
      <xsl:text>‘</xsl:text>  <!-- › also possible -->
    </xsl:when>
    <xsl:otherwise>  <!-- Fallback to English -->
      <xsl:text>’</xsl:text>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

</xsl:stylesheet>
