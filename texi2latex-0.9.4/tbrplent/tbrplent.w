@q===============================================================================@>
@q  tbrplent -- replace UTF-8 enconded characters to LaTeX commands.             @>
@q	$Id: tbrplent.w,v 1.1 2008/04/07 09:10:35 alatina Exp $	                 @>
@q                                                                               @>
@q  Copyright (C) 2004, 2005 Torsten Bronger <bronger@@physik.rwth-aachen.de>.   @>
@q                                                                               @>
@q  This file is part of texi2latex.                                             @>
@q                                                                               @>
@q  texi2latex is free software; you can redistribute it and/or modify it under  @>
@q  the terms of the GNU General Public License as published by the Free         @>
@q  Software Foundation; either version 2 of the License, or (at your option)    @>
@q  any later version.                                                           @>
@q                                                                               @>
@q  texi2latex is distributed in the hope that it will be useful, but WITHOUT    @>
@q  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        @>
@q  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for    @>
@q  more details.                                                                @>
@q                                                                               @>
@q  You should have received a copy of the GNU General Public License along      @>
@q  with texi2latex; if not, write to the Free Software Foundation, Inc., 59     @>
@q  Temple Place, Suite 330, Boston, MA 02111-1307 USA                           @>
@q                                                                               @>
@q                                                                               @>
@q                                                                               @>
@q  To get a compilable C++ source code, use                                     @>
@q       ctangle tbrplent.w - tbrplent.cc                                        @>
@q  To get a structured documentation of the program, use                        @>
@q       cweave tbrplent.w                                                       @>
@q       tex tbrplent                                                            @>
@q                                                                               @>
@q  cweave and ctangle belong to the cweb system by Levy/Knuth.                  @>
@q  Further information can be obtained at                                       @>
@q       http://www-cs-faculty.stanford.edu/~knuth/cweb.html                     @>
@q===============================================================================@>

\secpagedepth=1
\def\TeX{T\kern-.1667em\lower.5ex\hbox{E}\kern-.125emX}
\def\LaTeX{L\kern-.36em%
        {\setbox0=\hbox{T}%
         \vbox to\ht0{\hbox{\sevenrm A}\vss}%
        }%
        \kern-.15em%
        \TeX}
\def\LaTeXbf{L\kern-.36em%
        {\setbox0=\hbox{T}%
         \vbox to\ht0{\hbox{\sevenbf A}\vss}%
        }%
        \kern-.15em%
        \TeX}
\def\AMSinner{\cal A\mkern-2mu\raise-0.5ex\hbox{$\cal M$}\mkern-2muS}
\def\AMS{\ifmmode\AMSinner\else$\AMSinner$\fi}
\font\sf=cmss10 \font\sfbf=cmssbx10
\font\sfa=cmss7
\def\tbook{{\sfbf t\sf book}}

\font\stitlefont=cmss8 scaled 1815         % sans serif type in title
\font\sbtitlefont=cmssbx10 scaled\magstep2 % sans bold type in title
\font\ttitlefont=cmtt8 scaled 1815         % typewriter type in title


\def\title{TBRPLENT (Version 1.5.1)}
\def\topofcontents{\null\vfill
  \centerline{\titlefont The {\sbtitlefont t\stitlefont book} Unicode
  Filter {\ttitlefont TBRPLENT}}
  \vskip 15pt
  \centerline{(Version 1.5.1)}
  \vfill}
\def\botofcontents{\vfill
\noindent
Copyright \copyright\ 2002--2005 Torsten Bronger
                           ({\tt bronger@@users.sourceforge.net})
\bigskip\noindent
Permission is granted to make and distribute verbatim copies of this
document provided that the copyright notice and this permission notice
are preserved on all copies.

\smallskip\noindent
Permission is granted to copy and distribute modified versions of this
document under the conditions for verbatim copying, provided that the
entire resulting derived work is given a different name and distributed
under the terms of a permission notice identical to this one.
}

@i c++lib.w
@s ios int

@** Introduction.  This program \.{tbrplent} (``\tbook\ replace entities'') is to be
used as part of the \tbook\ system.  It takes standard input and writes to standard
output so that it can be used as a filter in shell commands like

\smallskip\noindent{\tt saxon test.xml tblatex.xsl \char"7C\ tbrplent > test.tex}

\smallskip\noindent The Saxon {\mc XSLT} stylesheets produce \LaTeX\ output with two
kinds of characters that \LaTeX\ can't cope with: Unicode characters, and special
delimiters in order to distinguish between ``user'' text and other (non-user) text.  In
user text, a \.{\$} must be interpreted as a dollar sign, but not as a symbol for
beginning equations.  However, everywhere else it {\it is\/} enclosing formulas.
``User text'' is called ``text node'' in an {\mc XML} file.

The purpose of this program is to substitute for all Unicode characters their \LaTeX\
Latin-1 counterpart, or, if there is none, an appropriate macro (sequence).  The second
task is to replace all special \LaTeX\ characters in {\it user\/} text with an escaped
sequence, e.\,g.\ \.{"\$"}~$\to$ \.{"\\\$"}.

@* The main Routine.  It gives an overview of this titchy program.  It may be expanded
in a way that is also accepts command line options and that it can input from and
output into a file directly.  But for me I saw no need for that.

First, we need file streams.

@<Header...@>=
#include <fstream>
 
@ Now the |main()| routine itself.  Another somewhat weak thing is the fixed name (that
is defined here) of the file that contains the entities.  But even that should be
bearable.

@d entities_file_name "tbents.txt"

@<The main() function@>=
int main() {
    prefixed_ifstream entities_file(entities_file_name);
    entity_list tbook_entities;

    read_entities(entities_file, tbook_entities);
    @<Add Dingbats to entity table@>@;
    process_tex_file(cin, cout, tbook_entities);
    return 0;
}

@** Global Definitions. Here is everything that should come first.  Since it's very
unconvenient to have these ubiquitous ``|std::@[@t@>@]|'' flying around here, I'm lazy
and make it implicit.

@c 
@h 
@<Header files inclusions@>@;

using namespace std;

@ The global constant |file_name_prefix| contains the directory that contains the file
\.{tbents.txt}.  It is expected that |#TBLIBDIR| ends in a \.{/}, so it must be \.{./} in
order to denote the current directory.

@c
const char* tblibdir_env_raw = getenv("TBLIBDIR");
const string tblibdir_env = (tblibdir_env_raw == NULL ? "" : tblibdir_env_raw);
#ifdef TBLIBDIR
const string file_name_prefix(tblibdir_env.size() > 0 ? tblibdir_env : TBLIBDIR);
#else
const string file_name_prefix(tblibdir_env.size() > 0 ? tblibdir_env : "");
#endif

@* The Prefixed Input Files Stream Class.  In order to be free as far as the directory
where the files are stored is concerned, I need a variant if the |ifstream| class that is
aware of |TBLIBDIR|.

@s prefixed_ifstream int

@c
class prefixed_ifstream : public ifstream {
public:@/
    prefixed_ifstream(string filename) : ifstream() {
        @<Construct complete file name and open stream@>@;
    }    
};

@ Here we construct the complete (``prefixed'') filename, i.\,e.\ the filename with the
default directory given by |TBLIBDIR|, and open the stream in its default mode.

@<Construct complete file name and open stream@>=
        string prefixed_file_name(file_name_prefix);
        if (!file_name_prefix.empty()) {
            if (file_name_prefix[file_name_prefix.size()-1] != '/')
                prefixed_file_name = prefixed_file_name + "/";
        }
        prefixed_file_name = prefixed_file_name + filename;
        this->open(prefixed_file_name.c_str());
        if (!this->good()) cerr << "tbrplent: File " << prefixed_file_name << 
            " couldn't be opened" << endl;


@* Text Node Delimiters.  As explained above, they enclose text that belonged to a text
node in the original {\mc XML} file.  They are single characters, here realised by
their Unicode representation, because that's the way they come out of the {\mc XSLT}
processor.  (And \CPLUSPLUS/ still can't handle Unicode.)

@ Now we need the |string| class.

@<Header...@>=
#include <string>

@ The |start_delimiter| stands at the beginning, the |end_delimiter| at the end.  They
mustn't be used in the {\mc XML}-file under any circumstances.  They are utterly
unimportant in text generation anyway; after all, they are control characters.

|start_math_delimiter| and |end_math_delimiter| do the same thing for formulas.

|start_text_math_delimiter| and |end_text_math_delimiter| do the same thing for
text within formulas.  There is a subtle difference to real text nodes here:  A
\.{\BS text} command in a formula encloses a {\it real\/} text node, but all
terminal MathML elements encloses {\it mathematical\/} text nodes.  And these
two delimiters are for the latter case.  We will escape special characters
within these nodes, but we will still be in mathematical mode.

@c
const unsigned start_delimiter=0x98; /* (``start of string'')
                                            as Unicode number */
const unsigned end_delimiter=0x9c; /* (``string terminator'')
                                            as Unicode number */

const unsigned start_math_delimiter=0x96; /* (``start of guarded area'')
                                            as Unicode number */
const unsigned end_math_delimiter=0x97; /* (``end of guarded area'')
                                            as Unicode number */

const unsigned start_text_math_delimiter=0x86; /* (``start of selected area'')
                                            as Unicode number */
const unsigned end_text_math_delimiter=0x87; /* (``end of selected area'')
                                            as Unicode number */


@** The Class for Entities.  My entity struct/record/class (as you wish). |name| is the
entity name that is used in {\mc XML} constructs like \.{\&copy;}.  |latex_macro| is a
neat \LaTeX\ representation for that entity, e.\,g.\ \.{\\copyright}.

@s entity int

@c
struct entity {
    string name;
    string latex_macro;
};


@ My struct |entity| should be able to read itself from a stream.  It expects the
following: First the name of the entity (that is never used in this program, but makes
the entity file more readable), and secondly the corresponding \LaTeX\ macro, that is
supposed to reach to the end of the current line and stands between double quotes.

Therefore this line rest is first read into |rest|.  (And because |rest| is more
succinct than |e.latex_macro|.~\.{:-)}  After having done so, the |rest| is reduced to
the part between the |""|.

@c
istream& operator>>(istream& s, entity& e) {
    s >> e.name;
    string rest;
    getline(s,rest);
    while (!rest.empty() && rest[0] != '\"') 
        rest = rest.substr(1,rest.size()-1);
    while (!rest.empty()>0 && rest[rest.size()-1] != '\"')
        rest = rest.substr(0,rest.size()-1);
    if (rest.size() >= 2)
        rest = rest.substr(1,rest.size()-2);
    e.latex_macro = rest;
    return s;
}

@ Apparently we've used streams here.

@<Header...@>=
#include <iostream>
 
@* The Container for the Entity Table.  We also need another new datatype, namely
|entity_list|.  It's just an array of |entity|s with a key that is the Unicode number
of the respective entity.

At first, we need the |map| container here.

@<Header...@>=
#include <map>

@ Now we can define |entity_list|.

@s entity_list int

@c
typedef map<int,entity> entity_list;

@** Creating the Entity Table.  As the |main()| routine suggests, first we have to read
the entity table from the entity file with the name |entities_file_name|.  Otherwise we
wouldn't know what to do with most of the Unicodes.

@* Reading the Entities File.  This routine reads the entities from |entities_file| and
stores them in |tbook_entities|.

The entities file has a very simple format.  It's read line by line.  It consists of
three columns, separated by white space.  The first column contains the Unicode number,
the second the entity name (just for human convenience; it is never used in this
program), and the third one the \LaTeX\ macro (sequence).  Obviously the last two
columns have exactly the format expected by the |>>| operator for |entity|.

Comment lines are introduced by a \.{\#} in the {\it first\/} column.

@c
@<The function |string_to_dec()|@>@;
void read_entities(istream& entities_file, entity_list& tbook_entities) {
    entity e;
    string unicode_string;
    int unicode;
    string help;
    int number_of_entities = 0;

    while (entities_file >> unicode_string) {
        if (unicode_string[0] == '#') {     // Line is a comment.
            string rest;
            getline(entities_file, rest);  // gobble it
            continue;
        }
        if (!(entities_file >> e)) break;
        unicode = string_to_dec(unicode_string);
        if (!e.latex_macro.empty()) tbook_entities[unicode]=e;
        number_of_entities++; }
//    cerr << number_of_entities << " entities read.\n";
    if (tbook_entities[0xfffd].latex_macro.empty())
        tbook_entities[0xfffd].latex_macro = "\\replacechar ";
}

@* Hexadecimal to Decimal Conversion.  This helping routine takes its argument |s|,
interprets it as a hexadecimal number like \.{0x7a} and returns its decimal value.

For |string_to_dec()|, we first need |isalpha()| and things like that.

@<Header...@>=
#include <cstdlib>

@ Now for the routine itself.  For hexadecimal conversion, the number must follow an
\.{'x'}.  If it is not, the function assumes that |s| is a decimal number and converts it
as such.

@<The function |string_to_dec()|@>=
int string_to_dec(string number) {
    istringstream ss(number);
    int result = 0;

    if ( number.find("x") == string::npos)  
    /* Is |number| decimal or hexadecimal? */
        ss >> result;
    else {
        char c;
        do ss >> c; while (c != 'x');
        ss >> hex >> result;
    } 
    return result;
}

@* Adding Zapf's Dingbats.  The nice Dingbats are available in \LaTeX\ by the
\.{pifont.sty} package.  It's macro \.{\\ding} takes a number and creates the
corresponding Dingbat symbol.  So all we have to do here is to apply the proper ``zero
point shift'' between Unicode and original Dingbats encoding.

Unfortunately, there are some exceptions.

@s ostringstream int

@<Add Dingbats to entity table@>=
for (int a = 33; a <= 254; a++) {
    int unicode = 0x2700+a-32;  // Default offset
    ostringstream ding_command; 
    if (a >= 96) unicode -= 32;
    switch (a) { // Now for all the exceptions. Sigh.
        case 37: unicode = 0x260e;@+ break;
        case 42: unicode = 0x261b;@+ break;
        case 43: unicode = 0x261e;@+ break;
        case 72: unicode = 0x2605;@+ break;
        case 108: unicode = 0x25cf;@+ break;
        case 110: unicode = 0x25a0;@+ break;
        case 115: unicode = 0x25b2;@+ break;
        case 116: unicode = 0x25bc;@+ break;
        case 117: unicode = 0x25c6;@+ break;
        case 119: unicode = 0x25d7;@+ break;
        case 168: unicode = 0x2663;@+ break;
        case 169: unicode = 0x2666;@+ break;
        case 170: unicode = 0x2665;@+ break;
        case 171: unicode = 0x2660;@+ break;
    }
    if (a >= 172 && a <= 181) unicode = 0x2460 + a - 172;
    @[@,@]/* |213| to |215| are MathML arrows */
    ding_command << "\\ding{" << a << '}';
    tbook_entities[unicode].latex_macro = ding_command.str();
}

@ We've used a string stream here.

@<Header...@>=
#include <sstream>

@** Processing the Raw \LaTeX\ File.  Now for the main part, why we are here.  We can
assume that the the entities are read in and stored in |tbook_entities|, which is
passed here.  Now we read standard input character by character and test whether it's a
delimiter character.  If so, we enter the ``text node mode'' where we fix the special
symbols by substitution, so that they are printed nicely.

Moreover, we decrypt all occuring unicodes, or pass unsuspicious (i.\,e.\ {\mc ASCII})
characters unchanged to standard output.

@c
enum status {outer, in_text, in_math, in_text_math};
typedef stack<status> state_stack;

@<The function |handle_unicode()|@>@;

void process_tex_file(istream& in, ostream& out, entity_list& tbook_entities) {
    char c;
    state_stack state;
    state.push(outer);
    while (in.get(c))
        handle_unicode(c, in, out, state, tbook_entities);
    state.pop();
    if (!state.empty()) cerr << "tbrplent: Not all delimiters were closed" << endl;
}

@ We've used a stack container here.

@<Header...@>=
#include <stack>

@* Process Unicodes.  It may be possible that \LaTeX\ can digest Unicodes directly,
however I don't want to try that.  (I know of {\mc XML}\TeX\ and the Unicode package.)

Strictly speaking, I interpret {\mc UTF}-8 prefix codes with up to 21 significant bits
(contained in 4 bytes) here.  This also allows for the upper MathML entities.  The
first byte, which is already read into |c| when this function is entered, determines
how many bytes are yet to be read in and interpreted.  For further information read a
description of {\mc UTF}-8, it's quite simple but too tedious.

@<The function |handle_unicode()|@>=
@<The function |mask_and_shift()|@>@;
void handle_unicode(char c, istream& in, ostream& out, state_stack& state,
                    entity_list& tbook_entities) {
    unsigned long int unicode = 0;

    if ((unsigned char)c < 128) {  // normal {\mc ASCII} $\Rightarrow$ pass it and out
        if (state.top() == in_text || state.top() == in_text_math)
            @<Substitute \LaTeX's active characters@>@;
        else out.put(c);
        return;
    }
    if ((c & 0xe0) == 0xc0) {  // 11 bits in 2 bytes: first 2048 positions, umlauts etc.
        char c2;
        in.get(c2);
        unicode = mask_and_shift(c,0x1f,6) + mask_and_shift(c2,0x3f);
    } else if ((c & 0xf0) == 0xe0) {  /* 16 bis in 3 bytes: first 65536 positions,
                                         MathML, Indian etc. */
        char c2, c3;
        in.get(c2); in.get(c3);
        unicode = mask_and_shift(c,0x0f,12) + 
                  mask_and_shift(c2,0x3f,6) + 
                  mask_and_shift(c3,0x3f);
    } else if ((c & 0xf8) == 0xf0) {  // 21 bits in 4 bytes: first 2 million positions, for MathML
        char c2, c3, c4;
        in >> c2 >> c3 >> c4;
        unicode = mask_and_shift(c,0x07,18) + 
                  mask_and_shift(c2,0x3f,12) + 
                  mask_and_shift(c3,0x3f,6) + 
                  mask_and_shift(c4,0x3f);
    }
    @<Read and deal with special C1 delimiters@>@;
    @<Produce output for \LaTeX@>@;
}

@ This is a little bit awkward.  We have three different kinds of delimiters as
described above.  I have to read them and push the correct state on the state
stack, or, if an end delimiter occurs, pop the top state.  Some tests seem to
be appropriate in order to be rather sure that the style sheet didn't produce
probably (or surely) wrong nesting.

@<Read and deal with special C1 delimiters@>=
    if (unicode == start_delimiter) {
        if (state.top() == in_text)
            cerr << "tbrplent: warning: strange nesting of text delimiters"
                 << endl;
        state.push(in_text);
        return;
    }
    if (unicode == end_delimiter) {
        switch (state.top()) {
        case outer:
            cerr << "tbrplent: Internal error: end delimiter without start"
                " delimiter" << endl; break;
        case in_text: state.pop(); break;
        default:
            cerr << "tbrplent: Internal error: end text delimiter after"
                " another start delimiter" << endl; break;
        }
        return;
    }
    if (unicode == start_math_delimiter) {
        if (state.top() == in_math)
            cerr << "tbrplent: warning: strange nesting of math delimiters"
                 << endl;
        state.push(in_math);
        return;
    }
    if (unicode == end_math_delimiter) {
        switch (state.top()) {
        case outer:
            cerr << "tbrplent: Internal error: end math delimiter without start"
                " math delimiter" << endl; break;
        case in_math: state.pop(); break;
        default:
            cerr << "tbrplent: Internal error: end math delimiter after"
                " another start delimiter" << endl; break;
        }
        return;
    }
    if (unicode == start_text_math_delimiter) {
        if (state.top() != in_math)
            cerr << "tbrplent: warning: strange nesting of text math delimiters"
                 << endl;
        state.push(in_text_math);
        return;
    }
    if (unicode == end_text_math_delimiter) {
        switch (state.top()) {
        case outer:
            cerr << "tbrplent: Internal error: end text math delimiter without"
                " start text math delimiter" << endl; break;
        case in_text_math: state.pop(); break;
        default:
            cerr << "tbrplent: Internal error: end text math delimiter after"
                " another start delimiter" << endl; break;
        }
        return;
    }

@ The following routine is necessary because apparently only {\it signed\/} characters can
be read from a stream via |in.get()| and so I have to do much nasty type casting.

@<The function |mask_and_shift()|@>=
inline unsigned long int mask_and_shift(char c, unsigned char mask, 
                                        int shift_value = 0) {
    return (unsigned long)((unsigned char)c & mask) << shift_value;
}

@ This is the last phase, if something non-{\mc ASCII} was encountered: The \LaTeX\
output is produced.  There are two possibilities, either the character was non-{\mc
ASCII}, but at least Latin-1, then it is passed to the output directly (\LaTeX\ will
later work with the Latin-1 input encoding).  If it's so pathological that there it's
beyond slot 255, we look it up in the |tbook_entities| table and produce a latex macro
(sequence) as output.

If the \LaTeX\ macro is enclosed with \.{\$...\$\{\}}, then it's treated as pure math
code.  If it starts with \.{\\ifmmode}, it's treated as code that can survive in text and
math mode.  Otherwise, we assume that it's text mode only and embed it in
\AMS\TeX's \.{\\text\{...\}}.

If the Unicode is not representable by \LaTeX\ macros, the Replacement Character |0xFFFD|
is used.

The code is a little bit prolonged by the fact that before we can test whether a string
starts with a certain substring, we have to check whether the string is long enough.
Maybe a |starts_with()| routine would help here.

@<Produce output for \LaTeX@>=
    string latex_macro;
    if (unicode >=161 && unicode <= 255) /* actually |160|, but \.{"\~"} looks nicer in
                                            output. */
        latex_macro = char(unicode); 
    else {
        latex_macro = tbook_entities[unicode].latex_macro;
        if (latex_macro.empty()) {
            cerr << "tbrplent: Not found: 0x" << hex << unicode << endl;
            latex_macro = tbook_entities[0xFFFD].latex_macro;
            }
        if (state.top() == in_math || state.top() == in_text_math) {
            bool pure_math_macro=false, text_and_math_macro=false;
            if (latex_macro.size() >= 3)
                if (latex_macro.substr(latex_macro.size()-3,3) == "${}") 
                    pure_math_macro = true;
            if (pure_math_macro) {
                // Delete \LaTeX\ math delimiters \.{\$...\$\{\}}
                latex_macro.replace(latex_macro.size()-3,3,"");
                latex_macro.replace(0,1,"");
            } else {
                 if (latex_macro.size() >= 8)
                    if (latex_macro.substr(0,8) == "\\ifmmode")
                        text_and_math_macro = true;
                 if (!text_and_math_macro) latex_macro = "\\text{" + latex_macro + "}";
            }
        } else @<Adjust following space@>@;
    }
    out << latex_macro;


@ If we have a |latex_macro| like \.{\\texteuro\{\}}, the last \.{\{\}} should be replaced
with a space, if no space follows in the raw file.  The purpose is to have better kerning
or ligatures, and commands like \.{\\noboundary} wouldn't work without this.

@<Adjust following space@>=
if (latex_macro.size() >= 3) if (latex_macro.substr(latex_macro.size()-2,2) == "{}") {
    if (latex_macro.substr(latex_macro.size()-3,3) != "${}") {
        char following_char;
        in.get(following_char);
        if (!isspace(following_char))
            latex_macro.replace(latex_macro.size()-2,2," ");
        in.putback(following_char);
    }
}

@* Substitution of Active Characters.  The the second main task of this program.  We
are now (hopefully) within some text that used to be a text node in the original {\mc
XML} file.  So all ``special'' symbols should be printed as if they had no special
meaning at all -- they should be usual {\mc ASCII} characters.

In order to achieve that, we must also assure that all semantic changing ligatures are
not applied.

I must assume T1 font encoding here.

@<Substitute \LaTeX's active characters@>=
switch (c) {
    case '\"': out << "{\\mbox{\\char34{}}}"; @+ break;
    case '!': out << "!{}"; @+ break;  // to annhilate ligatures
    case '?': out << "?{}"; @+ break;  // to annhilate ligatures
    case '-': out << "-{}"; @+ break;  // to annhilate ligatures
    case '\'': out << "\'{}"; @+ break;  // to annhilate ligatures
    case '`': out << "`{}"; @+ break;  // to annhilate ligatures
    case '<': out << "<{}"; @+ break;  // to annhilate ligatures
    case '>': out << ">{}"; @+ break;  // to annhilate ligatures
    case '&': out << "\\&"; @+ break;
    case '$': out << "\\$"; @+ break;
    case '{': out << "\\{"; @+ break;
    case '|': out << "{\\mbox{\\char124{}}}"; @+ break;
    case '}': out << "\\}"; @+ break;
    case '%': out << "\\%"; @+ break;
    case '#': out << "{\\mbox{\\char35{}}}"; @+ break;
    case '^': out << "{\\mbox{\\char94{}}}"; @+ break;
    case '_': out << "{\\mbox{\\char95{}}}"; @+ break;
    case '~': out << "{\\mbox{\\char126{}}}"; @+ break;
    case '\\': out << "{\\mbox{\\char92{}}}"; @+ break;
    default: out.put(c); @+ break;
}

@ Here the |main()| routine finds its home.

@c
@<The main() function@>

@** Index.
