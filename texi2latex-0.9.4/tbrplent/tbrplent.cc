/*4:*/
#line 135 "tbrplent.w"

#define entities_file_name "tbents.txt" \


#line 136 "tbrplent.w"

/*2:*/
#line 111 "tbrplent.w"

#include <fstream> 

/*:2*//*9:*/
#line 190 "tbrplent.w"

#include <string> 

/*:9*//*13:*/
#line 262 "tbrplent.w"

#include <iostream> 

/*:13*//*14:*/
#line 271 "tbrplent.w"

#include <map> 

/*:14*//*18:*/
#line 325 "tbrplent.w"

#include <cstdlib> 

/*:18*//*21:*/
#line 386 "tbrplent.w"

#include <sstream> 

/*:21*//*23:*/
#line 416 "tbrplent.w"

#include <stack> 

/*:23*/
#line 137 "tbrplent.w"


using namespace std;

/*:4*//*5:*/
#line 145 "tbrplent.w"

const char*tblibdir_env_raw= getenv("TBLIBDIR");
const string tblibdir_env= (tblibdir_env_raw==NULL?"":tblibdir_env_raw);
#ifdef TBLIBDIR
const string file_name_prefix(tblibdir_env.size()> 0?tblibdir_env:TBLIBDIR);
#else
const string file_name_prefix(tblibdir_env.size()> 0?tblibdir_env:"");
#endif

/*:5*//*6:*/
#line 160 "tbrplent.w"

class prefixed_ifstream:public ifstream{
public:
prefixed_ifstream(string filename):ifstream(){
/*7:*/
#line 171 "tbrplent.w"

string prefixed_file_name(file_name_prefix);
if(!file_name_prefix.empty()){
if(file_name_prefix[file_name_prefix.size()-1]!='/')
prefixed_file_name= prefixed_file_name+"/";
}
prefixed_file_name= prefixed_file_name+filename;
this->open(prefixed_file_name.c_str());
if(!this->good())cerr<<"tbrplent: File "<<prefixed_file_name<<
" couldn't be opened"<<endl;


/*:7*/
#line 164 "tbrplent.w"

}
};

/*:6*//*10:*/
#line 206 "tbrplent.w"

const unsigned start_delimiter= 0x98;

const unsigned end_delimiter= 0x9c;


const unsigned start_math_delimiter= 0x96;

const unsigned end_math_delimiter= 0x97;


const unsigned start_text_math_delimiter= 0x86;

const unsigned end_text_math_delimiter= 0x87;



/*:10*//*11:*/
#line 229 "tbrplent.w"

struct entity{
string name;
string latex_macro;
};


/*:11*//*12:*/
#line 245 "tbrplent.w"

istream&operator>>(istream&s,entity&e){
s>>e.name;
string rest;
getline(s,rest);
while(!rest.empty()&&rest[0]!='\"')
rest= rest.substr(1,rest.size()-1);
while(!rest.empty()> 0&&rest[rest.size()-1]!='\"')
rest= rest.substr(0,rest.size()-1);
if(rest.size()>=2)
rest= rest.substr(1,rest.size()-2);
e.latex_macro= rest;
return s;
}

/*:12*//*15:*/
#line 278 "tbrplent.w"

typedef map<int,entity> entity_list;

/*:15*//*17:*/
#line 296 "tbrplent.w"

/*19:*/
#line 332 "tbrplent.w"

int string_to_dec(string number){
istringstream ss(number);
int result= 0;

if(number.find("x")==string::npos)

ss>>result;
else{
char c;
do ss>>c;while(c!='x');
ss>>hex>>result;
}
return result;
}

/*:19*/
#line 297 "tbrplent.w"

void read_entities(istream&entities_file,entity_list&tbook_entities){
entity e;
string unicode_string;
int unicode;
string help;
int number_of_entities= 0;

while(entities_file>>unicode_string){
if(unicode_string[0]=='#'){
string rest;
getline(entities_file,rest);
continue;
}
if(!(entities_file>>e))break;
unicode= string_to_dec(unicode_string);
if(!e.latex_macro.empty())tbook_entities[unicode]= e;
number_of_entities++;}

if(tbook_entities[0xfffd].latex_macro.empty())
tbook_entities[0xfffd].latex_macro= "\\replacechar ";
}

/*:17*//*22:*/
#line 398 "tbrplent.w"

enum status{outer,in_text,in_math,in_text_math};
typedef stack<status> state_stack;

/*24:*/
#line 428 "tbrplent.w"

/*26:*/
#line 531 "tbrplent.w"

inline unsigned long int mask_and_shift(char c,unsigned char mask,
int shift_value= 0){
return(unsigned long)((unsigned char)c&mask)<<shift_value;
}

/*:26*/
#line 429 "tbrplent.w"

void handle_unicode(char c,istream&in,ostream&out,state_stack&state,
entity_list&tbook_entities){
unsigned long int unicode= 0;

if((unsigned char)c<128){
if(state.top()==in_text||state.top()==in_text_math)
/*29:*/
#line 612 "tbrplent.w"

switch(c){
case'\"':out<<"{\\mbox{\\char34{}}}";break;
case'!':out<<"!{}";break;
case'?':out<<"?{}";break;
case'-':out<<"-{}";break;
case'\'':out<<"\'{}";break;
case'`':out<<"`{}";break;
case'<':out<<"<{}";break;
case'>':out<<">{}";break;
case'&':out<<"\\&";break;
case'$':out<<"\\$";break;
case'{':out<<"\\{";break;
case'|':out<<"{\\mbox{\\char124{}}}";break;
case'}':out<<"\\}";break;
case'%':out<<"\\%";break;
case'#':out<<"{\\mbox{\\char35{}}}";break;
case'^':out<<"{\\mbox{\\char94{}}}";break;
case'_':out<<"{\\mbox{\\char95{}}}";break;
case'~':out<<"{\\mbox{\\char126{}}}";break;
case'\\':out<<"{\\mbox{\\char92{}}}";break;
default:out.put(c);break;
}

/*:29*/
#line 436 "tbrplent.w"

else out.put(c);
return;
}
if((c&0xe0)==0xc0){
char c2;
in.get(c2);
unicode= mask_and_shift(c,0x1f,6)+mask_and_shift(c2,0x3f);
}else if((c&0xf0)==0xe0){

char c2,c3;
in.get(c2);in.get(c3);
unicode= mask_and_shift(c,0x0f,12)+
mask_and_shift(c2,0x3f,6)+
mask_and_shift(c3,0x3f);
}else if((c&0xf8)==0xf0){
char c2,c3,c4;
in>>c2>>c3>>c4;
unicode= mask_and_shift(c,0x07,18)+
mask_and_shift(c2,0x3f,12)+
mask_and_shift(c3,0x3f,6)+
mask_and_shift(c4,0x3f);
}
/*25:*/
#line 469 "tbrplent.w"

if(unicode==start_delimiter){
if(state.top()==in_text)
cerr<<"tbrplent: warning: strange nesting of text delimiters"
<<endl;
state.push(in_text);
return;
}
if(unicode==end_delimiter){
switch(state.top()){
case outer:
cerr<<"tbrplent: Internal error: end delimiter without start"
" delimiter"<<endl;break;
case in_text:state.pop();break;
default:
cerr<<"tbrplent: Internal error: end text delimiter after"
" another start delimiter"<<endl;break;
}
return;
}
if(unicode==start_math_delimiter){
if(state.top()==in_math)
cerr<<"tbrplent: warning: strange nesting of math delimiters"
<<endl;
state.push(in_math);
return;
}
if(unicode==end_math_delimiter){
switch(state.top()){
case outer:
cerr<<"tbrplent: Internal error: end math delimiter without start"
" math delimiter"<<endl;break;
case in_math:state.pop();break;
default:
cerr<<"tbrplent: Internal error: end math delimiter after"
" another start delimiter"<<endl;break;
}
return;
}
if(unicode==start_text_math_delimiter){
if(state.top()!=in_math)
cerr<<"tbrplent: warning: strange nesting of text math delimiters"
<<endl;
state.push(in_text_math);
return;
}
if(unicode==end_text_math_delimiter){
switch(state.top()){
case outer:
cerr<<"tbrplent: Internal error: end text math delimiter without"
" start text math delimiter"<<endl;break;
case in_text_math:state.pop();break;
default:
cerr<<"tbrplent: Internal error: end text math delimiter after"
" another start delimiter"<<endl;break;
}
return;
}

/*:25*/
#line 459 "tbrplent.w"

/*27:*/
#line 556 "tbrplent.w"

string latex_macro;
if(unicode>=161&&unicode<=255)

latex_macro= char(unicode);
else{
latex_macro= tbook_entities[unicode].latex_macro;
if(latex_macro.empty()){
cerr<<"tbrplent: Not found: 0x"<<hex<<unicode<<endl;
latex_macro= tbook_entities[0xFFFD].latex_macro;
}
if(state.top()==in_math||state.top()==in_text_math){
bool pure_math_macro= false,text_and_math_macro= false;
if(latex_macro.size()>=3)
if(latex_macro.substr(latex_macro.size()-3,3)=="${}")
pure_math_macro= true;
if(pure_math_macro){

latex_macro.replace(latex_macro.size()-3,3,"");
latex_macro.replace(0,1,"");
}else{
if(latex_macro.size()>=8)
if(latex_macro.substr(0,8)=="\\ifmmode")
text_and_math_macro= true;
if(!text_and_math_macro)latex_macro= "\\text{"+latex_macro+"}";
}
}else/*28:*/
#line 591 "tbrplent.w"

if(latex_macro.size()>=3)if(latex_macro.substr(latex_macro.size()-2,2)=="{}"){
if(latex_macro.substr(latex_macro.size()-3,3)!="${}"){
char following_char;
in.get(following_char);
if(!isspace(following_char))
latex_macro.replace(latex_macro.size()-2,2," ");
in.putback(following_char);
}
}

/*:28*/
#line 582 "tbrplent.w"

}
out<<latex_macro;


/*:27*/
#line 460 "tbrplent.w"

}

/*:24*/
#line 402 "tbrplent.w"


void process_tex_file(istream&in,ostream&out,entity_list&tbook_entities){
char c;
state_stack state;
state.push(outer);
while(in.get(c))
handle_unicode(c,in,out,state,tbook_entities);
state.pop();
if(!state.empty())cerr<<"tbrplent: Not all delimiters were closed"<<endl;
}

/*:22*//*30:*/
#line 638 "tbrplent.w"

/*3:*/
#line 120 "tbrplent.w"

int main(){
prefixed_ifstream entities_file(entities_file_name);
entity_list tbook_entities;

read_entities(entities_file,tbook_entities);
/*20:*/
#line 357 "tbrplent.w"

for(int a= 33;a<=254;a++){
int unicode= 0x2700+a-32;
ostringstream ding_command;
if(a>=96)unicode-= 32;
switch(a){
case 37:unicode= 0x260e;break;
case 42:unicode= 0x261b;break;
case 43:unicode= 0x261e;break;
case 72:unicode= 0x2605;break;
case 108:unicode= 0x25cf;break;
case 110:unicode= 0x25a0;break;
case 115:unicode= 0x25b2;break;
case 116:unicode= 0x25bc;break;
case 117:unicode= 0x25c6;break;
case 119:unicode= 0x25d7;break;
case 168:unicode= 0x2663;break;
case 169:unicode= 0x2666;break;
case 170:unicode= 0x2665;break;
case 171:unicode= 0x2660;break;
}
if(a>=172&&a<=181)unicode= 0x2460+a-172;

ding_command<<"\\ding{"<<a<<'}';
tbook_entities[unicode].latex_macro= ding_command.str();
}

/*:20*/
#line 126 "tbrplent.w"

process_tex_file(cin,cout,tbook_entities);
return 0;
}

/*:3*/
#line 639 "tbrplent.w"


/*:30*/
