canvas .c
pack .c

for {set i 0} {$i<20} {incr i} {
    set xmax 300
    set ymax 200
    set dy 10
    set dx 15
    .c create rectangle [expr $i*$dx] [expr $ymax-$i*$dy] $xmax [expr $ymax-($i+1)*$dy] -fill red
}

update
.c postscript -file step2.eps
.c delete all
for {set i 0} {$i<5} {incr i} {
    set xmax 300
    set ymax 200
    set dy 40
    set dx 60
    .c create rectangle [expr $i*$dx] $ymax [expr ($i+1)*$dx] [expr $ymax-($i+0.5)*$dy] -fill green
}

update
.c postscript -file step2a.eps

exit