<?xml version="1.0"?>
<!--
    common.mod.xsl - routines of general use that are called from elsewhere.
	$Id: common.mod.xsl,v 1.1 2008/04/07 09:25:57 alatina Exp $	

    Copyright © 2004, 2005 Torsten Bronger <bronger@physik.rwth-aachen.de>.

    This file is part of texi2latex.

    texi2latex is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License as published by the Free
    Software Foundation; either version 2 of the License, or (at your option)
    any later version.

    texi2latex is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with texi2latex; if not, write to the Free Software Foundation, Inc., 59
    Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<!DOCTYPE xsl:stylesheet [
<!ENTITY bph           "&#x0082;">  <!-- Break permitted here -->
]>

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'"/>
<xsl:variable name="lowercase" select="'abcdefghijklmnopqrstuvwxyz'"/>

<!-- This routine inserts &bph; (break permitted here) after every character
     in $string that is included in $break-after.  By the way, the &bph;'s are
     not processed by LaTeX, but tbrplent. -->

<xsl:template name="insert-breakpoints">
  <xsl:param name="string"/>
  <xsl:param name="break-after"/>
  <xsl:choose>
    <xsl:when test="string-length($break-after) &gt; 1">
      <xsl:call-template name="insert-breakpoints">
        <xsl:with-param name="break-after" select="substring($break-after,2)"/>
        <xsl:with-param name="string">
          <xsl:call-template name="substitute-in-string">
            <xsl:with-param name="str" select="$string"/>
            <xsl:with-param name="look-for" select="substring($break-after,1,1)"/>
            <xsl:with-param name="replace-with" select="concat(substring($break-after,1,1),'&bph;')"/>
          </xsl:call-template>
        </xsl:with-param>
      </xsl:call-template>
    </xsl:when>
    <xsl:when test="string-length($break-after) = 1">  <!-- Just for performance reasons -->
      <xsl:call-template name="substitute-in-string">
        <xsl:with-param name="str" select="$string"/>
        <xsl:with-param name="look-for" select="$break-after"/>
        <xsl:with-param name="replace-with" select="concat($break-after,'&bph;')"/>
      </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="$string"/>   <!-- This should never happen -->
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!-- This is a very general substring replacements routine.  It replaces almost
     all occurences of $look-for in $str with $replace-with.

     If $replace-at-very-end is false, it does not replace at the very end of
     the string.  This is because in URLs line breaks at the end would be
     silly.  ATTENTION: The default for $replace-at-very-end is false()! -->

<xsl:template name="substitute-in-string">
  <xsl:param name="str"/>
  <xsl:param name="look-for"/>
  <xsl:param name="replace-with"/>
  <xsl:param name="replace-at-very-end" select="false()"/>
  <xsl:choose>
    <xsl:when test="translate(substring-after($str,$look-for),$end-delimiter,'') = ''
                    and not($replace-at-very-end)">
      <xsl:value-of select="$str"/>
    </xsl:when>
    <xsl:when test="contains($str,$look-for)">
      <xsl:value-of select="substring-before($str,$look-for)"/>
      <xsl:value-of select="$replace-with"/>
      <xsl:call-template name="substitute-in-string">
        <xsl:with-param name="str" select="substring-after($str,$look-for)"/>
        <xsl:with-param name="look-for" select="$look-for"/>
        <xsl:with-param name="replace-with" select="$replace-with"/>
        <xsl:with-param name="replace-at-very-end" select="$replace-at-very-end"/>
      </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="$str"/>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!-- In this routine I normalise strings that are supposed to serve as an
     identifier: All diacritic signed are removed (i.e. only the naked Latin
     letter remains), all letters are converted to lowercase, spaces become
     dashs, and special glyphes are simply removed.  It doesn't cover
     everything, of course. -->

<xsl:template name="clean-up-identifier">
  <xsl:param name="identifier"/>
  <xsl:variable name="identifier-normalized" select="normalize-space($identifier)"/>
  <!-- Unfortunately, I can only delete all superfluous charactes *except* for
       the apostrophe.  This will be done below. -->
  <xsl:variable name="identifier-almost-cleaned-up">
    <xsl:value-of select="translate($identifier-normalized,
      concat('ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõö÷øùúûüýþÿ',
             $uppercase,' !&#34;$%&amp;/()=?{[]}\#*+.,:;_`@&lt;&gt;|~^'),
      concat('aaaaaaaceeeeiiiidnoooooxouuuuytsaaaaaaaceeeeiiiidnooooo-ouuuuyty',
             $lowercase,'-'))"/>
  </xsl:variable>
  <!-- And now for the apostrophe, and give it back. -->
  <xsl:value-of select='translate($identifier-almost-cleaned-up,"&apos;","")'/>
</xsl:template>

</xsl:stylesheet>
