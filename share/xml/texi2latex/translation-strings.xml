<!--
    translation-strings.xml - translations of snippets in many human
    languages.  
	$Id: translation-strings.xml,v 1.1 2008/04/07 09:25:57 alatina Exp $	

    Copyright © 2004, 2005 Torsten Bronger <bronger@physik.rwth-aachen.de>.

    This file is part of texi2latex.

    texi2latex is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License as published by the Free
    Software Foundation; either version 2 of the License, or (at your option)
    any later version.

    texi2latex is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with texi2latex; if not, write to the Free Software Foundation, Inc., 59
    Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<!-- This file is UTF-8 encoded. -->

<!-- Here the text patches for all supported languages are stored.  If you know
     the proper translation of a commented-out line, please mail it to me!

     It is important that the more general language codes come first, so "en"
     before "en-GB". -->

<!-- At the moment, most of the lines of this file - originally taken from the
     tbook project - are not actually used by texi2latex.  It is included into
     i18n.mod.xsl. -->

<!-- English -->

<!-- Actually we only copy here. -->

<loc:strings xmlns:loc="local">
  <loc:stringgroup xml:lang="en">
    <loc:string key="Table of Contents">Table of Contents</loc:string>
    <loc:string key="All rights reserved.">All rights reserved.</loc:string>
    <loc:string key="No warranty given.">No warranty given.</loc:string>
    <loc:string key="Abstract">Abstract</loc:string>
    <loc:string key="Part">Part</loc:string>
    <loc:string key="Chapter">Chapter</loc:string>
    <loc:string key="Appendix">Appendix</loc:string>
    <loc:string key="here">here</loc:string>
    <loc:string key="Figure">Figure</loc:string>
    <loc:string key="Table">Table</loc:string>
    <loc:string key="and">and</loc:string>
    <loc:string key="editor">editor</loc:string>
    <loc:string key="in">in</loc:string>
    <loc:string key="References">References</loc:string>
    <loc:string key="Master thesis">Master thesis</loc:string>
    <loc:string key="PhD thesis">PhD thesis</loc:string>
    <loc:string key="Index">Index</loc:string>
    <loc:string key="Subject:">Subject:</loc:string>
    <loc:string key="typeset by">typeset by</loc:string>
    <loc:string key="see">see</loc:string>
    <loc:string key="Click for original bitmap">Click for original bitmap</loc:string>
    <loc:string key="prev">previous</loc:string>
    <loc:string key="next">next</loc:string>
    <loc:string key="Theorem">Theorem</loc:string>
    <loc:string key="Proof">Proof</loc:string>
    <loc:string key="if">if</loc:string>
    <loc:string key="section">section</loc:string>
    <loc:string key="node">node</loc:string>
    <loc:string key="Info file">Info file</loc:string>
    <loc:string key="plain" kind="code">tbenl</loc:string>
    <loc:string key="tbenh" kind="code">tbenh</loc:string>
  </loc:stringgroup>

<!-- German -->

  <loc:stringgroup xml:lang="de" nouns-lowercase="no">
    <loc:string key="Table of Contents">Inhaltsverzeichnis</loc:string>
    <loc:string key="All rights reserved.">Alle Rechte vorbehalten.</loc:string>
    <loc:string key="No warranty given.">Keine Gewähr für irgendwas.</loc:string>
    <loc:string key="Abstract">Zusammenfassung</loc:string>
    <loc:string key="Part">Teil</loc:string>
    <loc:string key="Chapter">Kapitel</loc:string>
    <loc:string key="Appendix">Anhang</loc:string>
    <loc:string key="here">hier</loc:string>
    <loc:string key="Figure">Abbildung</loc:string>
    <loc:string key="Table">Tabelle</loc:string>
    <loc:string key="and">und</loc:string>
    <loc:string key="editor">Hrsg.</loc:string>
    <loc:string key="in">in</loc:string>
    <loc:string key="References">Literatur</loc:string>
    <loc:string key="Master thesis">Diplomarbeit</loc:string>
    <loc:string key="PhD thesis">Doktorarbeit</loc:string>
    <loc:string key="Index">Stichwortverzeichnis</loc:string>
    <loc:string key="typeset by">Satz:</loc:string>
    <loc:string key="Subject:">Betreff:</loc:string>
    <loc:string key="see">siehe</loc:string>
    <loc:string key="Click for original bitmap">Für Original-Bitmap anklicken</loc:string>
    <loc:string key="prev">zurück</loc:string>
    <loc:string key="next">nächstes</loc:string>
    <loc:string key="Theorem">Satz</loc:string>
    <loc:string key="Proof">Beweis</loc:string>
    <loc:string key="if">falls</loc:string>
    <loc:string key="section">Abschnitt</loc:string>
    <loc:string key="node">Knoten</loc:string>
    <loc:string key="Info file">Info-Datei</loc:string>
    <loc:string key="plain" kind="code">tbdel</loc:string>
    <loc:string key="tbenh" kind="code">tbdeh</loc:string>
  </loc:stringgroup>

<!-- French -->

<!-- The French language is a good example for strings that contain more than
     just plain text.  The "Figure" string is not only "Fig.", but
     "\textsc{Fig.}", i.e. the markup can be given, too.  Unfortunately, you
     can't easily change the *structure* of the substitution.  Therefore a
     "Première Parte" instead of "Part&#xa0;I" is not so simple. -->

<!-- Thanks to Alexis <dth2001@caramail.com>. -->

  <loc:stringgroup xml:lang="fr">
    <loc:string key="Table of Contents">Table des matières</loc:string>
    <loc:string key="All rights reserved.">Touts droits réservés.</loc:string>
    <loc:string key="No warranty given.">Sans garantie.</loc:string>
    <loc:string key="Abstract">Résumé</loc:string>
    <loc:string key="Part">Partie</loc:string>
    <loc:string key="Chapter">Chapitre</loc:string>
    <loc:string key="Appendix">Annexe</loc:string>
    <loc:string key="here">ici</loc:string>
    <loc:string key="Figure"><sc>Fig.</sc></loc:string>
    <loc:string key="Table"><sc>Tab.</sc></loc:string>
    <loc:string key="and">et</loc:string>
    <loc:string key="editor">rédacteur</loc:string>
    <loc:string key="in">dans</loc:string>
    <loc:string key="References">Références</loc:string>
    <loc:string key="Master thesis">Thèse de maître</loc:string>
    <loc:string key="PhD thesis">Thèse de doctorat</loc:string>
    <loc:string key="Index">Index</loc:string>
    <loc:string key="Subject:">Object:</loc:string>
    <!--loc:string key="typeset by">typeset by</loc:string-->
    <loc:string key="see">voir</loc:string>
    <loc:string key="Click for original bitmap">Cliquer ici pour le bitmap
      original</loc:string>
    <loc:string key="prev">précédent</loc:string>
    <loc:string key="next">suivant</loc:string>
    <loc:string key="Theorem">Theorème</loc:string>
    <loc:string key="Proof">Démonstration</loc:string>
    <loc:string key="if">si</loc:string>
    <loc:string key="section">pages</loc:string>  <!-- FixMe: probably
		                                       subopimal -->
    <!--loc:string key="node">node</loc:string-->
    <!--loc:string key="Info file">Info file</loc:string-->
    <loc:string key="plain" kind="code">tbfrl</loc:string>
    <loc:string key="tbenh" kind="code">tbfrh</loc:string>
  </loc:stringgroup>

<!-- Italian -->

<!-- FixMe: "Click for original bitmap", "Subject:", "typeset by", and
     "Theorem" are non-verified. -->

  <loc:stringgroup xml:lang="it">
    <loc:string key="Table of Contents">Indice</loc:string>
    <loc:string key="All rights reserved.">Tutti i diritti riservati.</loc:string>
    <!--loc:string key="No warranty given.">No warranty given.</loc:string-->
    <loc:string key="Abstract">Sommario</loc:string>
    <loc:string key="Part">Parte</loc:string>
    <loc:string key="Chapter">Capitolo</loc:string>
    <loc:string key="Appendix">Appendice</loc:string>
    <loc:string key="here">qui</loc:string>
    <loc:string key="Figure">Figura</loc:string>
    <loc:string key="Table">Tabella</loc:string>
    <loc:string key="and">e</loc:string>
    <loc:string key="editor">cura</loc:string>
    <loc:string key="in">in</loc:string>
    <loc:string key="References">Riferimenti bibliografici</loc:string>
    <loc:string key="Master thesis">Tesi di laurea</loc:string>
    <loc:string key="PhD thesis">Tesi di dottorato</loc:string>
    <loc:string key="Index">Indice analitico</loc:string>
    <loc:string key="Subject:">Oggetto:</loc:string>
    <loc:string key="typeset by">composizione di</loc:string>
    <loc:string key="see">vedi</loc:string>
    <loc:string key="Click for original bitmap">Clicca qui per il originale</loc:string>
    <loc:string key="prev">indietro</loc:string>
    <loc:string key="next">avanti</loc:string>
    <loc:string key="Theorem">Teorema</loc:string>
    <loc:string key="Proof">Dimostrazione</loc:string>
    <loc:string key="if">qualora</loc:string>
    <loc:string key="section">sezione</loc:string>
    <!--loc:string key="node">node</loc:string-->
    <!--loc:string key="Info file">Info file</loc:string-->
    <loc:string key="plain" kind="code">tbitl</loc:string>
    <loc:string key="tbenh" kind="code">tbith</loc:string>
  </loc:stringgroup>

<!-- Spanish -->

<!-- Thanks to Francesc Alted. -->

  <loc:stringgroup xml:lang="es">
    <loc:string key="Table of Contents">Índice</loc:string>
    <loc:string key="All rights reserved.">Todos los derechos reservados.</loc:string>
    <loc:string key="No warranty given.">No se ofrece garantía de ningún tipo.</loc:string>
    <loc:string key="Abstract">Resumen</loc:string>
    <loc:string key="Part">Parte</loc:string>
    <loc:string key="Chapter">Capítulo</loc:string>
    <loc:string key="Appendix">Apéndice</loc:string>
    <loc:string key="here">aquí</loc:string>
    <loc:string key="Figure">Figura</loc:string>
    <loc:string key="Table">Tabla</loc:string>
    <loc:string key="and">y</loc:string>
    <loc:string key="editor">editor</loc:string>
    <loc:string key="in">en</loc:string>
    <loc:string key="References">Bibliografía</loc:string>
    <loc:string key="Master thesis">Proyecto Fin de Carrera</loc:string>
    <loc:string key="PhD thesis">Tesis Doctoral</loc:string>
    <loc:string key="Index">Índice alfabético</loc:string>
    <loc:string key="Subject:">Asunto:</loc:string>
    <loc:string key="typeset by">maquetado por</loc:string>
    <loc:string key="see">véase</loc:string>
    <loc:string key="Click for original bitmap">Haz click aquí para ver
      la imagen original</loc:string>
    <loc:string key="prev">anterior</loc:string>
    <loc:string key="next">siguiente</loc:string>
    <!--loc:string key="Theorem">Theorem</loc:string-->
    <loc:string key="Proof">Demostración</loc:string>
    <!--loc:string key="if">if</loc:string-->
    <loc:string key="section">sección</loc:string>
    <!--loc:string key="node">node</loc:string-->
    <!--loc:string key="Info file">Info file</loc:string-->
    <loc:string key="plain" kind="code">tbesl</loc:string>
    <loc:string key="tbenh" kind="code">tbesh</loc:string>
  </loc:stringgroup>

<!-- Catalan -->

<!-- Thanks to Francesc Alted. -->

  <loc:stringgroup xml:lang="ca">
    <loc:string key="Table of Contents">Índex</loc:string>
    <loc:string key="All rights reserved.">Tots els drets reservats.</loc:string>
    <loc:string key="No warranty given.">No es dóna garantia de cap mena.</loc:string>
    <loc:string key="Abstract">Resum</loc:string>
    <loc:string key="Part">Part</loc:string>
    <loc:string key="Chapter">Capítol</loc:string>
    <loc:string key="Appendix">Apèndix</loc:string>
    <loc:string key="here">ací</loc:string>
    <loc:string key="Figure">Figura</loc:string>
    <loc:string key="Table">Taula</loc:string>
    <loc:string key="and">i</loc:string>
    <loc:string key="editor">editor</loc:string>
    <loc:string key="in">a</loc:string>
    <loc:string key="References">Bibliografia</loc:string>
    <loc:string key="Master thesis">Projecte Cap de Carrera</loc:string>
    <loc:string key="PhD thesis">Tesi Doctoral</loc:string>
    <loc:string key="Index">Índex alfabètic</loc:string>
    <loc:string key="Subject:">Assumpte:</loc:string>
    <loc:string key="typeset by">maquetat per</loc:string>
    <loc:string key="see">vegeu</loc:string>
    <loc:string key="Click for original bitmap">Pica ací per veure l'imatge original</loc:string>
    <loc:string key="prev">anterior</loc:string>
    <loc:string key="next">seguent</loc:string>
    <!--loc:string key="Theorem">Theorem</loc:string-->
    <loc:string key="Proof">Demostració</loc:string>
    <!--loc:string key="if">if</loc:string-->
    <!--loc:string key="section">section</loc:string-->
    <!--loc:string key="node">node</loc:string-->
    <!--loc:string key="Info file">Info file</loc:string-->
    <loc:string key="plain" kind="code">tbcal</loc:string>
    <loc:string key="tbenh" kind="code">tbcah</loc:string>
  </loc:stringgroup>

<!-- Polish -->

<!-- Thanks to Andrzej Matyka. -->

  <loc:stringgroup xml:lang="pl">
    <loc:string key="Table of Contents">Spis treści</loc:string>
    <loc:string key="All rights reserved.">Wszelkie prawa zastrzeżone.</loc:string>
    <loc:string key="No warranty given.">Nie udziela się gwarancji.</loc:string>
    <loc:string key="Abstract">Abstrakt</loc:string>
    <loc:string key="Part">Część</loc:string>
    <loc:string key="Chapter">Rozdział</loc:string>
    <loc:string key="Appendix">Dodatek</loc:string>
    <loc:string key="here">tutaj</loc:string>
    <loc:string key="Figure">Rysunek</loc:string>
    <loc:string key="Table">Tabela</loc:string>
    <loc:string key="and">i</loc:string>
    <loc:string key="editor">redaktor</loc:string>
    <loc:string key="in">w</loc:string>
    <loc:string key="References">Literatura</loc:string>
    <loc:string key="Master thesis">Praca magisterska</loc:string>
    <loc:string key="PhD thesis">Rozprawa doktorska</loc:string>
    <loc:string key="Index">Indeks</loc:string>
    <loc:string key="typeset by">Skład wykonał</loc:string>
    <loc:string key="Subject:">Przedmiot:</loc:string>
    <loc:string key="see">patrz</loc:string>
    <loc:string key="Click for original bitmap">Kliknij, aby otrzymać 
      originalną mapę bitową</loc:string>
    <loc:string key="prev">poprzedni</loc:string>
    <loc:string key="next">następny</loc:string>
    <loc:string key="Theorem">Twierdzenie</loc:string>
    <loc:string key="Proof">Dowód</loc:string>
    <!--loc:string key="if">if</loc:string-->
    <!--loc:string key="section">section</loc:string-->
    <!--loc:string key="node">node</loc:string-->
    <!--loc:string key="Info file">Info file</loc:string-->
    <loc:string key="plain" kind="code">tbpll</loc:string>
    <loc:string key="tbenh" kind="code">tbplh</loc:string>
  </loc:stringgroup>

<!-- Dutch -->

<!-- Thanks to Alan Luth. -->

  <loc:stringgroup xml:lang="nl">
    <loc:string key="Table of Contents">Inhoudsopgave</loc:string>
    <loc:string key="All rights reserved.">Alle rechten zijn voorbehouden.</loc:string>
    <loc:string key="No warranty given.">Er worden geen garanties gegeven.</loc:string>
    <loc:string key="Abstract">Samenvatting</loc:string>
    <loc:string key="Part">Deel</loc:string>
    <loc:string key="Chapter">Hoofdstuk</loc:string>
    <loc:string key="Appendix">Aanhangsel</loc:string>
    <loc:string key="here">hier</loc:string>
    <loc:string key="Figure">Figuur</loc:string>
    <loc:string key="Table">Tabel</loc:string>
    <loc:string key="and">en</loc:string>
    <loc:string key="editor">redactie</loc:string>
    <loc:string key="in">in</loc:string>
    <loc:string key="References">Referenties</loc:string>
    <loc:string key="Master thesis">Scriptie</loc:string>
    <loc:string key="PhD thesis">Proefschrift</loc:string>
    <loc:string key="Index">Register</loc:string>
    <loc:string key="Subject:">Onderwerp:</loc:string>
    <loc:string key="typeset by">typeset door</loc:string>
    <loc:string key="see">zie</loc:string>
    <loc:string key="Click for original bitmap">Klik voor de originele bitmap</loc:string>
    <loc:string key="prev">vorige</loc:string>
    <loc:string key="next">volgende</loc:string>
    <loc:string key="Theorem">Stelling</loc:string>
    <loc:string key="Proof">Bewĳs</loc:string>
    <!--loc:string key="if">if</loc:string-->
    <loc:string key="section">sectie</loc:string>
    <!--loc:string key="node">node</loc:string-->
    <!--loc:string key="Info file">Info file</loc:string-->
    <loc:string key="plain" kind="code">tbnll</loc:string>
    <loc:string key="tbenh" kind="code">tbnlh</loc:string>
  </loc:stringgroup>

<!-- Swedish -->

<!-- Thanks to Pär Boman. -->

  <loc:stringgroup xml:lang="sv">
    <loc:string key="Table of Contents">Innehållsförteckning</loc:string>
    <loc:string key="All rights reserved.">Alla rättigheter 
      reserverade.</loc:string>
    <loc:string key="No warranty given.">Ingen garanti gives.</loc:string>
    <loc:string key="Abstract">Sammanfattning</loc:string>
    <loc:string key="Part">Del</loc:string>
    <loc:string key="Chapter">Kapitel</loc:string>
    <loc:string key="Appendix">Appendix</loc:string>
    <loc:string key="here">här</loc:string>
    <loc:string key="Figure">Figur</loc:string>
    <loc:string key="Table">Tabell</loc:string>
    <loc:string key="and">och</loc:string>
    <loc:string key="editor">redaktör</loc:string>
    <loc:string key="in">i</loc:string>
    <loc:string key="References">Referenser</loc:string>
    <loc:string key="Master thesis">Avhandling</loc:string>
    <loc:string key="PhD thesis">Doktorsavhandling</loc:string>
    <loc:string key="Index">Index</loc:string>
    <loc:string key="Subject:">Ämne:</loc:string>
    <loc:string key="typeset by">typeset av</loc:string>
    <loc:string key="see">se</loc:string>
    <loc:string key="Click for original bitmap">Klicka för original 
      bitmap</loc:string>
    <loc:string key="prev">föregående</loc:string>
    <loc:string key="next">nästa</loc:string>
    <loc:string key="Theorem">Teorem</loc:string>
    <loc:string key="Proof">Bevis</loc:string>
    <!--loc:string key="if">if</loc:string-->
    <!--loc:string key="section">section</loc:string-->
    <!--loc:string key="node">node</loc:string-->
    <!--loc:string key="Info file">Info file</loc:string-->
    <loc:string key="plain" kind="code">tbenl</loc:string>
    <loc:string key="tbenh" kind="code">tbenh</loc:string>
  </loc:stringgroup>

<!-- Danish -->

<!-- Thanks to Lars Juel Nielsen and Bonnie Yelverton. -->

  <loc:stringgroup xml:lang="da">
    <loc:string key="Table of Contents">Indhold</loc:string>
    <loc:string key="All rights reserved.">Alle rettighedder forbeholdt.</loc:string>
    <loc:string key="No warranty given.">Ingen garantier udstedes.</loc:string>
    <loc:string key="Abstract">Resumé</loc:string>
    <loc:string key="Part">Del</loc:string>
    <loc:string key="Chapter">Kapitel</loc:string>
    <loc:string key="Appendix">Bilag</loc:string>
    <loc:string key="here">her</loc:string>
    <loc:string key="Figure">Figur</loc:string>
    <loc:string key="Table">Tabel</loc:string>
    <loc:string key="and">og</loc:string>
    <loc:string key="editor">redaktør</loc:string>
    <loc:string key="in">i</loc:string>
    <loc:string key="References">Litteratur</loc:string>
    <loc:string key="Master thesis">Speciale</loc:string>
    <loc:string key="PhD thesis">ph.d.-afhandling</loc:string>
    <loc:string key="Index">Stikordsregister</loc:string>
    <loc:string key="Subject:">Emne:</loc:string>
    <loc:string key="typeset by">Sat af</loc:string>
    <loc:string key="see">se</loc:string>
    <loc:string key="Click for original bitmap">Klik for original bitmap</loc:string>
    <loc:string key="prev">forrige</loc:string>
    <loc:string key="next">næste</loc:string>
    <loc:string key="Theorem">Sætning</loc:string>
    <loc:string key="Proof">Bevis</loc:string>
    <!--loc:string key="if">if</loc:string-->
    <!--loc:string key="section">section</loc:string-->
    <!--loc:string key="node">node</loc:string-->
    <!--loc:string key="Info file">Info file</loc:string-->
    <loc:string key="plain" kind="code">tbdal</loc:string>
    <loc:string key="tbenh" kind="code">tbdah</loc:string>
  </loc:stringgroup>

<!-- Norwegian - Bokmal -->

  <loc:stringgroup xml:lang="no">
    <loc:string key="Table of Contents">Innhold</loc:string>
    <!--loc:string key="All rights reserved."></loc:string-->
    <!--loc:string key="No warranty given."></loc:string-->
    <loc:string key="Abstract">Sammendrag</loc:string>
    <loc:string key="Part">Del</loc:string>
    <loc:string key="Chapter">Kapittel</loc:string>
    <loc:string key="Appendix">Tillegg</loc:string>
    <!--loc:string key="here"></loc:string-->
    <loc:string key="Figure">Figur</loc:string>
    <loc:string key="Table">Tabell</loc:string>
    <loc:string key="and">og</loc:string>
    <loc:string key="editor">redaktør</loc:string>
    <loc:string key="in">i</loc:string>
    <loc:string key="References">Referanser</loc:string>
    <loc:string key="Master thesis">Hovedoppgave</loc:string>
    <loc:string key="PhD thesis">Doktorgradsavhandling</loc:string>
    <loc:string key="Index">Register</loc:string>
    <!--loc:string key="Subject:"></loc:string-->
    <!--loc:string key="typeset by"></loc:string-->
    <loc:string key="see">se</loc:string>
    <!--loc:string key="Click for original bitmap"></loc:string-->
    <loc:string key="prev">forrige</loc:string>
    <loc:string key="next">neste</loc:string>
    <!--loc:string key="Theorem"></loc:string-->
    <loc:string key="Proof">Bevis</loc:string>
    <!--loc:string key="if">if</loc:string-->
    <!--loc:string key="section">section</loc:string-->
    <!--loc:string key="node">node</loc:string-->
    <!--loc:string key="Info file">Info file</loc:string-->
    <loc:string key="plain" kind="code">tbnol</loc:string>
    <loc:string key="tbenh" kind="code">tbnoh</loc:string>
  </loc:stringgroup>

<!-- Norwegian - Nynorsk -->

  <loc:stringgroup xml:lang="no-nyn">
    <loc:string key="Table of Contents">Innhald</loc:string>
    <!--loc:string key="All rights reserved."></loc:string-->
    <!--loc:string key="No warranty given."></loc:string-->
    <loc:string key="Abstract">Samandrag</loc:string>
    <loc:string key="Part">Del</loc:string>
    <loc:string key="Chapter">Kapittel</loc:string>
    <loc:string key="Appendix">Tillegg</loc:string>
    <!--loc:string key="here"></loc:string-->
    <loc:string key="Figure">Figur</loc:string>
    <loc:string key="Table">Tabell</loc:string>
    <loc:string key="and">og</loc:string>
    <!--loc:string key="editor"></loc:string-->
    <loc:string key="in">i</loc:string>
    <loc:string key="References">Referansar</loc:string>
    <!--loc:string key="Master thesis"></loc:string-->
    <!--loc:string key="PhD thesis"></loc:string-->
    <loc:string key="Index">Register</loc:string>
    <!--loc:string key="Subject:"></loc:string-->
    <!--loc:string key="typeset by"></loc:string-->
    <loc:string key="see">Sjå</loc:string>
    <!--loc:string key="Click for original bitmap"></loc:string-->
    <loc:string key="prev">att</loc:string>
    <loc:string key="next">fram</loc:string>
    <!--loc:string key="Theorem"></loc:string-->
    <loc:string key="Proof">Bevis</loc:string>
    <!--loc:string key="if">if</loc:string-->
    <!--loc:string key="section">section</loc:string-->
    <!--loc:string key="node">node</loc:string-->
    <!--loc:string key="Info file">Info file</loc:string-->
    <loc:string key="plain" kind="code">tbnol</loc:string>
    <loc:string key="tbenh" kind="code">tbnoh</loc:string>
  </loc:stringgroup>

<!-- Finnish -->

  <loc:stringgroup xml:lang="fi">
    <loc:string key="Table of Contents">Sisältö</loc:string>
    <!--loc:string key="All rights reserved."></loc:string-->
    <!--loc:string key="No warranty given."></loc:string-->
    <loc:string key="Abstract">Tiivistelmä</loc:string>
    <loc:string key="Part">Osa</loc:string>
    <loc:string key="Chapter">Luku</loc:string>
    <loc:string key="Appendix">Liite</loc:string>
    <!--loc:string key="here"></loc:string-->
    <loc:string key="Figure">Kuva</loc:string>
    <loc:string key="Table">Taulukko</loc:string>
    <loc:string key="and">ja</loc:string>
    <!--loc:string key="editor"></loc:string-->
    <loc:string key="in">teoksessa</loc:string>
    <loc:string key="References">Viitteet</loc:string>
    <loc:string key="Master thesis">pro gradu</loc:string>
    <loc:string key="PhD thesis">väitöskirja</loc:string>
    <loc:string key="Index">Hakemisto</loc:string>
    <!--loc:string key="Subject:"></loc:string-->
    <!--loc:string key="typeset by"></loc:string-->
    <loc:string key="see">katso</loc:string>
    <!--loc:string key="Click for original bitmap"></loc:string-->
    <loc:string key="prev">Edellinen</loc:string>
    <loc:string key="next">Seuraava</loc:string>
    <!--loc:string key="Theorem"></loc:string-->
    <loc:string key="Proof">Todistus</loc:string>
    <!--loc:string key="if">if</loc:string-->
    <!--loc:string key="section">section</loc:string-->
    <!--loc:string key="node">node</loc:string-->
    <!--loc:string key="Info file">Info file</loc:string-->
    <loc:string key="plain" kind="code">tbfil</loc:string>
    <loc:string key="tbenh" kind="code">tbfih</loc:string>
  </loc:stringgroup>

<!-- Portuguese -->

  <loc:stringgroup xml:lang="pt">
    <loc:string key="Table of Contents">Conteùdo</loc:string>
    <!--loc:string key="All rights reserved.">All rights reserved.</loc:string-->
    <!--loc:string key="No warranty given.">No warranty given.</loc:string-->
    <loc:string key="Abstract">Resumo</loc:string>
    <loc:string key="Part">Parte</loc:string>
    <loc:string key="Chapter">Capítulo</loc:string>
    <loc:string key="Appendix">Apêndice</loc:string>
    <!--loc:string key="here">here</loc:string-->
    <loc:string key="Figure">Figura</loc:string>
    <loc:string key="Table">Tabela</loc:string>
    <loc:string key="and">e</loc:string>
    <loc:string key="editor">editor</loc:string>
    <loc:string key="in">in</loc:string>
    <loc:string key="References">Referências</loc:string>
    <loc:string key="Master thesis">Tese de Mestrado</loc:string>
    <loc:string key="PhD thesis">Tese de Doutoramento</loc:string>
    <loc:string key="Index">Índice</loc:string>
    <!--loc:string key="Subject:">Subject:</loc:string-->
    <!--loc:string key="typeset by">typeset by</loc:string-->
    <loc:string key="see">ver</loc:string>
    <!--loc:string key="Click for original bitmap">Click for original bitmap</loc:string-->
    <!--loc:string key="prev"></loc:string-->
    <!--loc:string key="next">next</loc:string-->
    <!--loc:string key="Theorem">Theorem</loc:string-->
    <loc:string key="Proof">Demonstração</loc:string>
    <!--loc:string key="if">if</loc:string-->
    <loc:string key="section">seção</loc:string>
    <!--loc:string key="node">node</loc:string-->
    <!--loc:string key="Info file">Info file</loc:string-->
    <loc:string key="plain" kind="code">tbptl</loc:string>
    <loc:string key="tbenh" kind="code">tbpth</loc:string>
  </loc:stringgroup>

<!-- Brazilian -->

<!-- Thanks to Codejun. -->

  <loc:stringgroup xml:lang="pt-br">
    <loc:string key="Table of Contents">Sumário</loc:string>
    <loc:string key="All rights reserved.">Todos os direitos reservados.</loc:string>
    <loc:string key="No warranty given.">Nenhuma garantia inclusa.</loc:string>
    <loc:string key="Abstract">Resumo</loc:string>
    <loc:string key="Part">Parte</loc:string>
    <loc:string key="Chapter">Capítulo</loc:string>
    <loc:string key="Appendix">Apêndice</loc:string>
    <loc:string key="here">aqui</loc:string>
    <loc:string key="Figure">Figura</loc:string>
    <loc:string key="Table">Tabela</loc:string>
    <loc:string key="and">e</loc:string>
    <loc:string key="editor">editor</loc:string>
    <loc:string key="in">in</loc:string>
    <loc:string key="References">Referências</loc:string>
    <loc:string key="Master thesis">Tese de Mestrado</loc:string>
    <loc:string key="PhD thesis">Tese de Doutoramento</loc:string>
    <loc:string key="Index">Índice Remissivo</loc:string>
    <loc:string key="Subject:">Assunto:</loc:string>
    <loc:string key="typeset by">Feito por</loc:string>
    <loc:string key="see">veja</loc:string>
    <loc:string key="Click for original bitmap">Clique para bitmap original</loc:string>
    <loc:string key="prev">Anterior</loc:string>
    <loc:string key="next">Proximo</loc:string>
    <loc:string key="Theorem">Teorema</loc:string>
    <loc:string key="Proof">Demonstração</loc:string>
    <!--loc:string key="if">if</loc:string-->
    <loc:string key="section">seção</loc:string>  <!-- FixMe: unconfirmed -->
    <!--loc:string key="node">node</loc:string-->
    <!--loc:string key="Info file">Info file</loc:string-->
    <loc:string key="plain" kind="code">tbptl</loc:string>
    <loc:string key="tbenh" kind="code">tbpth</loc:string>
  </loc:stringgroup>

<!-- Russian -->

<!-- Thanks to Kirill Bogdanov, Lubov Kozlova, and Ivan Tkatchev. -->

  <loc:stringgroup xml:lang="ru">
    <loc:string key="Table of Contents">Оглавление</loc:string>
    <loc:string key="All rights reserved.">Все права защищены.</loc:string>
    <loc:string key="No warranty given.">Без предоставления какой-либо
      гарантии.</loc:string>
    <loc:string key="Abstract">Аннотация</loc:string>
    <loc:string key="Part">Часть</loc:string>
    <loc:string key="Chapter">Глава</loc:string>
    <loc:string key="Appendix">Приложение</loc:string>
    <loc:string key="here">здесь</loc:string>
    <loc:string key="Figure">Рис.</loc:string>
    <loc:string key="Table">Таблица</loc:string>
    <loc:string key="and">и</loc:string>
    <loc:string key="editor">редактор</loc:string>
    <loc:string key="in">в</loc:string>
    <loc:string key="References">Литература</loc:string>
    <loc:string key="Master thesis">Дипломная работа</loc:string>
    <loc:string key="PhD thesis">Кандидатская работа</loc:string>
    <loc:string key="Index">Предметный указатель</loc:string>
    <loc:string key="Subject:">Тема:</loc:string>
    <loc:string key="typeset by">набрано</loc:string>
    <loc:string key="see">см.</loc:string>
    <loc:string key="Click for original bitmap">Нажмите для просмотра
      оригинального изображения</loc:string>
    <loc:string key="prev">пред.</loc:string>
    <loc:string key="next">след.</loc:string>
    <loc:string key="Theorem">Теорема</loc:string>
    <loc:string key="Proof">Доказательство</loc:string>
    <!--loc:string key="if">if</loc:string-->
    <loc:string key="section">раздел</loc:string>  <!-- FixMe: not confirmed -->
    <!--loc:string key="node">node</loc:string-->
    <!--loc:string key="Info file">Info file</loc:string-->
    <loc:string key="plain" kind="code">tbenl</loc:string>
    <loc:string key="tbenh" kind="code">tbenh</loc:string>
  </loc:stringgroup>
</loc:strings>
