<?xml version="1.0"?>
<!--
    inline.mod.xsl - inline elements like "@emph".
	$Id: inline.mod.xsl,v 1.1 2008/04/07 09:25:57 alatina Exp $	

    Copyright © 2004, 2005 Torsten Bronger <bronger@physik.rwth-aachen.de>.

    This file is part of texi2latex.

    texi2latex is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License as published by the Free
    Software Foundation; either version 2 of the License, or (at your option)
    any later version.

    texi2latex is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with texi2latex; if not, write to the Free Software Foundation, Inc., 59
    Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">


<xsl:template match="code | kbd | key | samp | verb | var | env | command |
                     option | dfn | cite | abbrevword | abbrevdesc |
                     acronymword | acronymdesc | emph | strong | sc | slanted |
                     sansserif | i | b | r | dmn">
  <xsl:value-of select="concat('{\',local-name(),'Hook{')"/>
  <xsl:apply-templates/>
  <xsl:text>}}</xsl:text>
</xsl:template>

<xsl:template match="tt">
  <xsl:text>{\tHook{</xsl:text>
  <xsl:apply-templates/>
  <xsl:text>}}</xsl:text>
</xsl:template>

<xsl:template match="file">
  <xsl:text>\fileHook{</xsl:text>
  <xsl:if test="ancestor-or-self::title">
    <!-- In headings that may wander in the TOC -->
    <xsl:text>\protect</xsl:text>
  </xsl:if>
  <xsl:text>\hyphenrules{nohyphenation}</xsl:text>
  <xsl:apply-templates/>
  <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="file/text()">
  <xsl:value-of select="$start-delimiter"/>
  <xsl:call-template name="insert-breakpoints">
    <!-- "/\" for POSIX and Windows. -->
    <xsl:with-param name="break-after" select="'/\'"/>
    <xsl:with-param name="string" select="."/>
  </xsl:call-template>
  <xsl:value-of select="$end-delimiter"/>
</xsl:template>

<!-- For acronyms, every occurence of acronymdesc is processed.  Maybe it would
     be better to limit that to the first one (one one specific acronym). -->

<xsl:template match="acronym">
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="math">
  <xsl:text>$</xsl:text>
  <xsl:value-of select="."/>
  <xsl:text>$</xsl:text>
</xsl:template>

<xsl:template match="footnote">
  <xsl:text>\footnote{</xsl:text>
  <xsl:apply-templates select="para/node()"/>  <!-- FixMe: Multiple <para>s are not dealt with -->
  <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="frenchspacing">
  <xsl:choose>
    <xsl:when test="not(@val) or @val != 'on'">
      <xsl:text>\nonfrenchspacing </xsl:text>
    </xsl:when>
    <xsl:otherwise>
      <xsl:text>\frenchspacing </xsl:text>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!-- Logos -->

<xsl:template match="logo">
  <xsl:variable name="name" select="normalize-space(.)"/>
  <xsl:choose>
    <xsl:when test="$name = 'LaTeX'">
      <xsl:text>\LaTeX{}</xsl:text>
    </xsl:when>
    <xsl:when test="$name = 'TeX'">
      <xsl:text>\TeX{}</xsl:text>
    </xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="$name"/>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

</xsl:stylesheet>
