<?xml version="1.0"?>
<!--
    lists.mod.xsl - itemizations enumerations, and two-column tables.
	$Id: lists.mod.xsl,v 1.1 2008/04/07 09:25:57 alatina Exp $	

    Copyright © 2004, 2005 Torsten Bronger <bronger@physik.rwth-aachen.de>.

    This file is part of texi2latex.

    texi2latex is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License as published by the Free
    Software Foundation; either version 2 of the License, or (at your option)
    any later version.

    texi2latex is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with texi2latex; if not, write to the Free Software Foundation, Inc., 59
    Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- Itemizations and enumerations -->

<xsl:template match="itemize">
  <xsl:text>\begin{itemize}</xsl:text>
  <xsl:apply-templates select="item"/>
  <xsl:text>\end{itemize}</xsl:text>
</xsl:template>

<xsl:template match="enumerate">
  <xsl:variable name="first-normalised" select="normalize-space(@first)"/>
  <xsl:variable name="start-value">
    <xsl:choose>
      <xsl:when test="contains($lowercase,$first-normalised)">
        <xsl:value-of select="string-length(substring-before($lowercase,
                              $first-normalised))"/>
      </xsl:when>
      <xsl:when test="contains($uppercase,$first-normalised)">
        <xsl:value-of select="string-length(substring-before($uppercase,
                              $first-normalised))"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="floor(number($first-normalised)) - 1"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <xsl:variable name="i-name" select="substring('iiiiiiiiii',1,
                                      count(ancestor-or-self::enumerate))"/>
  <xsl:text>\begin{enumerate}%&#10;</xsl:text>
  <xsl:if test="$start-value != 0">
    <xsl:value-of select="concat('\setcounter{enum',$i-name,'}{',$start-value,'}%&#10;')"/>
  </xsl:if>
  <xsl:text>\renewcommand{\theenum</xsl:text>
  <xsl:value-of select="$i-name"/>
  <xsl:text>}{</xsl:text>
  <xsl:choose>
    <xsl:when test="contains($lowercase,$first-normalised)">
      <xsl:text>\alph{</xsl:text>
    </xsl:when>
    <xsl:when test="contains($uppercase,$first-normalised)">
      <xsl:text>\Alph{</xsl:text>
    </xsl:when>
    <xsl:otherwise>
      <xsl:text>\arabic{</xsl:text>
    </xsl:otherwise>
  </xsl:choose>
  <xsl:text>enum</xsl:text>
  <xsl:value-of select="$i-name"/>
  <xsl:text>}}%&#10;</xsl:text>
  <xsl:apply-templates select="item"/>
  <xsl:text>\end{enumerate}</xsl:text>
</xsl:template>

<!-- This is a shortcut template to make things simpler in the LaTeX source if
     there is no need for complication.  Besides, this is a very frequent case.
     -->

<xsl:template match="enumerate[not(ancestor::enumerate) and
                     (not(@first) or number(@first) = 1)]">
  <xsl:text>\begin{enumerate}%&#10;</xsl:text>
  <xsl:apply-templates select="item"/>
  <xsl:text>\end{enumerate}</xsl:text>
</xsl:template>


<xsl:template match="enumerate/item | itemize/item">
  <xsl:text>\item </xsl:text>
  <xsl:if test="parent::itemize">
    <xsl:apply-templates select="preceding-sibling::itemfunction"/>
  </xsl:if>
  <xsl:apply-templates/>
</xsl:template>

<!-- Here the bullets are created.  In the case of real bullets this is rather
     redundant, but I am to lazy to change that.  Besides, I don't think that
     it is necessary. -->

<xsl:template match="itemfunction">
  <xsl:text>[{</xsl:text>
  <xsl:apply-templates/>
  <xsl:text>}]</xsl:text>
</xsl:template>

<!-- Two-column tables, which are called "descriptions" in LaTeX.  For real
     tables, see the multicolumn tables in tables.mod.xsl.  The medskips seem
     to bee too less flexible. -->

<xsl:template match="table">
  <xsl:text>\begingroup\parindent0pt\vskip\medskipamount </xsl:text>
  <xsl:apply-templates/>
  <xsl:text>\medskip\endgroup </xsl:text>
</xsl:template>

<xsl:template match="tableterm">
  <xsl:variable name="contents">
    <xsl:text>{\tabletermHook{</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>}}</xsl:text>
  </xsl:variable>
  <xsl:text>\setbox\ItemBox=\hbox{</xsl:text>
  <xsl:value-of select="$contents"/>
  <xsl:text>\quad}%&#10;</xsl:text>
  <xsl:if test="not(following-sibling::tableterm)">
    <xsl:text>\ifdim\wd\ItemBox &gt; \tableindent&#10;</xsl:text>
  </xsl:if>
  <xsl:text>\vbox{\unhbox\ItemBox}</xsl:text>
  <xsl:if test="not(following-sibling::tableterm)">
    <xsl:text>\fi</xsl:text>
  </xsl:if>
  <xsl:text>&#10;</xsl:text>
  <xsl:text>\Nopagebreak </xsl:text>
</xsl:template>

<xsl:template match="tableitem/item">
  <xsl:text>&#10;\begin{definitionitem}[\tableindent]\vskip-\parsep
\leavevmode\hbox to 0pt{\hss\hbox to \tableindent{\box\ItemBox\hfill}}%&#10;</xsl:text>
  <xsl:apply-templates/>
  <xsl:text>\end{definitionitem}&#10;</xsl:text>
</xsl:template>

<!-- paras in items must carry their \par *afterwards*.  (This may be
     reasonable for all cases, but I don't care.) -->

<xsl:template match="item/para">
  <xsl:apply-templates/>
  <xsl:text>\par </xsl:text>
</xsl:template>


</xsl:stylesheet>
