Defining a lattice is usually the first step of any simulation.
With Placet three different types of beamlines can be simulated. Firstly,
a normal main linac where the structure length is very small compared to the
beta-function (the main linac of CLIC). Secondly, an accelerator where the 
structure length is comparable to the beta-function (the drive beam accelerator
of CLIC). And at last a decelerator with a high group velocity inside the
structures (the drive beam decelerator of CLIC). In the first
case one can assume that the beam position does not change significantly
inside the structure while in the second case each structure consists of cells
with a number of transverse dipole and quadrupole modes. It is assumed that
these modes do not couple to one another and that the wakefield kick is located
in the cell. In the last case a single longitudinal and a single transverse
mode are assumed but the group velocity is taken into account for both.
Currently the three models cannot be mixed but this is planned as a future
extension.

\input{dec}
\subsection{Setting up the Lattice}
Two possibilities exist to create the lattice, one can either read a complete
description file for the main beamline or create and place the elements
individually. The first is done using the command {\tt SetupMainBeamline}. In
the second case one creates the elements one after another starting from the
injection point. For each module the girder has to be created using the command
{\tt Girder}, then all the elements are placed using {\tt Bpm},
{\tt Quadrupole}, {\tt Drift} and {\tt Cavity} commands. The length of the
girder is automatically adjusted to match the total length of all elements it
supports.

\begin{clist}
\command Girder \\
Creates a new girder to place the elements created in the following.

\command Quadrupole \\
Creates a quadrupole and places it on the current girder.
\opf Length length of the quadrupole in $\u{m}$.
\opf Strength integrated strength of the quadrupole in $\u{T/m}$.

\command Drift \\
Creates a drift and places it on the current girder.
\opf Length length of the drift in $\u{m}$.

\command Bpm \\
Creates a BPM and places it on the current girder.
\opf Length length of the BPM in $\u{m}$.

\nix{
\command Cavity \\
Creates an accelerating  cavity and places it on the current girder.
\opfr Length length of the cavity in $\u{m}$.

\command Solcav \\
Creates a solenoid with acceleration and places it one the current girder.
\opfr length The length of the solenoid in $\u{m}$.
\opfr bz The strength of the solenoid in $\u{T}$.
\opfr v1 The strength of the solenoid in $\u{T}$.
}

\command DecCavity \\
Creates a decelerating cavity and places it on the current girder.
\opfr Length length of the cavity in $\u{m}$.
\opf v1 Determines the azimuthal rotation angle of the structure in radian.
Is only needed if the field non-uniformity is simulated. {\it the name will
change to phi}

\command Dipole \\
Creates a correction dipole and places it one the current girder.
\opfr length The length of the dipole in $\u{m}$.

\command Solenoid \\
Creates a solenoid and places it one the current girder.
\opfr length The length of the solenoid in $\u{m}$.
\opfr bz The strength of the solenoid in $\u{T}$.

\command QuadBpm \\
Creates a quadrupole with a BPM in its centre and places it one the current
girder. \opfr length The length of the solenoid in $\u{m}$.
\opfr strength The strength of the solenoid in $\u{T/m}$.

\command BeamlineSet \\
Fixes the beamline. This command is used to do some initial calculations, it
must be called once but only once in run.

\end{clist}

\subsection{Matching}
In order to create beams the initial twiss parameter of a lattice have to be
known. Two comands exist to match to a periodic lattice, {\tt MatchFodo} and
{\tt MatchTriplet}. With two other commands ({\tt TwissPlot} and
{\tt TwissPlotStep}) the twiss parameters can be plotted along the beamline.

\subsubsection{Matching Commands}
\begin{clist}
\command MatchFodo \\
This command calculates the matched twiss parameters in the centre of a
quadrupole of a FODO cells. It returns a list in the form
{\{ alpha\_x {\it value}
beta\_x {\it value}
mu\_x {\it value}
alpha\_y {\it value}
beta\_y {\it value}
mu\_y {\it value}\}}. Here, {\tt beta\_x} and {\tt beta\_y} are the
horizontal and vertical beta-functions in metre, {\tt alpha\_x} and
{\tt alpha\_y} are the $\alpha$-parameters and {\tt mu\_x} and {\tt mu\_y}
are the phase advances in radian.
\opfr K1 Integrated normalised strength of the first quadrupole in
\u{m^{-1}}.
\opfr K2 Integrated normalised strength of the second quadrupole in
\u{m^{-1}}.
\opfr l1 Length of the first quadrupole in $\u{[m]}$.
\opfr l2 Length of the second quadrupole in $\u{[m]}$.
\opfr L Distance between quadrupole centres in $\u{[m]}$.

\command MatchTriplet \\
This command calculates the matched twiss parameters in the centre of a
triplet in a periodic triplet lattice. It returns data in the same way as
{\tt MatchFodo}.
\opfr K1 Integrated normalised strength of the inner quadrupole in
\u{m^{-1}}.
\opfr K2 Integrated normalised strength of the outer quadrupoles in
\u{m^{-1}}.
\opfr l1 Length of the inner quadrupole in $\u{[m]}$.
\opfr l2 Length of the outer quadrupoles in $\u{[m]}$.
\opfr L Distance between triplet centres in $\u{[m]}$.
\opfr l\_sep Distance between the inner and the outer quadrupole in \u{m}.
This value is the drift between the two not the distance between the centres.
\end{clist}

\subsection{Plotting of Twiss Parameters}
The twiss parameters can be plotted with two different functions,
{\tt TwissPlot} and {\tt TwissPlotStep}. Both write a similar file, but the
first calculates the parameters only in the quadrupole centres, while the
second one plots them in each element in small steps.
The output file contains in each line the following data:
\begin{displaymath}
j(s), s, E(s), \beta_x(s), \alpha_x(s), \beta_y(s), \alpha_y(s),
\end{displaymath}
Here, $s$ is the longitudinal position at which all the other values are
determined. $j(s)$ is the element number and $E(s)$,
$\beta_x(s)$, $\alpha_x(s)$, $\beta_y(s)$ and $\alpha_y(s)$ are the energy and
the twiss parameters of the reference particle. $E$ is given in [GeV]
and $\beta$ in [m].
\begin{clist}
\command TwissPlot \\
Plots the Twiss parameters along the beamline and stores the results in a
file or returns them as a string. The parameters are evaluated in the centre
of each quadrupole.
\ops beam
Name of the beam to be used for the calculation.
\ops file
Name of the file where the results should be stored. If no file is specified
the result is returned as a list and can be processed in the program.
Examples
\begin{verbatim}
TwissPlot -beam beam0 -file twiss.dat
set twiss [TwissPlot -beam beam0]
\end{verbatim}

\command TwissPlotStep \\
This commands corresponds to {\tt TwissPlot} except that the Twiss parameters
are plotted in each element at the entry and exit. In between they are plotted
at intervalls not larger than specified by {\tt -step}.
\ops beam
Name of the beam to be used for the calculation.
\ops file
Name of the file where the results should be stored. If no file is specified
the result is returned as a list and can be processed in the program.
\opf step
The maximum step in [m] before a new value of the Twiss parameters is plotted.
In each element the value immediately after the entry and at the exit are
plotted. The positions in between are will be equidistant. The defualt is
$0.02\u{m}$.
\end{clist}

\subsubsection{Example}
The following example matches to a FODO-lattice with a quadrupole spacing of
2\u{m}, quadrupole lengths of $30\u{mm}$ and integrated quadrupole strengths
of $K=0.6\u{m^{-1}}$, the result is stored in the array {\tt match}.
\begin{verbatim}
array set match [MatchFodo -l1 0.3 -l2 0.3 -K1 0.6 -K2 -0.6 -L 2.0]
\end{verbatim}
The values can now be accessed via {\tt \$match(beta\_x)} as for example
\begin{verbatim}
puts "beta_x = $match(beta_x) m"
\end{verbatim}

\subsection{Example of a Lattice}
Here the decelerator lattice of CLIC should serve as an example. It consists
of simple FODO-cells with a length of $2.23\u{m}$ each. Before this part of the
program is executed, {\tt s} has been set to the normalised quadrupole strength
and {\tt e} to the initial beam energy. {\tt de} is the maximum decelration per
FODO-cell.
\begin{verbatim}
#
# Definition of the geometry
#
set girderlength 2.23
set cavitylength 0.8
set quadrupolelength 0.14
set bpmlength 0.03
set driftlength [expr 0.25*($girderlength-2.0*$cavitylength \
        -2.0*$quadrupolelength-2.0*$bpmlength)]
#
# Implementation of the actual lattice
# starts and ends in the centre of a quadrupole
#
set first 1

for {set i 0} { $i <250} {incr i } {

    Girder
    if {$first == 0} {
        Drift -length $driftlength
        Bpm -length $bpmlength
        Quadrupole -length $quadrupolelength \
                   -strength [expr $s * $e]
    } {
        Quadrupole -length [expr 0.5*$quadrupolelength] \
                   -strength [expr 0.5 * $s * $e]
        set first 0
    }
    Drift -length $driftlength
    DecCavity -length $cavitylength -v1 $v1
    Drift -length $driftlength
    Bpm -length $bpmlength
    Quadrupole -length $quadrupolelength \
               -strength [expr -$s * ($e-0.5*$de)]
    Drift -length $driftlength
    DecCavity -length $cavitylength -v1 $v2
    set e [expr $e - $de]
}

Drift -length $driftlength
Bpm -length $bpmlength
Quadrupole -length [expr 0.5*$quadrupolelength] \
        -strength [expr 0.5 * $s * $e]

#
# fix the lattice
#

BeamlineSet
\end{verbatim}

\subsection{Extracting Information from the Lattice}

\begin{figure}
%\epsfxsize=16cm
\rotate{\epsfbox{beamline.eps}}
\caption{The window created by the command {\tt BeamlineView}.}
\label{f:beamline}
\end{figure}

\begin{clist}
\command PlotFillFactor \\
Plots the variation of the integrated fill factor over the linac length.
This is usefull to estimate the highest integrated fill factor for a two beam
accelerator.
\ops file name of the file where to store the result, if not set the results
will be printed to stdout.

\command BeamlineInfo \\
returns a list containing information about the beamline. This can be stored
in a Tcl array using {\tt array set name [BeamlineInfo]}. The following
indices are available:
\begin{itemize}
\setlength{\labelsep}{2mm}
\item {\tt length:} the total length of the beamline in $\u{[m]}$
\item {\tt n\_quadrupole:} the number of quadrupoles in the beamline
\item {\tt n\_bpm:} the number of BPMs in the beamline
\item {\tt n\_cavity:} the number of cavities in the beamline
\item {\tt n\_drift:} the number of drifts in the beamline
\item {\tt n\_drift:} the total number of elements in the beamline
\end{itemize}

\command BeamlineView \\
This command allows to investigate the current status of the beamline
It opens a window in which the beamline elements are plotted, see
Fig.~\ref{f:beamline}. Each type can
individually be switched on and off and the transverse resolutions can
be adjusted to the requirements. This command can be used as a callback
function in any element along the line. If one clicks on an element the
vertical position is displayed. The resolution for this depends on the
resolution of the window.

The scaling of the horizontal and vertical
axis can be changed by clicking into the fields corresponding to
{\tt sscale} and {\tt yscale}, respectively. After changing the values one has
to type return for the change to become active. Also the centre of the
vertical can be varied by ajusting {\tt y0}. The slider can be used to
change the region of the beam line viewed.
With the checkboxes the different lement types can be switched on or off.

\command BeamShow \\
This command shows the beam in a window. It is used by {\tt BeamViewFilm}
and {\tt BeamView} but can also be incorporated into new graphical
routines.

\command BeamViewFilm \\
This command shows the current status of a bunch train. It can be used as a
callback function in any element and displays the positions of all bunches
as they pass this element. Normally this function is attached to a number of
elements (e.g. all quadrupoles) thus showing the time evolution of the
train.

\command WakeSet %\\
This command produces a wakefield set consisting of the wavelengths, the
loss factors and the damping of each mode. It creates a new command with the
name of the set to evaluate the wakefields at any given point.
Example:
\begin{quote}
WakeSet wake {1.0 0.5 100 1.1 0.5 100}
\end{quote}
creates a wakefield with two modes having wavelengths of $1.0$ and $1.1\u{m}$
a loss factor of $0.5\u{V/(pCm^2)}$ and $Q=100$ each. With
\begin{quote}
puts [wake 0.7]
\end{quote}
prints the wakefield $0.7\u{m}$ behind the driving particle.

\command TestSpectrum %\\
calculates the dependence of the sensitivity of the emittance growth onto the
wavelength of the prealignment error. The result is the amplitude the
error may have to create only $6\u{\%}$ emittance growth.
\ops file
name of the file where to store the results.
\opf lambda0
minimal wavelength to be used.
\opf lambda1
maximal wavelength to be used.
\opi steps
number of steps to be used. The intervall between the two wavelength is divided
logarithmically.

\command TestSpectrumSimpleCorrection %\\
calculates the dependence of the sensitivity of the emittance growth onto the
wavelength of the prealignment error. The result is the amplitude the
error may have to create only $6\u{\%}$ emittance growth.
\ops file
name of the file where to store the results.
\opf lambda0
minimal wavelength to be used.
\opf lambda1
maximal wavelength to be used.
\opi steps
number of steps to be used. The intervall between the two wavelength is divided
logarithmically.

\end{clist}

\subsection{Energy Related Commands}
In order to minimise the effect of transverse wakefields the so-called
BNS-damping can be used. This can be done by introducing and energy spread in
the bunches. The command {\tt BnsPlot} allows to plot the spread required to
fulfil the damping condition. The command {\tt EnergySpreadPlot} allows to
plot the actual energy spread of the beam along the beamline.
The other three commends are usually used for investigating the drive beam.
\begin{clist}
\command BnsPlot %\\
Plots the energy spread required for BNS-damping along the beamline.
\ops beam
name of the beam to be used for the caluclation
\ops file
name of the file where to store the results.

\command EnergySpreadPlot %\\
Plots the energy spread along the beamline.
\ops beam
name of the beam to be used for the caluclation
\ops file
name of the file where to store the results.

\command BeamEnergyPlot %\\
Prints the total beam energy along the beamline into a file.
\ops beam
name of the beam to be used for the calculation
\ops file
file where to store the results

\command BeamEnergyPrint %\\
Prints the total beam energy of the current beam to stdout.

\command BeamEnergyProfilePlot %\\
Prints the maximum, minimum and mean energy of each bunch in the current beam.
\ops file
name of the file where the values should be stored

\end{clist}