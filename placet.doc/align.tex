A number of different alignment techniques is available in the program, in
the current stage usually in the form of test routines that perform an
averaging over a number of machines. While it is of course possible to
simulate single machine with these routines it is generally much faster
to do a significant number in one call since the overhead
to calculate the response coefficients needed for the simulation is
in most cases larger than time needed to simulate a machine. All routines
take an option {\tt -machines number} that specifies the number of machines
to be simulated. The other common options are {\tt -emitt\_file name} where
{\tt name} specifies the output file for the averaged emittance along the line
and {\tt -survey name} which specifies the initial misalignment. The two most
important possibilities are {\tt Clic} where all elements are scattered around
a straight line and {\tt AtlZero} where they are moved according to the
Atl-law. An option that is common to all commands but with a slightly different
meaning is {\tt -beam name} which specifies the name of the beam to be used.
This beam must beforehand be declared. It is used for the correction
(if any) and in some cases additional beams (for example {\tt -testbeam name})
can be specified for specific purposes.

\subsection{Misalignment Schemes}
Several prealignment schemes exist as commands provided by the code. These
commands can either be called directly or used as arguments of the
beam-based alignment routines. The most common cases should be covered. If
different schemes are necessary, they can be implemented by defining
a new tcl-procedure (say {\tt my\_survey}) and use its name as an argument to
the alignment routine which then will call this procedure.
\subsection{Available Routines}
The following routines are available for misaligning the beamline. They can be
either specified in an \op{survey}\ option  or directly used as commands in
which case they don not take arguments.
\begin{clist}
\command {\tt None} \\
Does no alignment at all. This is usefull if one wants to study the
effects of initial conditions defined by hand.
\command {\tt Zero} \\
Perfectly aligns the beamline. This is usefull if one wants to study the
effects of initial offsets for example.
\command {\tt Clic} \\
All elements are scattered around the beamline axis according to the
values specified with {\tt SurveyErrorSet}.
\command {\tt Nlc} \\
The same as {\tt Clic} except that the BPMs in front of each quadrupole
are misaligned with respect to the quadrupole.
\command {\tt Atl} \\
The girders are moved according to the ATL-law, with a value
$A=0.5\cdot 10^{-6}\u{(\mu m)^2/(sm)}$ and a time {\tt survey(time)}.
\command {\tt AtlZero} \\
The same as above except that the misalignment starts from a perfectly aligned
beamline.
\command {\tt Atl2} \\
The elements are moved according to the ATL-law (pretending they are not
mounted onto girders), with a value
$A=0.5\cdot 10^{-6}\u{(\mu m)^2/(sm)}$ and a time {\tt survey(time)}.
\command {\tt AtlZero2} \\
The same as above except that the misalignment starts from a perfectly aligned
beamline.
\command {\tt Earth} \\
The elements will follow the curvature of the earth, which is assumed to have
a radius of $6500\u{km}$.
\end{clist}
A user defined misalignement routine can be implemented in {\tt tcl}.
It can use any combination of the above commands and also
access individual elements via the commands
{\tt ElementSetToOffset} and {\tt ElementAddOffset}, as discribed below.

These misalignment schemes make use of the values defined by
\begin{clist}
\command SurveyErrorSet %\\
\opf cavity\_x width of the horizontal alignment error of the structures in
micro-metres. A Gaussian distribution is assumed.
\opf cavity\_xp width of the horizontal angular distribution of the structures
in micro-radian. A Gaussian distribution is assumed.
\opf cavity\_y width of the vertical alignment error of the structures in
micro-metres. A Gaussian distribution is assumed.
\opf cavity\_yp width of the vertical angular distribution of the structures in
micro-radian. A Gaussian distribution is assumed.
\opf quadrupole\_x width of the horizontal alignment error of the quadrupoles
in micro-metres. A Gaussian distribution is assumed.
\opf quadrupole\_xp width of the horizontal angular distribution of the
quadrupoles in micro-radian. A Gaussian distribution is assumed.
\opf quadrupole\_y width of the vertical alignment error of the quadrupoles in
micro-metres. A Gaussian distribution is assumed.
\opf quadrupole\_yp width of the vertical angular distribution of the
quadrupoles in micro-radian. A Gaussian distribution is assumed.
\opf bpm\_x width of the horizontal alignment error of the BPMs
in micro-metres. A Gaussian distribution is assumed.
\opf bpm\_xp width of the horizontal angular distribution of the
BPMs in micro-radian. A Gaussian distribution is assumed.
\opf bpm\_y width of the vertical alignment error of the BPMs in
micro-metres. A Gaussian distribution is assumed.
\opf bpm\_yp width of the vertical angular distribution of the
BPMs in micro-radian. A Gaussian distribution is assumed.
\end{clist}

In addition it is possible to include the earth magnetic field in the
simulation. Currently this field is represented by thin dipoles betweeen the
lements.
\begin{clist}
\command EarthField $b_z$ $b_x$ \\
This command includes the magnetic field in the simulation. The field must be
given in $[T]$.
\end{clist}

\subsection{Accessing Individual Elements}
It is also possible to change the position of a single element via the
following commands
\begin{clist}
\command ElementSetToOffset {\it element\_number} %\\
Sets the element number {\it element\_number} to the specified offsets.
If a value is not specified the previous value is kept.
{\tt ElementSetToOffset 0 -y 10.0} changes therefore the vertical position of
element number 0 to $10\u{\mu m}$ and leaves the other parameters unchanged.
\opf x The horizontal position in \u{[\mu m]}.
\opf y The vertical position in \u{[\mu m]}.
\opf x The horizontal tilt angle in \u{[\mu radian]}.
\opf y The vertical tilt angle in \u{[\mu radian]}.
\command ElementAddOffset {\it element\_number} %\\
Adds a value to the offset of an element.
If a value is not specified the previous value is kept.
{\tt ElementAddOffset 0 -y 10.0} increases therefore the vertical position of
element number 0 by $10\u{\mu m}$ and leaves the other parameters unchanged.
\opf x The horizontal position in \u{[\mu m]}.
\opf y The vertical position in \u{[\mu m]}.
\opf x The horizontal tilt angle in \u{[\mu radian]}.
\opf y The vertical tilt angle in \u{[\mu radian]}.
\end{clist}

\subsection{Emittance Growth without Correction}
This routine will be rarly of use for the normal misaligned lattice but
is interesting for a number of other cases. It allows to evaluate the
emittance growth for a beam with an initial offset---using {\tt -survey Zero}
and offsetting the beam to be tracked. It also allows to evaluate the effect
of ground motion ({\tt -survey Atl}) and it is of course handy to show
the inportance of correction.

\begin{clist}
\command TestNoCorrection %\\
Simulates several machines without any correction.
\opsr beam name of the beam to use for the tracking.
\opi machines number of machines to simulate, defaults to 1.
\ops emitt\_file name of the file where to store the emittance averaged over
all machines. Defaults to NULL (empty name) which means no output file should
be produced.
\ops survey The prealignment routine to be used.
\end{clist}

\subsection{Feedbacks}

\begin{clist}
\command TestFeedback %\\
Test the emittance growth with feedbacks.
\opsr beam
name of the beam to use for the correction.
\ops testbeam
name of the beam to use for evaluating the effect of the
correction, defaults to the beam used for the correction.
\opi machines
number of machines to simulate, defaults to 1.
\opsr feedbacklist
list of the the feedbacks to be used for the simulation
\ops emitt\_file
name of the file where to store the emittance averaged over
all machines. Defaults to NULL (empty name) which means no output file should
be produced.
\ops survey
prealignment method to be used before the feedbacks are recalibrated
\end{clist}

\subsection{One-To-One Correction}
The one-to-one correction algorithm can be tested on the current lattice
using the command {\tt TestSimpleCorrection}. In this method each
(or each second) quadrupole is moved to centre the beam in a downstream
BPM.

\subsection{Few-To-Few Correction}
The few-to-few correction is implemented in the same fashion as the one-to-one
scheme, only a number of quadrupoles are corrected simultaneously using the
information of at least as many BPMs. The command is also
{\tt TestSimpleCorrection} with the option {\tt -binlength} which takes the
number of quadrupoles per bin added. All BPMs inside the bin up to the last one
in front of the first quadrupole that is not inside the bin are used for
correction. It is also possible to specify the option {\tt -binoverlap number}
which will start the next bin not with the first quadrupole after the current
one but rather using the last {\tt number} quadrupoles again. Of course
the specified number has to be smaller than the number of quadrupoles per bin.

\begin{clist}
\command TestSimpleCorrection \\
Tests simple few-to-few alignment. The quadrupoles are moved in order to
steer the beam through the centres of the BPMs.
\opsr beam name of the beam to use for the correction.
\ops testbeam name of the beam to use for evaluating the effect of the
correction, defaults to the beam used for the correction.
\opi machines number of machines to simulate, defaults to 1.
\opi binlength number of quadrupoles per bin, defaults to 1.
\opi interleave If this value is zero all quadrupoles are used for the
correction. If it is larger than zero only the focusing quadrupoles
in each plane are used which normally leads to better results.
If the value is smaller than one, only the
defocusing quadrupoles are used (this is a very bad idea). For interleaved
correction only one quadrupole per bin is allowed. Value defaults to 0.
\opi binoverlap number of quadrupoles in the overlap of two consecutive bins,
defaults to 0. This value has to be smaller than \op binlength
\opf jitter\_y vertical jitter during correction in $\u{[\mu m]}$. The jitter is
assumed to be Gaussian. Defaults to 0.
\opf jitter\_x horizontal jitter during correction in $\u{[\mu m]}$. The
jitter is assumed to be Gaussian. Defaults to 0.
\opf bpm\_resolution resolution of the BPMs in $\u{[\mu m]}$, defaults to 0.
\ops emitt\_file
name of the file where to store the emittance averaged over
all machines. Defaults to NULL (empty name) which means no output file should
be produced.

\command TestSimpleCorrectionDipole \\
This command simulates few-to-few correction. In contrast to
{\tt TestSimpleCorrection} dipoles are used to steer the beam, rather
movements of the quadrupoles.
\opsr beam name of the beam to use for the correction.
\ops testbeam name of the beam to use for evaluating the effect of the
correction, defaults to the beam used for the correction.
\opi machines number of machines to simulate, defaults to 1.
\opi binlength number of quadrupoles per bin, defaults to 1.
\opi interleave If this value is zero all quadrupoles are used for the
correction. If it is larger than zero only the focusing quadrupoles
in each plane are used which normally leads to better results.
If the value is smaller than one, only the
defocusing quadrupoles are used (this is a very bad idea). For interleaved
correction only one quadrupole per bin is allowed. Value defaults to 0.
\opi binoverlap number of quadrupoles in the overlap of two consecutive bins,
defaults to 0. This value has to be smaller than \op binlength
\opf jitter\_y vertical jitter during correction in $\u{[\mu m]}$. The jitter is
assumed to be Gaussian. Defaults to 0.
\opf jitter\_x horizontal jitter during correction in $\u{[\mu m]}$. The
jitter is assumed to be Gaussian. Defaults to 0.
\opf bpm\_resolution resolution of the BPMs in $\u{[\mu m]}$, defaults to 0.
\ops emitt\_file
name of the file where to store the emittance averaged over
all machines. Defaults to NULL (empty name) which means no output file should
be produced.
\end{clist}

\subsection{Ballistic Correction}
A ballistic correction can be performed using the command
{\tt TestBallisticCorrection} which accepts a large number of options.

\begin{clist}
\command TestBallisticCorrection \\
allows to test the ballistic correction.
\opsr beam Name of the beam to use for the correction.
\ops bumpbeam Name of the beam to be used for tuning the emittance bumps.
Between emittance bumps a one-to-one correction is performed. This beam can
therefore also be used without any bump to simply apply this correction.
If no name is specified but emittance tuning bumps are specified the original
beam will be used.
\ops testbeam Name of the beam to use for evaluating the effect of the
correction, defaults to the beam used for the correction or the
one for the bump tunig if specified. Can be used to evaluate the effect of
additional error sources, for example a jitter in the RF-phase after
correction for.
\opi machines Number of machines to simulate, defaults to 1.
\ops survey Survey to be used for the misalignment. Defaults to {\tt Clic}.
\opi binlength Number of quadrupoles per bin, defaults to the useless value 1.
\opi binloop Number of iterations of the full ballistic correction cycle of
each bin, defaults to 1.
\opi b\_loop number of iterations of the ballistic step in each iteration,
defaults to 1.
\opi f\_loop number of iterations of the few-to-few correction step in each
iteration, defaults to 1.
\opf jitter\_y vertical jitter during correction in $\u{[\mu m]}$. The jitter
is assumed to be Gaussian. Defaults to 0.
\opf jitter\_x horizontal jitter during correction in $\u{[\mu m]}$. The
jitter is assumed to be Gaussian. Defaults to 0.
\opf bpm\_resolution resolution of the BPMs in $\u{[\mu m]}$, defaults to 0.

\opi response This value determines the knowledge of the response coefficients
used for the correction. If it is 0 perfect knowledge is assumed.
For 1 and 2 the response coefficients are calculated from the strength of the
first quadrupole in each bin and the distance to the other elements. The
wakefield effects are not included. For very long sectors it is therefore
necessary to use {\tt -gain {\it value}} with {\it value$<1$} to achieve
convergence.

\opi field\_error\_type Sets the errors of the magnetic fields in the quadrupoles
during correction. If 0 no errors, for 1 relative errors and for 2 absolute
errors are assumed for the remnant field of the quadrupoles. The error for the
full field is always relative but only take into account for
{\op field\_error\_type $\ne0$}
\opf field\_zero\_error size of the remnant field. For {\op field\_error\_type $=1$}
the ration to the full field for {\op field\_error\_type $=2$} the size of the
remnant field in $\u{[T/m^2]}$. Defaults to 0.
\opf field\_resolution
relative field error for switched on quadrupoles,
defaults to 0.
\opl bumplist
contains for each bump to be used the number of the element
where to start the bump.
\opl bumpquads
Can be used to specify a list with the quadrupole strengths to be used for the
one-to-one correction and the tuning of the emittance bumps after the
ballistic correction. This allows to align the BPMs with a lattice different
from the one in normal operation.
\ops emitt\_file
Name of the file where to store the emittance averaged over
all machines. Defaults to NULL (empty name) which means no output file should
be produced.
\ops size\_file
The sizes of the beam during correction are stored in the file with this name
so that one can check if the acceptance is large enough.
Defaults to NULL which means no file is produced.
\opf offset\_x
Sets the sigma of the initial horizontal position error of the beam. For each
machine this is a constant value.
\opf offset\_y
Sets the sigma of the initial vertical position error of the beam. For each
machine this is a constant value.
\opf quad\_strength Defines the relative strength of the last
quadrupole during the ballistic step compared to normal operation.
\opf bpm\_resolution The resolution of the BPMs in \u{[\mu m]}
during the correction.
\opf field\_position\_error The shift of the quadrupole centre between the case
with full field and the one where the quadrupole is switched off and only the
remnant field remains. The value is given in \u{[\mu m]}.

\opf quadrupole\_resolution The error of the final position of the quadrupole
in \u{[\mu m]}. This is the difference between the calculated shift and the
one actually applied. It follows a Gaussian distribution.

\opf quadrupole\_stepsize The step size of the quadrupole movement in
\u{[\mu m]}. This allows to simulated a finite step size of the quadrupole
movers. The quadrupoles will be only moved in multiplies of this value.

\opf gain The gain used in the shift of the first quadrupole and the BPMs
during the ballistic step. In long sectors, transverse wakefields can amplify
a kick given by the first quadrupole. This can make the correction
unstable if the gain is not chosen to be smaller than one.

\opi rf\_align Determines whether the accelerating structures should be
realigned to the beam.
\opi atl
Choses the recorrection to be applied after \op time seconds.
The following choices are possible:
\begin{itemize}
\setlength{\labelsep}{2mm}
\item 0 do not apply ground motion 
\item 1 do not correct after applying ground motion
\item 2 use the emittance bumps as a simple feedback to steer the beam on its
origninal trajectory
\item 3 apply a full one-to-one correction
\item 4 apply a full one-to-one correction and recalibrate emittance bumps
\item 5 do a full correction.
\end{itemize}
\opf time the time passed after the first correction if \op atl  is not zero.
\end{clist}

\subsection{Dispersion-Free Steering}
This command test dispersion free steering.
\begin{clist}
\command TestFreeCorrection \\
\opi machines
Number of machines to simulate, the default is 1.
\ops beam
Name of the beam to be used for correction.
\opi binlength
Number of quadrupoles per bin, defualt is 1, but this has to be changed to
get stable alignment.
\opi binoverlap
Overlap of bins in no of quadrupoles, defualt is 0. Obviously, this number has
to be smaller than the number of quadrupoles per bin.
\opf jitter\_y
Vertical beam jitter during correction in \u{[\mu m]}. Default is $0\u{\mu m}$.
\opf jitter\_x
Horizontal beam jitter during correction in \u{[\mu m]}. Default is
$0\u{\mu m}$.
\opf bpm\_resolution
BPM resolution during the correction in \u{[\mu m]}. Default value is
$0\u{\mu m}$.
\opi rf\_align
If not zero the structures are aligned after the correction. Default is 0.
\ops survey
Type of prealignment survey to be used, defaults to CLIC.
\ops emitt\_file
Filename for the results, defaults to NULL (no output).
\opf wgt0
Weight for the BPM position. Default value is 1.
\opf wgt1
Weight for the BPM resolution. Default value is 1.
\opf pwgt
Weight for the old quadrupole position. Default value is 0.
\end{clist}

\subsection{Wakefield-Free Steering}

\subsection{Qadrupole Jitter}
\begin{clist}
\command TestQuadrupoleJitter %\\
tests the effect of quadrupole jitter on the emittance growth. It starts using
a perfectly aligned beamline and caluclates the effect of each quadrupole
on the final emittance assuming a linear lattice. Based on these results it
calculates the growth of the emittance with respect to the beamline axis
for different jitter amplitudes. The time necessary for the calculation is
depending on the number of quadrupoles but not very much on the number of
machines.
\end{clist}

