\subsection{Model of the Beam}
Two main models for the beam are implemented in PLACET. First one is represented by many particles
which are Gaussian distributed in the beam ellipse.
The second one is represented as a number of bunches, each of which is cut
longitudinally into slices. Each slice consists of one or more macro particles
that have the same longitudinal position. Their energies can differ to
simulate the effect of an initial energy spread. Each particle is represented
by a position in phase space $(x_i,x_i^\prime,y_i,y_i^\prime,E_i,z_i)$, where
$z_i$ is assumed to be constant along the beam line. If the lattice is linear
the particle can also be described by a matrix
\begin{displaymath}
\Sigma_i=\left(
\begin{array}{*4c}
\langle \sigma_{x,i}\sigma_{x,i}\rangle &
\langle \sigma_{x,i}\sigma_{x^\prime,i}\rangle &
\langle \sigma_{x,i}\sigma_{y,i}\rangle &
\langle \sigma_{x,i}\sigma_{y^\prime,i}\rangle\\
\langle \sigma_{x^\prime,i}\sigma_{x,i}\rangle &
\langle \sigma_{x^\prime,i}\sigma_{x^\prime,i}\rangle &
\langle \sigma_{x^\prime,i}\sigma_{y,i}\rangle &
\langle \sigma_{x^\prime,i}\sigma_{y^\prime,i}\rangle\\
\langle \sigma_{y,i}\sigma_{x,i}\rangle &
\langle \sigma_{y,i}\sigma_{x^\prime,i}\rangle &
\langle \sigma_{y,i}\sigma_{y,i}\rangle &
\langle \sigma_{y,i}\sigma_{y^\prime,i}\rangle\\
\langle \sigma_{y^\prime,i}\sigma_{x,i}\rangle &
\langle \sigma_{y^\prime,i}\sigma_{x^\prime,i}\rangle &
\langle \sigma_{y^\prime,i}\sigma_{y,i}\rangle &
\langle \sigma_{y^\prime,i}\sigma_{y^\prime,i}\rangle
\end{array}
\right)
\end{displaymath}
The average position of the beam is given by
\begin{equation}
\langle x\rangle=\frac{\sum_{i=0}^{n}x_iw_i}{\sum_{i=0}^{n}w_i}
\end{equation}
Here, $w_i$ are the weights of the particles. $x^\prime$, $y$ and $y^\prime$
are calculated accordingly.

The beam matrix of the whole pulse is given by
\begin{equation}
\Sigma=\left(
\begin{array}{*4c}
\Sigma_{xx} &\Sigma_{xx^\prime} &\Sigma_{xy} &\Sigma_{x y^\prime} \\
\Sigma_{x^\prime x} &\Sigma_{x^\prime x^\prime} &\Sigma_{x^\prime y}
&\Sigma_{x^\prime y^\prime} \\
\Sigma_{yx} &\Sigma_{yx^\prime} &\Sigma_{yy} &\Sigma_{y y^\prime} \\
\Sigma_{y^\prime x} &\Sigma_{y^\prime x^\prime} &\Sigma_{y^\prime y}
&\Sigma_{y^\prime y^\prime}
\end{array}
\right)
\end{equation}
Here, the element $\Sigma_{n_1n_2}$ is calculated as
\begin{equation}
\Sigma_{n_1n_2}=\frac{\sum_{i=0}^{n}w_i
\left(\sigma_{n_1,i}\sigma_{n_2,i}+\left(n_{1,i}-\langle n_1\rangle\right)
\left(n_{2,i}-\langle n_2\rangle\right)\right)}%
{\sum_{i=0}^{n}w_i}
\end{equation}
The emittances of the beam are defined by
\begin{eqnarray}
\epsilon_x&=&\gamma\sqrt{\Sigma_{xx}\Sigma_{x^\prime x^\prime}
-\Sigma_{x^\prime x}\Sigma_{x x^\prime}}\\
\epsilon_y&=&\gamma\sqrt{\Sigma_{yy}\Sigma_{y^\prime y^\prime}
-\Sigma_{y^\prime y}\Sigma_{y y^\prime}}.
\end{eqnarray}

The $n$-sigma envelope is defined as
\begin{equation}
\hat r=\sqrt{\max{((|x_i|+n\sigma_{x,i})^2+(|y_i|+n\sigma_y)^2)}}
\end{equation}
It will contain all particles that initially fulfilled
\begin{eqnarray}
n^2&\ge&
\left(\frac{x-\langle x_i\rangle}{\sigma_{x,i}}\right)^2
+\left(\frac{x^\prime-\langle x^\prime_i\rangle}{\sigma_{x^\prime,i}}\right)^2\\
n^2&\ge&
\left(\frac{y-\langle y_i\rangle}{\sigma_{y,i}}\right)^2
+\left(\frac{y^\prime-\langle y^\prime_i\rangle}{\sigma_{y^\prime,i}}\right)^2\\
\end{eqnarray}

In case the lattice is non-linear---as for example in a decelerator with
a non-uniform longitudinal field---only first order tracking can be
performed. The particles have to be transversely distributed in each slice
to be able to calculated the size of the envelope and emittance.

\subsection{Models of the Elements}
The quadrupoles are treated as thick lenses with no fringe fields. The
drifts and BPMs are treated as drifts. The BPMs measure the beam position
in the longitudinal centre. Dipoles are treated as thin elements. A length can
be specified when they are placed in the beam line, the kick is applied in
the longitudinal centre.

\subsection{Wakefields Model of the Main-Linac Structure}
The short range wakefields of the main linac structures are defined
via a file that is read when a beam is created. In this file the longitudinal
single bunch beam loading is specified for each slice.

The accelerating RF-field is specified by an amplitude and a phase. Several
different phases can be defined for the accelerator, the amplitude can vary
from structure to structure\footnote{The fact that only a small number of
phases can be defined is historical and will be changed soon.}.

From single-bunch beam loading and the acceleration by the RF the gradient
$G_i$ is calculated for each slice. The transfer matrix through the
structure is then given by
\begin{equation}
\left(
\begin{array}{*4c}
1 & \frac{E_i}{G_i}\ln\frac{E_i+LG_i}{E_i} & 0 & 0 \\
0 & 1 & 0 & 0 \\
0 & 0 & 1 & \frac{E_i}{G_i}\ln\frac{E_i+LG_i}{E_i} \\
0 & 0 & 0 & 1
\end{array}
\right)
\end{equation}
The end fields of the structures are also included in the simulation. At the
structure entrance this is
\begin{equation}
M_i=\left(
\begin{array}{*2c}
1 & 0\\
-\frac{G_i}{2E_i} & 1
\end{array}
\right)
\end{equation}
And for the exit
\begin{equation}
M_i=\left(
\begin{array}{*2c}
1 & 0\\
\frac{G_i}{2E_i} & 1
\end{array}
\right)
\end{equation}
In the program the particles are tracked to the centre of the structure
using the matrices above, then the wakefield is calculated from the
offset and the corresponding kick is applied. Finally the particles are tracked
through the second part of the structure.
The wakefield is calculated according to
\begin{equation}
\delta x^\prime_j=\sum_{i=0}^{j-1}k_{i,j}x_iw_i
\end{equation}
Here, the coefficients $k_{i,j}$ are taken from a file that the user has to
provide.

\subsection{Wakefields Model of the Decelerating Structures}
Following the results of the CLIC study, the decelerating structures are
assumed to have one longitudinal and one transverse mode~\cite{c:modes}.
Each of these two modes can be described by a loss factor, a wavelength,
a group velocity and a damping. Currently the damping of the longitudinal
mode is neglected in the program.

\begin{figure}
\epsfxsize=16cm
\epsfbox{wake}
\caption{Schematic drawing of the wakefield left in the structure by a
particle during its passage.}
\label{f:step}
\end{figure}
The wakefield left by a particle in the structure is assumed to follow a step
function, see Fig.~\ref{f:step}. A witness particle following the driving one
at a distance $z$ will seen the field emitted at position $s_0$ at
\begin{equation}
s=s_0+z\frac{\beta}{1-\beta}
\end{equation}
Here, $\beta$ is the group velocity of the wakefield divided by the speed of
light. The time $\Delta t$ that has passed since the emission of the field is
\begin{equation}
\Delta t=\frac{z}{c}\frac{1}{1-\beta}
\end{equation}
The wakefield experienced by the witness particle once it caught up with it
can thus be calculated as
\begin{equation}
w(z)=w_0\exp\left(\frac{1}{1-\beta}\frac{\pi z}{\lambda Q}\right)
\end{equation}

\begin{figure}
\hbox{
\epsfxsize=6cm
\epsfbox{step2}
\epsfxsize=6cm
\epsfbox{step2a}
}
\caption{The field distribution experienced by a witness particle in the
structure (left hand side). From this the field distribution used in the
program is derived (right hand side).}
\label{f:step2}
\end{figure}
The field distribution experienced by a witness particle that follows several
other can be described by a number of step functions, see Fig.~\ref{f:step2}.
In the program the structure is cut into a number sections. The integral field
contribution of each particle that already passed the structure to each section
is calculated. The field is then assumed to be constant within each section,
see the figure.
Therefore the integral field is exact but the distribution is slightly
different than it should be. By varying the number of sections one can
verify that this has no influence on the simulation.

For the transverse field the calculation is the same, except that also the
transverse positions of the driving particles along the structure are recorded.
Angles of the the particle trajectories with respect to the structure axis
are therefore taken into account and the betatron wavelength does not need to
be much larger than the structure length.

The end fields of the structure are taken into account at the end of each
section. The kick is calculated in thin lens approximation at the structure
entry as
\begin{equation}
M_i=\left(
\begin{array}{*2c}
1 & 0\\
-\frac{G_i}{2E_i} & 1
\end{array}
\right)
\end{equation}
Here, $G_i$ is the gradient for the particle number $i$ in the structure and
$E_i$ is its energy. Equivalently at the structure exit one uses
\begin{equation}
M_i=\left(
\begin{array}{*2c}
1 & 0\\
\frac{G_i}{2E_i} & 1
\end{array}
\right)
\end{equation}

If the structure does not have rotational symmetry the longitudinal field may
depend on the transverse position of the particle within the structure. This
leads to a transverse kick. The longitudinal field can be described by a
multi-pole expansion
\begin{equation}
E_\parallel(z,r,\phi)=\sum_{i=0}^\infty 2k_i\frac{r^{m_si}}{a^{m_si}}
\cos(2\pi z/\lambda)\cos(m_si\phi).
\end{equation}
Here, $a$ is the structure radius and $m_s$ the number of symmetry planes. The
azimuthal angle $\Phi$ is zero in one of the symmetry planes, $z$ is the
position of the particle within the bunch (with respect to the crest) and
$r$ its distance to the structure axis.
The transverse field can be written as
\begin{eqnarray}
E_\perp(z,r,\phi)&=&\sum_{i=0}^\infty 2k_i m_si\frac{r^{m_si-1}}{a^{m_si}}
\frac{\lambda}{2\pi}
\sin(2\pi z/\lambda) \nonumber \\ 
& & [-\vec e_r\cos(m_si\phi)+\vec e_\phi\sin(m_si\phi)]\label{e:nonl}.
\end{eqnarray}
In the program the factors $k_i/k_0$ can be specified.
Because of the non-linearity of the fields the beam can in this case not be
represented by ellipses but by macro particles only.

