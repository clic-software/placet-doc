placet.pdf: placet.tex placet.ind install.tex align.tex lang_new.tex cavdef.tex bnsplot.tex energyspreadplot.tex beamenergyplot.tex beamenergyprint.tex beamenergyprofileplot.tex elementsettooffset.tex elementaddoffset.tex testnocorrection.tex testfeedback.tex testsimplecorrection.tex testsimplecorrectiondipole.tex testballisticcorrection.tex testfreecorrection.tex testfreecorrection2.tex testmeasuredcorrection.tex testquadrupolejitter.tex quadrupolegetstrength.tex quadrupolegetstrengthlist.tex quadrupolesetstrength.tex quadrupolesetstrengthlist.tex quadrupolenumberlist.tex lattice.tex intro.tex beam.tex model.tex dec.tex help.tex howto.tex graph.tex tutorials.tex faq.tex commands.tex octave-commands.tex
	pdflatex placet
placet.ind: placet.tex align.tex lattice.tex intro.tex lang_new.tex commands.tex
	latex placet
	makeindex placet
cavdef.texi:
	echo "Help CavityDefine" | placet -s > cavdef.texi
cavdef.tex: cavdef.texi
	cat header.texi cavdef.texi | grep -v "Default value" > tmpc1.texi
	bin/texi2latex tmpc1.texi
	awk 'BEGIN{lab=0} /InsertLabelMaybe/{lab=1} lab==1{print }' tmpc1.ltx > tmpc1.tex
	grep -v '\\end{document}' tmpc1.tex > cavdef.tex
bnsplot.texi:
	echo "Help BnsPlot" | placet -s > bnsplot.texi
bnsplot.tex: bnsplot.texi
	cat header.texi bnsplot.texi | grep -v "Default value" > tmpc11.texi
	bin/texi2latex tmpc11.texi
	awk 'BEGIN{lab=0} /InsertLabelMaybe/{lab=1} lab==1{print }' tmpc11.ltx > tmpc11.tex
	grep -v '\\end{document}' tmpc11.tex > bnsplot.tex
energyspreadplot.texi:
	echo "Help EnergySpreadPlot" | placet -s > energyspreadplot.texi
energyspreadplot.tex: energyspreadplot.texi
	cat header.texi energyspreadplot.texi | grep -v "Default value" > tmpc12.texi
	bin/texi2latex tmpc12.texi
	awk 'BEGIN{lab=0} /InsertLabelMaybe/{lab=1} lab==1{print }' tmpc12.ltx > tmpc12.tex
	grep -v '\\end{document}' tmpc12.tex > energyspreadplot.tex
beamenergyplot.texi:
	echo "Help BeamEnergyPlot" | placet -s > beamenergyplot.texi
beamenergyplot.tex: beamenergyplot.texi
	cat header.texi beamenergyplot.texi | grep -v "Default value" > tmpc13.texi
	bin/texi2latex tmpc13.texi
	awk 'BEGIN{lab=0} /InsertLabelMaybe/{lab=1} lab==1{print }' tmpc13.ltx > tmpc13.tex
	grep -v '\\end{document}' tmpc13.tex > beamenergyplot.tex
beamenergyprint.texi:
	echo "Help BeamEnergyPrint" | placet -s > beamenergyprint.texi
beamenergyprint.tex: beamenergyprint.texi
	cat header.texi beamenergyprint.texi | grep -v "Default value" > tmpc14.texi
	bin/texi2latex tmpc14.texi
	awk 'BEGIN{lab=0} /InsertLabelMaybe/{lab=1} lab==1{print }' tmpc14.ltx > tmpc14.tex
	grep -v '\\end{document}' tmpc14.tex > beamenergyprint.tex
beamenergyprofileplot.texi:
	echo "Help BeamEnergyProfilePlot" | placet -s > beamenergyprofileplot.texi
beamenergyprofileplot.tex: beamenergyprofileplot.texi
	cat header.texi beamenergyprofileplot.texi | grep -v "Default value" > tmpc15.texi
	bin/texi2latex tmpc15.texi
	awk 'BEGIN{lab=0} /InsertLabelMaybe/{lab=1} lab==1{print }' tmpc15.ltx > tmpc15.tex
	grep -v '\\end{document}' tmpc15.tex > beamenergyprofileplot.tex
elementsettooffset.texi:
	echo "Help ElementSetToOffset" | placet -s > elementsettooffset.texi
elementsettooffset.tex: elementsettooffset.texi
	cat header.texi elementsettooffset.texi | grep -v "Default value" > tmpc16.texi
	bin/texi2latex tmpc16.texi
	awk 'BEGIN{lab=0} /InsertLabelMaybe/{lab=1} lab==1{print }' tmpc16.ltx > tmpc16.tex
	grep -v '\\end{document}' tmpc16.tex > elementsettooffset.tex
elementaddoffset.texi:
	echo "Help ElementAddOffset" | placet -s > elementaddoffset.texi
elementaddoffset.tex: elementaddoffset.texi
	cat header.texi elementaddoffset.texi | grep -v "Default value" > tmpc17.texi
	bin/texi2latex tmpc17.texi
	awk 'BEGIN{lab=0} /InsertLabelMaybe/{lab=1} lab==1{print }' tmpc17.ltx > tmpc17.tex
	grep -v '\\end{document}' tmpc17.tex > elementaddoffset.tex
testnocorrection.texi:
	echo "Help TestNoCorrection" | placet -s > testnocorrection.texi
testnocorrection.tex: testnocorrection.texi
	cat header.texi testnocorrection.texi | grep -v "Default value" > tmpc18.texi
	bin/texi2latex tmpc18.texi
	awk 'BEGIN{lab=0} /InsertLabelMaybe/{lab=1} lab==1{print }' tmpc18.ltx > tmpc18.tex
	grep -v '\\end{document}' tmpc18.tex > testnocorrection.tex
testfeedback.texi:
	echo "Help TestFeedback" | placet -s > testfeedback.texi
testfeedback.tex: testfeedback.texi
	cat header.texi testfeedback.texi | grep -v "Default value" > tmpc19.texi
	bin/texi2latex tmpc19.texi
	awk 'BEGIN{lab=0} /InsertLabelMaybe/{lab=1} lab==1{print }' tmpc19.ltx > tmpc19.tex
	grep -v '\\end{document}' tmpc19.tex > testfeedback.tex
testsimplecorrection.texi:
	echo "Help TestSimpleCorrection" | placet -s > testsimplecorrection.texi
testsimplecorrection.tex: testsimplecorrection.texi
	cat header.texi testsimplecorrection.texi | grep -v "Default value" > tmpc110.texi
	bin/texi2latex tmpc110.texi
	awk 'BEGIN{lab=0} /InsertLabelMaybe/{lab=1} lab==1{print }' tmpc110.ltx > tmpc110.tex
	grep -v '\\end{document}' tmpc110.tex > testsimplecorrection.tex
testsimplecorrectiondipole.texi:
	echo "Help TestSimpleCorrectionDipole" | placet -s > testsimplecorrectiondipole.texi
testsimplecorrectiondipole.tex: testsimplecorrectiondipole.texi
	cat header.texi testsimplecorrectiondipole.texi | grep -v "Default value" > tmpc111.texi
	bin/texi2latex tmpc111.texi
	awk 'BEGIN{lab=0} /InsertLabelMaybe/{lab=1} lab==1{print }' tmpc111.ltx > tmpc111.tex
	grep -v '\\end{document}' tmpc111.tex > testsimplecorrectiondipole.tex
testballisticcorrection.texi:
	echo "Help TestBallisticCorrection" | placet -s > testballisticcorrection.texi
testballisticcorrection.tex: testballisticcorrection.texi
	cat header.texi testballisticcorrection.texi | grep -v "Default value" > tmpc112.texi
	bin/texi2latex tmpc112.texi
	awk 'BEGIN{lab=0} /InsertLabelMaybe/{lab=1} lab==1{print }' tmpc112.ltx > tmpc112.tex
	grep -v '\\end{document}' tmpc112.tex > testballisticcorrection.tex
testfreecorrection.texi:
	echo "Help TestFreeCorrection" | placet -s > testfreecorrection.texi
testfreecorrection.tex: testfreecorrection.texi
	cat header.texi testfreecorrection.texi | grep -v "Default value" > tmpc113.texi
	bin/texi2latex tmpc113.texi
	awk 'BEGIN{lab=0} /InsertLabelMaybe/{lab=1} lab==1{print }' tmpc113.ltx > tmpc113.tex
	grep -v '\\end{document}' tmpc113.tex > testfreecorrection.tex
testfreecorrection2.texi:
	echo "Help TestFreeCorrection2" | placet -s > testfreecorrection2.texi
testfreecorrection2.tex: testfreecorrection2.texi
	cat header.texi testfreecorrection2.texi | grep -v "Default value" > tmpc121.texi
	bin/texi2latex tmpc121.texi
	awk 'BEGIN{lab=0} /InsertLabelMaybe/{lab=1} lab==1{print }' tmpc121.ltx > tmpc121.tex
	grep -v '\\end{document}' tmpc121.tex > testfreecorrection2.tex
testmeasuredcorrection.texi:
	echo "Help TestMeasuredCorrection" | placet -s > testmeasuredcorrection.texi
testmeasuredcorrection.tex: testmeasuredcorrection.texi
	cat header.texi testmeasuredcorrection.texi | grep -v "Default value" > tmpc114.texi
	bin/texi2latex tmpc114.texi
	awk 'BEGIN{lab=0} /InsertLabelMaybe/{lab=1} lab==1{print }' tmpc114.ltx > tmpc114.tex
	grep -v '\\end{document}' tmpc114.tex > testmeasuredcorrection.tex
testquadrupolejitter.texi:
	echo "Help TestQuadrupoleJitter" | placet -s > testquadrupolejitter.texi
testquadrupolejitter.tex: testquadrupolejitter.texi
	cat header.texi testquadrupolejitter.texi | grep -v "Default value" > tmpc115.texi
	bin/texi2latex tmpc115.texi
	awk 'BEGIN{lab=0} /InsertLabelMaybe/{lab=1} lab==1{print }' tmpc115.ltx > tmpc115.tex
	grep -v '\\end{document}' tmpc115.tex > testquadrupolejitter.tex
quadrupolegetstrength.texi:
	echo "Help QuadrupoleGetStrength" | placet -s > quadrupolegetstrength.texi
quadrupolegetstrength.tex: quadrupolegetstrength.texi
	cat header.texi quadrupolegetstrength.texi | grep -v "Default value" > tmpc116.texi
	bin/texi2latex tmpc116.texi
	awk 'BEGIN{lab=0} /InsertLabelMaybe/{lab=1} lab==1{print }' tmpc116.ltx > tmpc116.tex
	grep -v '\\end{document}' tmpc116.tex > quadrupolegetstrength.tex
quadrupolegetstrengthlist.texi:
	echo "Help QuadrupoleGetStrengthList" | placet -s > quadrupolegetstrengthlist.texi
quadrupolegetstrengthlist.tex: quadrupolegetstrengthlist.texi
	cat header.texi quadrupolegetstrengthlist.texi | grep -v "Default value" > tmpc117.texi
	bin/texi2latex tmpc117.texi
	awk 'BEGIN{lab=0} /InsertLabelMaybe/{lab=1} lab==1{print }' tmpc117.ltx > tmpc117.tex
	grep -v '\\end{document}' tmpc117.tex > quadrupolegetstrengthlist.tex
quadrupolesetstrength.texi:
	echo "Help QuadrupoleSetStrength" | placet -s > quadrupolesetstrength.texi
quadrupolesetstrength.tex: quadrupolesetstrength.texi
	cat header.texi quadrupolesetstrength.texi | grep -v "Default value" > tmpc118.texi
	bin/texi2latex tmpc118.texi
	awk 'BEGIN{lab=0} /InsertLabelMaybe/{lab=1} lab==1{print }' tmpc118.ltx > tmpc118.tex
	grep -v '\\end{document}' tmpc118.tex > quadrupolesetstrength.tex
quadrupolesetstrengthlist.texi:
	echo "Help QuadrupoleSetStrengthList" | placet -s > quadrupolesetstrengthlist.texi
quadrupolesetstrengthlist.tex: quadrupolesetstrengthlist.texi
	cat header.texi quadrupolesetstrengthlist.texi | grep -v "Default value" > tmpc119.texi
	bin/texi2latex tmpc119.texi
	awk 'BEGIN{lab=0} /InsertLabelMaybe/{lab=1} lab==1{print }' tmpc119.ltx > tmpc119.tex
	grep -v '\\end{document}' tmpc119.tex > quadrupolesetstrengthlist.tex
quadrupolenumberlist.texi:
	echo "Help QuadrupoleNumberList" | placet -s > quadrupolenumberlist.texi
quadrupolenumberlist.tex: quadrupolenumberlist.texi
	cat header.texi quadrupolenumberlist.texi | grep -v "Default value" > tmpc120.texi
	bin/texi2latex tmpc120.texi
	awk 'BEGIN{lab=0} /InsertLabelMaybe/{lab=1} lab==1{print }' tmpc120.ltx > tmpc120.tex
	grep -v '\\end{document}' tmpc120.tex > quadrupolenumberlist.tex
commands.texi:
	echo "Manual -file commands.texi" | placet -s
commands.tex: commands.texi
	cat header.texi commands.texi | grep -v "Default value" > tmp1.texi
	bin/texi2latex tmp1.texi
	awk 'BEGIN{lab=0} /InsertLabelMaybe/{lab=1} lab==1{print }' tmp1.ltx > tmp1.tex
	sed 's/section/subsection/g' tmp1.tex > tmp2.tex
	#sed 's/texttt/texttindex/g' tmp2.tex > tmp3.tex
	grep -v '\\end{document}' tmp2.tex > commands.tex
placet.doc.tar.gz : placet.doc.tar
	gzip -9 placet.doc.tar
placet.doc.tar : *.tex *.eps
	tar -cf placet.doc.tar *.tex *.eps
clean:
	rm -f placet.ind placet.ilg placet.aux placet.out placet.idx placet.toc placet.brf placet.log placet.dvi tmp1.texi tmpc* tmp1.ltx tmp1.tex tmp2.tex tmp3.tex commands.texi commands.tex cavdef.texi cavdef.tex bnsplot.texi bnsplot.tex energyspreadplot.texi energyspreadplot.tex beamenergyplot.texi beamenergyplot.tex beamenergyprint.texi beamenergyprint.tex beamenergyprofileplot.texi beamenergyprofileplot.tex elementsettooffset.texi elementsettooffset.tex  elementaddoffset.texi  elementaddoffset.tex testnocorrection.texi testnocorrection.tex testfeedback.texi testfeedback.tex testsimplecorrection.texi testsimplecorrection.tex testfreecorrection2.texi testfreecorrection2.tex testsimplecorrectiondipole.texi testsimplecorrectiondipole.tex testballisticcorrection.texi testballisticcorrection.tex testfreecorrection.texi testfreecorrection.tex testmeasuredcorrection.texi testmeasuredcorrection.tex testquadrupolejitter.texi testquadrupolejitter.tex quadrupolegetstrength.texi quadrupolegetstrength.tex quadrupolegetstrengthlist.texi  quadrupolegetstrengthlist.tex quadrupolesetstrength.texi quadrupolesetstrength.tex quadrupolesetstrengthlist.texi quadrupolesetstrengthlist.tex quadrupolenumberlist.texi quadrupolenumberlist.tex
