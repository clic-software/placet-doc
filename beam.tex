After setting up the lattice one can define the different beams.
Currently three different types of beam can be defined. {\tt MainBeam} and
{\tt InjectBeam} define beams that are tracked through accelerators,
{\tt DriveBeam} defines a beam that is tracked through a decelerator.
{\tt MainBeam} corresponds to lattices defined with {\tt Cavity} or
via {\tt SetupMainBeamline} or {\tt SetupSmoothBeamline} in which
the beta-function is large compared to the structure length.
{\tt InjectBeam} corresponds to a lattice define with {\tt AccCacity} in which
each cell is simulated. {\tt DriveBeam} corresponds to a decelerator lattice
which has been defined using {\tt DecCavity}.
Several tcl procedures to define the beams are given within the code, see
in the examples directories the tcl scripts named make\_beam.tcl.

\subsection{Defining a Main Beam}
\begin{clist}
\command MainBeam {\it beamname} \\
Creates a main beam.
\opi bunches
The number of bunches in the pulse.
\opi slices
The number of slices per bunch.
\opf charge
The nominal charge in particles per bunch. Used for multi-bunch
calculations.
\ops file
The name of the file where the wake fields are stored.
\opf e0
The initial beam energy in GeV.
\opf energyspread
The RMS energy spread of the beam in GeV. To simulate an energy spread the
number of macroparticles has to be set to more than one.
\opf ecut
The number of sigmas included in the energy spread.
\opi macroparticles
The number of particles per slice. Default value is 1. If a value different
from one is chosen the energy spread will become effective.
\opfr beta\_x Horizontal beta-function $\beta$.
\opfr alpha\_x Horizontal twiss parameter $\alpha$.
\opfr emitt\_x Horizontal normalized emittance parameter in $10^{-7}m$.
{\it likely to change to $10^{-6}$}
\opfr beta\_y Vertical beta-function $\beta$.
\opfr alpha\_y Vertical twiss parameter $\alpha$.
\opfr emitt\_y Vertical normalized emittance parameter in $10^{-7}\u{m}$.
{\it likely to change to $10^{-6}\u{m}$}

\opf phase  Beam phase at entrance [degree].

\opf longrange\_list Longrange wakefield list [$V/pC/m^{2}$].

\opf g\_list gradient list (normalised)

\opf b\_list beam loading list (normalised)

\opfr last\_wgt Weight of the last bunch for the emittance measurements. 
Default value 1.
\end{clist}

\subsection{Defining a Drive Beam}
\begin{clist}
\command DriveBeam {\it beamname} \\
Creates a beam to be tracked through the drive beam decelerator.
\opfr bunches the number of bunches to be used.
\opfr charge Number of particles per bunch. Default value is 9.5e+10.
\oplr slice\_list  A list of the longitudinal positions of the slices and
their weights. Usually a routine like {\tt GaussList} will be used.
\opfr beta\_x Horizontal beta-function $\beta$.
\opfr alpha\_x Horizontal twiss parameter $\alpha$.
\opfr emitt\_x Horizontal normalized emittance parameter in $10^{-7}m$.
{\it likely to change to $10^{-6}$}
\opfr beta\_y Vertical beta-function $\beta$.
\opfr alpha\_y Vertical twiss parameter $\alpha$.
\opfr emitt\_y Vertical normalized emittance parameter in $10^{-7}\u{m}$.
{\it likely to change to $10^{-6}\u{m}$}
\opi macroparticles the number of particles per slice. Default value is 1.
\opfr distance Distance between bunches. If the option {\tt -bunch\_list} is
used the distance is taken as the distance between buckets.
Default value is -1e+30.
\opf envelope The beam envelope in sigma. This value is only used if the number
of macroparticles per slice is larger than one. In this case all particles but
one are distributed onto a beam ellipse with the given size.
Default value is 0.
\opf envelope\_wgt The weight of particles on the envelope as defined above.

The rest of the weight is carried by the central particle.
Default value is 0
\opfr slice\_list List of the slice position and weight
\opfr real\_last\_wgt Set weight of the last bunch for real initial beam [NB: results in wrong single bunch effect].
Default value 1
\opfr last\_wgt Weight of the last bunch for BPM readings (you get emittance measurements).
Default value 1.
\opfr macro\_offset\_y rms y offset for macroparticles of each slice.
Default value 0.
\opfr energy\_distribution: A list containing the energy distribution in the
form \{$e_1$ $w_1$ $e_2$ $w_2$\}. The energies $e_i$ are given relative to the
nominal one and the weights $w_i$ should add up to one.
\opfr e0 Beam energy at entrance in [GeV].
Default value is 1.5
\opfr e0\_list List of beam slices energies at entrance [GeV] {e0 e1...} [OVERRIDES -e0 if present].
\opfr bunch\_list

A list describing the bunches. Its element must be lists that each contain
the bucket number, the relative bunch charge (will be multiplied by
{\tt charge} and a longitudinal shift with respect to the bucket in \u{[\mu m]}.
The bucket number must be sorted. Example:
\begin{verbatim}
{
  {0 0.5 0.0}
  {1 0.75 0.0}
  {2 1.0 0.0}
}
\end{verbatim}
\opf ramp
Number of bunches in the charge ramp. Cannot be specified together with
{\tt bunch\_list}. Default is 0 (no charge ramp).

\opf ramp\_start
Charge of the first bunch if {\tt ramp} is specified. Default is 1.0.
\opf ramp\_step
Number of bunches after which the charge is stepped up in case {\tt ramp} is
specified. Default value is 1.

\opf phi\_0
Azimuthal rotation angle for the particles on the beam ellipse in [radian].
These particles will satisfy the equations
\begin{eqnarray}
\sqrt{\left(\frac{x}{\sigma_x}\right)^2
+\left(\frac{x^\prime}{\sigma_{x^\prime}}\right)^2}
&=&{\tt envelope\ }\sin\phi_0\nonumber\\
\wedge\ \ \sqrt{\left(\frac{y}{\sigma_y}\right)^2
+\left(\frac{y^\prime}{\sigma_{y^\prime}}\right)^2}
&=&{\tt envelope\ }\cos\phi_0\nonumber
\end{eqnarray}
Default value is 0.
\nix{
 -resistive\_wall:      include resistive wall (not active in the moment)
                Default value: 0
 -phi\_0:               axial rotation angle [radian]
                Default value: 0
}
\opf particles number of particles of the beam (used when converting from slices to particles).
\end{clist}

\subsection{Modify Existing Beams}
%\begin{figure}
%\epsfxsize=16cm
%\epsfbox{.eps}
%\caption{Amplification of an initial offset in the lattice. The particle with
%the largest deviation is shown. The two rows of points correspond to focusing
%and defocusing quadrupoles. For comparison also the results without transverse
%wake fields are shown. In this case the effect is due to the adiabatic
%undamping.}
%\label{f:jitter}
%\end{figure}
The properties of a existing beam can be modified. A possible application
is the investigation of the jitter amplification in a lattice. In this case the
beam is set to a transverse offset using {\tt BeamAddOffset} and then
tracked through a perfectly aligned lattice lattice without any beam-based
alignment. The resulting beam position along the lattice gives the necessary
information. A simple example using the command {\tt TestNoCorrection}
explained below would look like the following:
%BeamSetToOffset beam0 -start 0 -end [expr $n_slice*$n_macro*$n_bunch] \
%                -y 1.0
\begin{verbatim}
BeamAddOffset -beam beam0 -y 1.0 

FirstOrder 1
TestNoCorrection -beam beam0 -survey Zero -emitt_file emitt.dat
\end{verbatim}
Here, {\tt n\_slice}, {\tt n\_macro} and {\tt n\_bunch} contain the number
of slices per bunch, macro particles per slice and bunches per train,
respectively. The beam will be vertically offset to $y=1\u{\mu m}$.
The tracking of the beam ellipses is switched off using {\tt FirstOrder},
this increases the speed of the calculation.
Then beam is tracked through the lattice after this has been perfectly aligned
(the option {\tt -survey Zero}). The results are store in file {\tt emitt.dat}.
The first column of this file contains the quadrupole number, the eighths column
the maximum radius reached by any particle tracked. %Figure~\ref{f:jitter}
%shows the result of this simulation for the example decelerator.

The transverse position of a beam can be modified using the commands
{\tt BeamSetToOffset} and {\tt BeamAddOffset} (\S~\ref{first-chapter}).
%\begin{clist}
%\command BeamSetToOffset \\
%Sets the beam particles in the range from {\tt -start} to {\tt -end} to the
%specified offsets. Parameters that are not specified are set to zero.
%\opir start The first particle (inclusive) to offset.
%\opir end The last particle (exclusive) to offset.
%\opf x The horizontal position in \u{[\mu m]}.
%\opf y The vertical position in \u{[\mu m]}.
%\opf angle\_x The horizontal angle in \u{[\mu m]}.
%\opf angle\_y The vertical angle in \u{[\mu m]}.

%\command BeamAddOffset \\
%Adds the specified values to the offsets of the beam particles in the range
%from {\tt -start} to {\tt -end}. Parameters that are not specified are not
%modified.
%\opir start The first particle (inclusive) to offset.
%\opir end The last particle (exclusive) to offset.
%\opf x The horizontal position in \u{[\mu m]}.
%\opf y The vertical position in \u{[\mu m]}.
%\opf angle\_x The horizontal angle in \u{[\mu m]}.
%\opf angle\_y The vertical angle in \u{[\mu m]}.
%\end{clist}
